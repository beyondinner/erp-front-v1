import axios from '../../config/axios/axios'
import * as utils from '../utils/utils.js'
import SimpleCrypto from 'simple-crypto-js'
import * as mb from '../../entities/models'


/**Generate by ASGENS
 *@author Charlietyn
 *@date Thu Sep 12 00:09:29 GMT-04:00 2020
 *@time Thu Sep 12 00:09:29 GMT-04:00 2020
 */
const simpleCrypto = new SimpleCrypto("innererp")

export default class SiteModel {
  _user = undefined

  static get url () {
    return ''
  };
  get mb(){
    return mb
  }
  get url () {
    return ''
  };

  get is_loggin () {
    return localStorage.getItem('uth-erp')
  }
  get user () {
    return localStorage.getItem('uth-erp') ? Object.freeze(utils.extract_user_data('id_user', simpleCrypto.decrypt(localStorage.getItem('uth-erp')))):null
  }
  get role () {
    return localStorage.getItem('uth-erp') ? Object.freeze(utils.extract_user_data('id_role', simpleCrypto.decrypt(localStorage.getItem('uth-erp')))):null
  }

  get jwt () {
    return simpleCrypto.decrypt(localStorage.getItem('uth-erp'));
  }
  get is_member () {
    return !this.user?false:this.user.club_member?true:false
  }
  get innser_coins () {
    return this.user.club_member?this.user.club_member.clum_members_currency:null
  }

  set user (value) {
    this._user = value
  }

  login (params = {}, isFormData = false) {
    return axios.post(this.url + 'login', params).then((response) => {
      const token_hash = simpleCrypto.encrypt(response.data.token);
      localStorage.setItem('uth-erp',token_hash);
      return response
    }).catch(error => {
      throw JSON.parse(JSON.stringify(error))
    })
  }
  has_access(accion=""){
    return this.is_member?false:true
  }
  logout (params = {}, isFormData = false) {
    localStorage.removeItem('uth-erp');
    // return axios.post(this.url + 'logout', params).then((response) => {
    //   return response
    // }).catch(error => {
    //   throw JSON.parse(JSON.stringify(error))
    // })
  }

  signup (params = {}, isFormData = false) {
    return axios.post(this.url + 'signup', params, header(isFormData)).then((response) => {
      return response
    }).catch(error => {
      throw JSON.parse(JSON.stringify(error))
    })
  }

  static custom (verb, action, params = {}, header = {'Content-Type': 'application/json; charset=UTF-8'}) {
    return axios[verb](this.url + '/' + action, params, header).then((response) => {
      return response
    }).catch(error => {
      throw JSON.parse(JSON.stringify(error))
    })
  }
}

