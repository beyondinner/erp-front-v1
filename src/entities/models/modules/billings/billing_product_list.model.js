/**Generate by ASGENS
*@author Charlietyn 
*@date Mon Oct 19 15:57:26 GMT-04:00 2020  
*@time Mon Oct 19 15:57:26 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'billings/billing_product_list';

    export default class Billing_product_list extends BaseModel {

       id_billing_product_list
       billing_product_list_price
       billing_product_list_concept
       billing_product_list_desc
       billing_product_list_quantity
       tax_id
       created_at
       updated_at
       id_product
       id_billing

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_billing_product_list = attributes.id_billing_product_list|| undefined
        this.billing_product_list_price = attributes.billing_product_list_price|| ''
        this.billing_product_list_concept = attributes.billing_product_list_concept|| ''
        this.billing_product_list_desc = attributes.billing_product_list_desc|| ''
        this.billing_product_list_quantity = attributes.billing_product_list_quantity|| ''
        this.tax_id = attributes.tax_id|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
        this.id_product = attributes.id_product|| ''
        this.id_billing = attributes.id_billing|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_billing_product_list = attributes.id_billing_product_list
        this.billing_product_list_price = attributes.billing_product_list_price
        this.billing_product_list_concept = attributes.billing_product_list_concept
        this.billing_product_list_desc = attributes.billing_product_list_desc
        this.billing_product_list_quantity = attributes.billing_product_list_quantity
        this.tax_id = attributes.tax_id
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
        this.id_product = attributes.id_product
        this.id_billing = attributes.id_billing
      }
    }

    static validations = {
      billing_product_list: {
        billing_product_list_price: {
          required,
          decimal,
        },
        billing_product_list_concept: {
          required,
        },
        billing_product_list_desc: {
          required,
        },
        billing_product_list_quantity: {
          required,
          integer,
        },
        tax_id: {
          required,
          integer,
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
        id_product: {
          integer,
        },
        id_billing: {
          required,
          integer,
        },
      },
    }

    static feedbacks = {
      billing_product_list: {
      id_billing_product_list: {
        isUnique: 'This id_billing_product_list has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Billing_product_list_price',
      dataIndex: 'billing_product_list_price',
      align:'center',
      key: 'billing_product_list_price',
      width: '20%',
      sorter: (a, b) => a.billing_product_list_price - b.billing_product_list_price
    },
    {
      title: 'Billing_product_list_concept',
      dataIndex: 'billing_product_list_concept',
      align:'center',
      key: 'billing_product_list_concept',
      width: '20%',
      sorter: (a, b) =>  (a.billing_product_list_concept > b.billing_product_list_concept)-(a.billing_product_list_concept < b.billing_product_list_concept)
    },
    {
      title: 'Billing_product_list_desc',
      dataIndex: 'billing_product_list_desc',
      align:'center',
      key: 'billing_product_list_desc',
      width: '20%',
      sorter: (a, b) =>  (a.billing_product_list_desc > b.billing_product_list_desc)-(a.billing_product_list_desc < b.billing_product_list_desc)
    },
    {
      title: 'Billing_product_list_quantity',
      dataIndex: 'billing_product_list_quantity',
      align:'center',
      key: 'billing_product_list_quantity',
      width: '20%',
      sorter: (a, b) => a.billing_product_list_quantity - b.billing_product_list_quantity
    },
    {
      title: 'Tax',
      dataIndex: 'tax.tax_name',
      align:'center',
      key: 'tax.tax_name',
      width: '20%',
      sorter: (a, b) =>  (a.tax.tax_name > b.tax.tax_name)-(a.tax.tax_name < b.tax.tax_name)
    },
    {
      title: 'Product',
      dataIndex: 'product.product_name',
      align:'center',
      key: 'product.product_name',
      width: '20%',
      sorter: (a, b) =>  (a.product.product_name > b.product.product_name)-(a.product.product_name < b.product.product_name)
    },
    {
      title: 'Billing',
      dataIndex: 'billing.billings_doc_number',
      align:'center',
      key: 'billing.billings_doc_number',
      width: '20%',
      sorter: (a, b) =>  (a.billing.billings_doc_number > b.billing.billings_doc_number)-(a.billing.billings_doc_number < b.billing.billings_doc_number)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id_billing_product_list;
    }
    class_name() {
        return 'Billing_product_list'
      }
  

   }

