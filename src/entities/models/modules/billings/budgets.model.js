/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Sep 07 15:53:43 GMT-04:00 2020
 *@time Mon Sep 07 15:53:43 GMT-04:00 2020
 */
import {integer, maxLength, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

import moment from 'moment'

const url = 'billings/budgets'

export default class Budgets extends BaseModel {

  id_budget
  contact_id
  budget_doc_number
  budget_start_date
  budget_expiration_date
  payment_type_id
  budget_internal_desc
  budget_status_id
  budget_items_list
  created_at
  updated_at

  constructor (attributes = null) {
    super()
    if (attributes != null) {
      this.id_budget = attributes.id_budget || undefined
      this.contact_id = attributes.contact_id || ''
      this.budget_doc_number = attributes.budget_doc_number || ''
      this.budget_start_date = attributes.budget_start_date ? moment(attributes.budget_start_date) : null
      this.budget_expiration_date = attributes.budget_expiration_date ? moment(attributes.budget_expiration_date) : null
      this.payment_type_id = attributes.payment_type_id || ''
      this.budget_internal_desc = attributes.budget_internal_desc || ''
      this.budget_status_id = attributes.budget_status_id || ''
      this.created_at = attributes.created_at ? moment(attributes.created_at).format() : moment()
      this.updated_at = attributes.updated_at ? moment(attributes.updated_at).format() : moment()
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_budget = attributes.id_budget
      this.contact_id = attributes.contact_id
      this.budget_doc_number = attributes.budget_doc_number
      this.budget_start_date = moment(attributes.budget_start_date).format()
      this.budget_expiration_date = moment(attributes.budget_expiration_date).format()
      this.payment_type_id = attributes.payment_type_id
      this.budget_internal_desc = attributes.budget_internal_desc
      this.budget_status_id = attributes.budget_status_id
      this.created_at = moment(attributes.created_at).format()
      this.updated_at = moment(attributes.updated_at).format()
    }
  }

  static validations = {
    budgets: {
      contact_id: {
        required,
        integer,
      },
      budget_doc_number: {
        required,
      },
      budget_start_date: {
        required,
      },
      budget_expiration_date: {
        required,
      },
      payment_type_id: {
        required,
        integer,
      },
      budget_internal_desc: {
        maxLength: maxLength(65535),
      },
      budget_status_id: {
        required,
        integer,
      },
      created_at: {
        required,
      },
      updated_at: {
        required,
      },
    },
  }

  static feedbacks = {
    budgets: {
      id_budget: {
        isUnique: 'This id_budget has been taken'

      },
    },
  }

  static columns = [
    {
      title: 'Inicia',
      dataIndex: 'budget_start_date',
      key: 'budget_start_date',
      width: '12%',
      align: 'center',
      sorter: (a, b) => a.budget_start_date - b.budget_start_date
    },
    {
      title: 'Vence',
      dataIndex: 'budget_expiration_date',
      key: 'budget_expiration_date',
      width: '12%',
      align: 'center',
      sorter: (a, b) => a.budget_expiration_date - b.budget_expiration_date
    },
    {
      title: 'Número',
      dataIndex: 'budget_doc_number',
      key: 'budget_doc_number',
      width: '12%',
      align: 'center',
      sorter: (a, b) => (a.budget_doc_number > b.budget_doc_number) - (a.budget_doc_number < b.budget_doc_number)
    },
    {
      title: 'Cliente',
      dataIndex: 'contacts.contact_full_name',
      key: 'contacts.id_contact',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.contacts.contact_full_name > b.contacts.contact_full_name) - (a.contacts.contact_full_name < b.contacts.contact_full_name)
    },
    {
      title: 'F. de pago',
      dataIndex: 'payment_types.payment_type_siglas',
      key: 'payment_types.id_payment_type',
      width: '12%',
      align: 'center',
      sorter: (a, b) => (a.payment_types.payment_type_siglas > b.payment_types.payment_type_siglas) - (a.payment_types.payment_type_siglas < b.payment_types.payment_type_siglas)
    },
    {
      title: 'Desc. interna',
      dataIndex: 'budget_internal_desc',
      key: 'budget_internal_desc',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.budget_internal_desc > b.budget_internal_desc) - (a.budget_internal_desc < b.budget_internal_desc)
    },
    {
      title: 'Estado',
      dataIndex: 'budget_status.budget_status_siglas',
      key: 'budget_status.id_budget_status',
      width: '12%',
      align: 'center',
      sorter: (a, b) => (a.budget_status.budget_status_siglas > b.budget_status.budget_status_siglas) - (a.budget_status.budget_status_siglas < b.budget_status.budget_status_siglas)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '25%',
      fixed:'right',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_budget
  }

  class_name () {
    return 'Budgets'
  }

}

