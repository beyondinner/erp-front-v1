/**Generate by ASGENS
*@author Charlietyn 
*@date Tue Nov 24 13:24:08 GMT-05:00 2020  
*@time Tue Nov 24 13:24:08 GMT-05:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  const url = 'billings/orders_product';

    export default class Orders_product extends BaseModel {

       id_order_products
       id_order
       id_product
       id_services
       amount

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_order_products = attributes.id_order_products|| undefined
        this.id_order = attributes.id_order|| ''
        this.id_product = attributes.id_product|| ''
        this.id_services = attributes.id_services|| ''
        this.amount = attributes.amount|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_order_products = attributes.id_order_products
        this.id_order = attributes.id_order
        this.id_product = attributes.id_product
        this.id_services = attributes.id_services
        this.amount = attributes.amount
      }
    }

    static validations = {
      orders_product: {
        id_order: {
          required,
          integer,
        },
        id_product: {
          integer,
        },
        id_services: {
          integer,
        },
        amount: {
          integer,
        },
      },
    }

    static feedbacks = {
      orders_product: {
      id_order_products: {
        isUnique: 'This id_order_products has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Order',
      dataIndex: 'order.orders_code',
      align:'center',
      key: 'order.orders_code',
      width: '20%',
      sorter: (a, b) =>  (a.order.orders_code > b.order.orders_code)-(a.order.orders_code < b.order.orders_code)
    },
    {
      title: 'Product',
      dataIndex: 'product.product_name',
      align:'center',
      key: 'product.product_name',
      width: '20%',
      sorter: (a, b) =>  (a.product.product_name > b.product.product_name)-(a.product.product_name < b.product.product_name)
    },
    {
      title: 'Services',
      dataIndex: 'services.service_name',
      align:'center',
      key: 'services.service_name',
      width: '20%',
      sorter: (a, b) =>  (a.services.service_name > b.services.service_name)-(a.services.service_name < b.services.service_name)
    },
    {
      title: 'Amount',
      dataIndex: 'amount',
      align:'center',
      key: 'amount',
      width: '20%',
      sorter: (a, b) => a.amount - b.amount
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id_order_products;
    }
    class_name() {
        return 'Orders_product'
      }
  

   }

