/**Generate by ASGENS
*@author Charlietyn
*@date Thu Mar 18 00:31:17 GMT-04:00 2021
*@time Thu Mar 18 00:31:17 GMT-04:00 2021
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'billings/payment_recurrency';

    export default class Payment_recurrency extends BaseModel {

       id_payment_recurrency
       id_service
       id_club_members
       initial_date
       recurrency_date
       active

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_payment_recurrency = attributes.id_payment_recurrency|| undefined
        this.id_service = attributes.id_service|| null
        this.id_club_members = attributes.id_club_members|| null
        this.initial_date = attributes.initial_date?moment(attributes.initial_date).format('YYYY-MM-DD'):null
        this.initial_date = attributes.initial_date|| null
        this.recurrency_date = attributes.recurrency_date|| null
        this.active = attributes.active=='1'?true:false
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_payment_recurrency = attributes.id_payment_recurrency
        this.id_service = attributes.id_service
        this.id_club_members = attributes.id_club_members
        this.initial_date = moment(attributes.initial_date).format('YYYY-MM-DD')
        this.recurrency_date = attributes.recurrency_date
        this.active = attributes.active
      }
    }

    static validations = {
      payment_recurrency: {
        id_service: {
          integer,
        },
        id_club_members: {
          required,
          integer,
        },
        initial_date: {
          required,
        },
        recurrency_date: {
          required,
          integer,
        },
        active: {
        },
      },
    }

    static feedbacks = {
      payment_recurrency: {
      id_payment_recurrency: {
        isUnique: 'This id_payment_recurrency has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Servicio',
      dataIndex: 'service.service_name',
      align:'center',
      key: 'service.service_name',

      sorter: (a, b) =>  a.service && b.service?(a.service.service_name > b.service.service_name)-(a.service.service_name < b.service.service_name):0
    },
    {
      title: 'Club_members',
      dataIndex: 'club_members.club_members_name',
      align:'center',
      key: 'club_members.club_members_name',

      sorter: (a, b) =>  a.club_members && b.club_members?(a.club_members.club_members_name > b.club_members.club_members_name)-(a.club_members.club_members_name < b.club_members.club_members_name):0
    },
    {
      title: 'Próximo Pago',
      customRender:(value)=>{
        return moment(value).format("DD/MM/YYYY")
      },
      align:'center',
      key: 'initial_date',

      sorter: (a, b) => a.initial_date - b.initial_date
    },
    {
      title: 'Tiempo Recurrente',
      dataIndex: 'recurrency_date',
      align:'center',
      key: 'recurrency_date',

      sorter: (a, b) => a.recurrency_date - b.recurrency_date
    },
    {
      title: 'Activo',
      scopedSlots: {
        customRender: 'activo'
      },
      align:'center',
      key: 'active',

    },
    {
      title: 'Acciones',
      key: 'action_elements',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_payment_recurrency;
    }
    class_name() {
        return 'Payment_recurrency'
      }


   }

