/**Generate by ASGENS
 *@author Charlietyn
 *@date Tue Nov 24 13:24:08 GMT-05:00 2020
 *@time Tue Nov 24 13:24:08 GMT-05:00 2020
 */
import {integer, maxLength, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

import moment from 'moment'

const url = 'billings/orders'

export default class Orders extends BaseModel {

  id_orders
  orders_code
  date_at
  id_contact
  id_user
  id_order_status
  created_at
  updated_at
  order_description

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_orders = attributes.id_orders || undefined
      this.orders_code = attributes.orders_code || ''
      this.date_at = attributes.date_at ? moment(attributes.date_at) : null
      this.id_contact = attributes.id_contact || ''
      this.id_user = attributes.id_user || ''
      this.id_order_status = attributes.id_order_status || ''
      this.created_at = attributes.created_at ? moment(attributes.created_at) : null
      this.updated_at = attributes.updated_at ? moment(attributes.updated_at) : null
      this.order_description = attributes.order_description || ''
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_orders = attributes.id_orders
      this.orders_code = attributes.orders_code
      this.date_at = moment(attributes.date_at)
      this.id_contact = attributes.id_contact
      this.id_user = attributes.id_user
      this.id_order_status = attributes.id_order_status
      this.created_at = moment(attributes.created_at)
      this.updated_at = moment(attributes.updated_at)
      this.order_description = attributes.order_description
    }
  }

  static validations = {
    orders: {
      orders_code: {
        required,
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_orders, orders_code} = object
          const _scenario = id_orders ? 'update' : 'create'
          const params = {id_orders, orders_code}
          const validation = await Orders.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        }
      },
      date_at: {},
      id_contact: {
        integer,
        required
      },
      id_user: {
        integer,
      },
      id_order_status: {
        required,
        integer,
      },
      created_at: {},
      updated_at: {},
      order_description: {
        maxLength: maxLength(65535),
      },
    },
  }

  static feedbacks = {
    orders: {
      id_orders: {
        isUnique: 'This id_orders has been taken'

      },
      orders_code: {
        isUnique: 'This orders_code has been taken'

      },
    },
  }

  static columns = [
    {
      title: 'Código',
      dataIndex: 'orders_code',
      align: 'center',
      key: 'orders_code',
      width: '20%',
      sorter: (a, b) => (a.orders_code > b.orders_code) - (a.orders_code < b.orders_code)
    },
    {
      title: 'Fecha de petición',
      dataIndex: 'date_at',
      align: 'center',
      key: 'date_at',
      width: '20%',
      sorter: (a, b) => a.date_at - b.date_at
    },
    {
      title: 'Proveedor',
      dataIndex: 'contact.contact_full_name',
      align: 'center',
      key: 'contact.contact_full_name',
      width: '20%',
      sorter: (a, b) => (a.contact.fiscal_name > b.contact.fiscal_name) - (a.contact.fiscal_name < b.contact.fiscal_name)
    },
    {
      title: 'Usuario',
      dataIndex: 'user.nombre_usuario',
      align: 'center',
      key: 'user.nombre_usuario',
      width: '20%',
      sorter: (a, b) => (a.user.nombre_usuario > b.user.nombre_usuario) - (a.user.nombre_usuario < b.user.nombre_usuario)
    },
    {
      title: 'Estado',
      scopedSlots: {
        customRender: 'status'
      },
      align: 'center',
      key: 'order_status.order_status',
      width: '20%',
      sorter: (a, b) => (a.order_status.order_status > b.order_status.order_status) - (a.order_status.order_status < b.order_status.order_status)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_orders
  }

  class_name () {
    return 'Orders'
  }

}

