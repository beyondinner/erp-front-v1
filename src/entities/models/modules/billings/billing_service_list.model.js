/**Generate by ASGENS
*@author Charlietyn
*@date Mon Oct 19 15:57:26 GMT-04:00 2020
*@time Mon Oct 19 15:57:26 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'billings/billing_service_list';

    export default class Billing_service_list extends BaseModel {

       id_billing_service_list
       billing_service_list_price
       billing_service_list_concept
       billing_service_list_desc
       billing_service_list_quantity
       tax_id
       created_at
       updated_at
       id_service
       id_billing

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_billing_service_list = attributes.id_billing_service_list|| undefined
        this.billing_service_list_price = attributes.billing_service_list_price|| ''
        this.billing_service_list_concept = attributes.billing_service_list_concept|| ''
        this.billing_service_list_desc = attributes.billing_service_list_desc|| ''
        this.billing_service_list_quantity = attributes.billing_service_list_quantity|| ''
        this.tax_id = attributes.tax_id|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
        this.id_service = attributes.id_service|| ''
        this.id_billing = attributes.id_billing|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_billing_service_list = attributes.id_billing_service_list
        this.billing_service_list_price = attributes.billing_service_list_price
        this.billing_service_list_concept = attributes.billing_service_list_concept
        this.billing_service_list_desc = attributes.billing_service_list_desc
        this.billing_service_list_quantity = attributes.billing_service_list_quantity
        this.tax_id = attributes.tax_id
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
        this.id_service = attributes.id_service
        this.id_billing = attributes.id_billing
      }
    }

    static validations = {
      billing_service_list: {
        id_billing_service_list: {
          required,
          integer,
        },
        billing_service_list_price: {
          required,
          decimal,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,billing_service_list_price}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,billing_service_list_price}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        billing_service_list_concept: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,billing_service_list_concept}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,billing_service_list_concept}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        billing_service_list_desc: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,billing_service_list_desc}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,billing_service_list_desc}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        billing_service_list_quantity: {
          required,
          integer,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,billing_service_list_quantity}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,billing_service_list_quantity}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        tax_id: {
          required,
          integer,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,tax_id}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,tax_id}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        created_at: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,created_at}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,created_at}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        updated_at: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,updated_at}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,updated_at}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        id_service: {
          integer,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,id_service}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,id_service}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        id_billing: {
          required,
          integer,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billing_service_list,id_billing}=object
               const _scenario=id_billing_service_list?'update':'create'
               const params={id_billing_service_list,id_billing}
               const validation= await Billing_service_list.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
      },
    }

    static feedbacks = {
      billing_service_list: {
      id_billing_service_list: {
        isUnique: 'This id_billing_service_list has been taken'

      },
      billing_service_list_price: {
        isUnique: 'This billing_service_list_price has been taken'

      },
      billing_service_list_concept: {
        isUnique: 'This billing_service_list_concept has been taken'

      },
      billing_service_list_desc: {
        isUnique: 'This billing_service_list_desc has been taken'

      },
      billing_service_list_quantity: {
        isUnique: 'This billing_service_list_quantity has been taken'

      },
      tax_id: {
        isUnique: 'This tax_id has been taken'

      },
      created_at: {
        isUnique: 'This created_at has been taken'

      },
      updated_at: {
        isUnique: 'This updated_at has been taken'

      },
      id_service: {
        isUnique: 'This id_service has been taken'

      },
      id_billing: {
        isUnique: 'This id_billing has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Id_billing_service_list',
      dataIndex: 'id_billing_service_list',
      align:'center',
      key: 'id_billing_service_list',
      width: '20%',
      sorter: (a, b) => a.id_billing_service_list - b.id_billing_service_list
    },
    {
      title: 'Billing_service_list_price',
      dataIndex: 'billing_service_list_price',
      align:'center',
      key: 'billing_service_list_price',
      width: '20%',
      sorter: (a, b) => a.billing_service_list_price - b.billing_service_list_price
    },
    {
      title: 'Billing_service_list_concept',
      dataIndex: 'billing_service_list_concept',
      align:'center',
      key: 'billing_service_list_concept',
      width: '20%',
      sorter: (a, b) =>  (a.billing_service_list_concept > b.billing_service_list_concept)-(a.billing_service_list_concept < b.billing_service_list_concept)
    },
    {
      title: 'Billing_service_list_desc',
      dataIndex: 'billing_service_list_desc',
      align:'center',
      key: 'billing_service_list_desc',
      width: '20%',
      sorter: (a, b) =>  (a.billing_service_list_desc > b.billing_service_list_desc)-(a.billing_service_list_desc < b.billing_service_list_desc)
    },
    {
      title: 'Billing_service_list_quantity',
      dataIndex: 'billing_service_list_quantity',
      align:'center',
      key: 'billing_service_list_quantity',
      width: '20%',
      sorter: (a, b) => a.billing_service_list_quantity - b.billing_service_list_quantity
    },
    {
      title: 'Tax',
      dataIndex: 'tax.tax_name',
      align:'center',
      key: 'tax.tax_name',
      width: '20%',
      sorter: (a, b) =>  (a.tax.tax_name > b.tax.tax_name)-(a.tax.tax_name < b.tax.tax_name)
    },
    {
      title: 'Service',
      dataIndex: 'service.service_name',
      align:'center',
      key: 'service.service_name',
      width: '20%',
      sorter: (a, b) =>  (a.service.service_name > b.service.service_name)-(a.service.service_name < b.service.service_name)
    },
    {
      title: 'Billing',
      dataIndex: 'billing.billings_doc_number',
      align:'center',
      key: 'billing.billings_doc_number',
      width: '20%',
      sorter: (a, b) =>  (a.billing.billings_doc_number > b.billing.billings_doc_number)-(a.billing.billings_doc_number < b.billing.billings_doc_number)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_billing_service_list;
    }
    class_name() {
        return 'Billing_service_list'
      }


   }

