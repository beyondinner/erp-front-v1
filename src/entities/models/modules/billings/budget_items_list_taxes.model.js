/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'billings/budget_items_list_taxes';

    export default class Budget_items_list_taxes extends BaseModel {

       id_budget_items_list_taxes
       budget_items_list_taxes_tax
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_budget_items_list_taxes = attributes.id_budget_items_list_taxes|| undefined
        this.budget_items_list_taxes_tax = attributes.budget_items_list_taxes_tax|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_budget_items_list_taxes = attributes.id_budget_items_list_taxes
        this.budget_items_list_taxes_tax = attributes.budget_items_list_taxes_tax
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      budget_items_list_taxes: {
        budget_items_list_taxes_tax: {
          required,
          integer,
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

    static feedbacks = {
      budget_items_list_taxes: {
      id_budget_items_list_taxes: {
        isUnique: 'This id_budget_items_list_taxes has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Impuesto',
      dataIndex: 'budget_items_list_taxes_tax',
      key: 'budget_items_list_taxes_tax',
      width: '100%',
      align: 'center',
      sorter: (a, b) => a.budget_items_list_taxes_tax - b.budget_items_list_taxes_tax
    },
    {
      title: 'Created_at',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Updated_at',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_budget_items_list_taxes;
    }

      class_name() {
        return 'Budget_items_list_taxes'
      }

   }

