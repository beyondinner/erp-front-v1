/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Sep 07 15:53:43 GMT-04:00 2020
 *@time Mon Sep 07 15:53:43 GMT-04:00 2020
 */
import {integer, maxLength, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

import moment from 'moment'

const url = 'billings/budget_items_list'

export default class Budget_items_list extends BaseModel {

  id_budget_items_list
  budget_id
  budget_items_list_concept
  budget_items_list_desc
  budget_items_list_quantity
  budget_items_list_price
  tax_id
  created_at
  updated_at
  id_product
  id_service

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_budget_items_list = attributes.id_budget_items_list || undefined
      this.budget_id = attributes.budget_id || ''
      this.budget_items_list_concept = attributes.budget_items_list_concept || ''
      this.budget_items_list_desc = attributes.budget_items_list_desc || ''
      this.budget_items_list_quantity = attributes.budget_items_list_quantity || ''
      this.tax_id = attributes.tax_id || ''
      this.budget_items_list_price = attributes.budget_items_list_price || 0
      this.id_product = attributes.id_product || ''
      this.id_service = attributes.id_service || ''
      this.created_at = attributes.created_at ? moment(attributes.created_at).format() : moment()
      this.updated_at = attributes.updated_at ? moment(attributes.updated_at).format() : moment()
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_budget_items_list = attributes.id_budget_items_list
      this.budget_id = attributes.budget_id
      this.budget_items_list_concept = attributes.budget_items_list_concept
      this.budget_items_list_price = attributes.budget_items_list_price
      this.budget_items_list_desc = attributes.budget_items_list_desc
      this.budget_items_list_quantity = attributes.budget_items_list_quantity
      this.tax_id = attributes.tax_id
      this.id_product = attributes.id_product
      this.id_service = attributes.id_service
      this.created_at = moment(attributes.created_at).format()
      this.updated_at = moment(attributes.updated_at).format()
    }
  }

  static validations = {
    budget_items_list: {
      budget_id: {
        required,
        integer,
      },
      budget_items_list_concept: {
        required,
        maxLength: maxLength(30),
      },
      budget_items_list_desc: {
        required,
        maxLength: maxLength(60),
      },
      budget_items_list_quantity: {
        required,
        integer,
      },
      tax_id: {
        required,
        integer,
      },
      created_at: {
        required,
      },
      updated_at: {
        required,
      },
    },
  }

  static feedbacks = {
    budget_items_list: {
      id_budget_items_list: {
        isUnique: 'This id_budget_items_list has been taken'

      },
    },
  }

  static columns = [
    {
      title: 'Budgets',
      dataIndex: 'budgets.budget_doc_number',
      key: 'budgets.id_budget',
      width: '20%',
      sorter: (a, b) => (a.budgets.budget_doc_number > b.budgets.budget_doc_number) - (a.budgets.budget_doc_number < b.budgets.budget_doc_number)
    },
    {
      title: 'Concepto',
      dataIndex: 'budget_items_list_concept',
      key: 'budget_items_list_concept',
      width: '20%',
      sorter: (a, b) => (a.budget_items_list_concept > b.budget_items_list_concept) - (a.budget_items_list_concept < b.budget_items_list_concept)
    },
    {
      title: 'Descripción',
      dataIndex: 'budget_items_list_desc',
      key: 'budget_items_list_desc',
      width: '20%',
      sorter: (a, b) => (a.budget_items_list_desc > b.budget_items_list_desc) - (a.budget_items_list_desc < b.budget_items_list_desc)
    },
    {
      title: 'Precio',
      dataIndex: 'budget_items_list_price',
      key: 'budget_items_list_price',
      width: '20%',
      sorter: (a, b) => (a.budget_items_list_price > b.budget_items_list_price) - (a.budget_items_list_price < b.budget_items_list_price)
    },
    {
      title: 'Cantidad',
      dataIndex: 'budget_items_list_quantity',
      key: 'budget_items_list_quantity',
      width: '20%',
      sorter: (a, b) => a.budget_items_list_quantity - b.budget_items_list_quantity
    },
    {
      title: 'Impuesto',
      dataIndex: 'budget_items_list_taxes.id_budget_items_list_taxes',
      key: 'budget_items_list_taxes.id_budget_items_list_taxes',
      width: '20%',
      sorter: (a, b) => a.budget_items_list_taxes.id_budget_items_list_taxes - b.budget_items_list_taxes.id_budget_items_list_taxes
    },
    {
      title: 'Created_at',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Updated_at',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_budget_items_list
  }

  class_name () {
    return 'Budget_items_list'
  }

}

