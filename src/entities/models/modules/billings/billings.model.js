/**Generate by ASGENS
*@author Charlietyn
*@date Mon Oct 19 22:24:35 GMT-04:00 2020
*@time Mon Oct 19 22:24:35 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'billings/billings';

    export default class Billings extends BaseModel {

       id_billings
       budget_id
       billings_status_id
       billings_doc_number
       created_at
       updated_at
       id_contact
       date_at
       expiration_date
       id_payment_type
       billing_description
       billing_types

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_billings = attributes.id_billings|| undefined
        this.budget_id = attributes.budget_id|| ''
        this.billings_status_id = attributes.billings_status_id|| ''
        this.billings_doc_number = attributes.billings_doc_number|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
        this.id_contact = attributes.id_contact|| ''
        this.date_at = attributes.date_at?moment(attributes.date_at):null
        this.expiration_date = attributes.expiration_date?moment(attributes.expiration_date):null
        this.id_payment_type = attributes.id_payment_type|| ''
        this.billing_description = attributes.billing_description|| ''
        this.billing_types = attributes.billing_types|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_billings = attributes.id_billings
        this.budget_id = attributes.budget_id
        this.billings_status_id = attributes.billings_status_id
        this.billings_doc_number = attributes.billings_doc_number
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
        this.id_contact = attributes.id_contact
        this.date_at = moment(attributes.date_at)
        this.expiration_date = moment(attributes.expiration_date)
        this.id_payment_type = attributes.id_payment_type
        this.billing_description = attributes.billing_description
        this.billing_types = attributes.billing_types
      }
    }

    static validations = {
      billings: {
        budget_id: {
          integer,
        },
        billings_status_id: {
          required,
          integer,
        },
        billings_doc_number: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_billings,billings_doc_number}=object
               const _scenario=id_billings?'update':'create'
               const params={id_billings,billings_doc_number}
               const validation= await Billings.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
        id_contact: {
          required,
          integer,
        },
        date_at: {
          required,
        },
        expiration_date: {
          required,
        },
        id_payment_type: {
          required,
          integer,
        },
        billing_description: {
          required,
          maxLength: maxLength(65535),
        },
      },
    }

    static feedbacks = {
      billings: {
      id_billings: {
        isUnique: 'This id_billings has been taken'

      },
      billings_doc_number: {
        isUnique: 'This billings_doc_number has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Presupuesto',
      scopedSlots: {
        customRender: 'budget'
      },
      align:'center',
      key: 'budget.budget_doc_number',
      width: '20%',
      sorter: (a, b) =>  (a.budget.budget_doc_number > b.budget.budget_doc_number)-(a.budget.budget_doc_number < b.budget.budget_doc_number)
    },
    {
      title: 'Estado',
      dataIndex: 'billings_status.billings_status_siglas',
      align:'center',
      key: 'billings_status.billings_status_siglas',
      width: '10%',
      sorter: (a, b) =>  (a.billings_status.billings_status_siglas > b.billings_status.billings_status_siglas)-(a.billings_status.billings_status_siglas < b.billings_status.billings_status_siglas)
    },
    {
      title: ' # Doc',
      dataIndex: 'billings_doc_number',
      align:'center',
      key: 'billings_doc_number',
      width: '20%',
      sorter: (a, b) =>  (a.billings_doc_number > b.billings_doc_number)-(a.billings_doc_number < b.billings_doc_number)
    },
    {
      title: 'Contacto',
      dataIndex: 'contact.contact_full_name',
      align:'center',
      key: 'contact.contact_full_name',
      width: '20%',
      sorter: (a, b) =>  (a.contact.contact_full_name > b.contact.contact_full_name)-(a.contact.contact_full_name < b.contact.contact_full_name)
    },
    {
      title: 'Fecha',
      customRender:(element)=> { return moment(element.date_at).format('DD/MM/YYYY')},
      align:'center',
      key: 'date_at',
      width: '20%',
      sorter: (a, b) => a.date_at - b.date_at
    },
    {
      title: 'Fecha de vencimiento',
      customRender:(element)=> { return moment(element.expiration_date).format('DD/MM/YYYY')},
      align:'center',
      key: 'expiration_date',
      width: '20%',
      sorter: (a, b) => a.expiration_date - b.expiration_date
    },
    {
      title: 'Tipo de pago',
      dataIndex: 'payment_type.payment_type_siglas',
      align:'center',
      key: 'payment_type.payment_type_siglas',
      width: '30%',
      sorter: (a, b) =>  (a.payment_type.payment_type_siglas > b.payment_type.payment_type_siglas)-(a.payment_type.payment_type_siglas < b.payment_type.payment_type_siglas)
    },
    {
      title: 'descripcion',
      dataIndex: 'billing_description',
      align:'center',
      key: 'billing_description',
      width: '20%',
      sorter: (a, b) =>  (a.billing_description > b.billing_description)-(a.billing_description < b.billing_description)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_billings;
    }
    class_name() {
        return 'Billings'
      }


   }

