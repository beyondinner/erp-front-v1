/**Generate by ASGENS
*@author Charlietyn
*@date Fri Oct 09 01:26:07 GMT-04:00 2020
*@time Fri Oct 09 01:26:07 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/event_product';

    export default class Event_product extends BaseModel {

       id_event_product
       created_at
       id_event
       id_product
       amount

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_event_product = attributes.id_event_product|| undefined
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.id_event = attributes.id_event|| ''
        this.id_product = attributes.id_product|| ''
        this.amount = attributes.amount|| 1
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_event_product = attributes.id_event_product
        this.created_at = moment(attributes.created_at)
        this.id_event = attributes.id_event
        this.id_product = attributes.id_product
        this.amount = attributes.amount
      }
    }

    static validations = {
      event_product: {
        created_at: {
        },
        id_event: {
          required,
          integer,
        },
        id_product: {
          required,
          integer,
        },
        amount: {
          required,
          integer,
        },
      },
    }

    static feedbacks = {
      event_product: {
      id_event_product: {
        isUnique: 'This id_event_product has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Event',
      dataIndex: 'event.event_name',
      key: 'event.event_name',
      align:'center',
      width: '20%',
      sorter: (a, b) =>  (a.event.event_name > b.event.event_name)-(a.event.event_name < b.event.event_name)
    },
    {
      title: 'Producto',
      dataIndex: 'product.product_name',
      key: 'product.product_name',
      align:'center',
      width: '40%',
      sorter: (a, b) =>  (a.product.product_name > b.product.product_name)-(a.product.product_name < b.product.product_name)
    },
    {
      title: 'Cantidad',
      dataIndex: 'amount',
      align:'center',
      key: 'amount',
      width: '20%',
      sorter: (a, b) => a.amount - b.amount
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      align:'center',
      // fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_event_product;
    }
    class_name() {
        return 'Event_product'
      }


   }

