/**Generate by ASGENS

 *@author Charlietyn
 *@date Sun Sep 06 18:31:24 GMT-04:00 2020
 *@time Sun Sep 06 18:31:24 GMT-04:00 2020
 */

import {email, integer, maxLength, minLength, required, url as urlvalid} from 'vuelidate/lib/validators'
import moment from 'moment'
import BaseModel from '../../base.model'

const url = 'managment/contacts'

export default class Contacts extends BaseModel {

  id_contact
  contact_legal_type_id
  contact_full_name
  contact_commercial_name
  contact_type_id
  contact_email
  contact_phone
  contact_cellphone
  contact_website
  contact_address
  contact_postal_code
  country_id
  state_id
  contact_internal_reference
  language_id
  currency_id
  payment_type_id
  contact_status_id
  contact_picture
  created_at
  updated_at
  club_member_id

  constructor (attributes = null) {
    super()
    if (attributes != null) {
      this.id_contact = attributes.id_contact || undefined
      this.contact_legal_type_id = attributes.contact_legal_type_id || ''
      this.contact_full_name = attributes.contact_full_name || ''
      this.contact_commercial_name = attributes.contact_commercial_name || ''
      this.contact_type_id = attributes.contact_type_id || ''
      this.contact_email = attributes.contact_email || ''
      this.contact_phone = attributes.contact_phone || null
      this.contact_cellphone = attributes.contact_cellphone || null
      this.contact_website = attributes.contact_website || null
      this.contact_address = attributes.contact_address || ''
      this.contact_postal_code = attributes.contact_postal_code || ''
      this.country_id = attributes.country_id || ''
      this.state_id = attributes.state_id || ''
      this.contact_internal_reference = attributes.contact_internal_reference || null
      this.language_id = attributes.language_id || ''
      this.currency_id = attributes.currency_id || ''
      this.payment_type_id = attributes.payment_type_id || ''
      this.contact_status_id = attributes.contact_status_id || ''
      this.contact_picture = attributes.contact_picture || 'contact_picture.jpg'
      this.created_at = attributes.created_at ? moment(attributes.created_at).format() : moment().format()
      this.updated_at = attributes.updated_at ? moment(attributes.updated_at).format() : moment().format()
      this.club_member_id = attributes.club_member_id|| null
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_contact = attributes.id_contact
      this.contact_legal_type_id = attributes.contact_legal_type_id
      this.contact_full_name = attributes.contact_full_name
      this.contact_commercial_name = attributes.contact_commercial_name
      this.contact_type_id = attributes.contact_type_id
      this.contact_email = attributes.contact_email
      this.contact_phone = attributes.contact_phone
      this.contact_cellphone = attributes.contact_cellphone
      this.contact_website = attributes.contact_website
      this.contact_address = attributes.contact_address
      this.contact_postal_code = attributes.contact_postal_code
      this.country_id = attributes.country_id
      this.state_id = attributes.state_id
      this.contact_internal_reference = attributes.contact_internal_reference
      this.language_id = attributes.language_id
      this.currency_id = attributes.currency_id
      this.payment_type_id = attributes.payment_type_id
      this.contact_status_id = attributes.contact_status_id
      this.contact_picture = attributes.contact_picture
      this.created_at = moment(attributes.created_at)
      this.updated_at = moment(attributes.updated_at)
      this.club_member_id = attributes.club_member_id
    }
  }

  static validations = {
    contacts: {
      contact_legal_type_id: {
        required,
        integer,
      },

      contact_full_name: {},
      contact_commercial_name: {},
      contact_type_id: {
        required,
        integer,
      },
      contact_email: {
        required,
        email,
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_contact, contact_email} = object
          const _scenario = id_contact ? 'update' : 'create'
          const params = {id_contact, contact_email}
          const validation = await Contacts.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        }
      },
      contact_phone: {},
      contact_cellphone: {
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_contact, contact_cellphone} = object
          const _scenario = id_contact ? 'update' : 'create'
          const params = {id_contact, contact_cellphone}
          const validation = await Contacts.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        }
      },
      contact_website: {
        urlvalid,
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_contact, contact_website} = object
          const _scenario = id_contact ? 'update' : 'create'
          const params = {id_contact, contact_website}
          const validation = await Contacts.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        }
      },
      contact_address: {
        required,
      },
      contact_postal_code: {
        integer,
        minLength: minLength(5),
        maxLength: maxLength(7)
      },
      country_id: {
        required,
        integer,
      },
      state_id: {
        required,
        integer,
      },
      contact_internal_reference: {
        maxLength: maxLength(65535),
      },
      language_id: {
        required,
        integer,
      },
      currency_id: {
        required,
        integer,
      },
      payment_type_id: {
        required,
        integer,
      },
      contact_status_id: {
        required,
        integer,
      },
      contact_picture: {
        maxLength: maxLength(40),
      },
      created_at: {
        required,
      },
      updated_at: {
        required,
      },
      product_desc: {
        required,
      },

    },
  }

  static getImageURL (contact_picture) {
    contact_picture=contact_picture|| 'contact_picture.jpg';
    return process.env.BASE_URL + 'assets/contacts/' + contact_picture
  }

  static feedbacks = {
    contacts: {
      id_contact: {
        isUnique: 'Ya existe un elemento con este ID'

      },
      contact_email: {
        isUnique: 'Ya existe un contacto con este email'

      },
      contact_cellphone: {
        isUnique: 'Ya existe un contacto con este teléfono móvil'

      },
      contact_website: {
        isUnique: 'Ya existe un contacto con este sitio web',
        urlvalid: 'Ingrese una url válida'
      },
    },
  }

  static columns = [
    {
      title: '',
      key: 'contact_picture',
      scopedSlots: {customRender: 'contact_picture'},
      width: '3%',
      align: 'center',
      sorter: (a, b) => (a.contact_picture > b.contact_picture) - (a.contact_picture < b.contact_picture)
    },
    {
      title: 'Forma jurídica',
      dataIndex: 'legal_types.legal_type_siglas',
      key: 'legal_types.id_legal_type',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.legal_types.legal_type_siglas > b.legal_types.legal_type_siglas) - (a.legal_types.legal_type_siglas < b.legal_types.legal_type_siglas)
    },
    {
      title: 'Nombre completo',
      dataIndex: 'contact_full_name',
      key: 'contact_full_name',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.contact_full_name > b.contact_full_name) - (a.contact_full_name < b.contact_full_name)
    },
    {
      title: 'Nombre comecial',
      dataIndex: 'contact_commercial_name',
      key: 'contact_commercial_name',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.contact_commercial_name > b.contact_commercial_name) - (a.contact_commercial_name < b.contact_commercial_name)
    },
    {
      title: 'Tipo de contacto',
      scopedSlots: {
        customRender: 'contact_type'
      },
      key: 'contact_types.id_contact_type',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.contact_types.contact_type_siglas > b.contact_types.contact_type_siglas) - (a.contact_types.contact_type_siglas < b.contact_types.contact_type_siglas)
    },
    {
      title: 'Email',
      dataIndex: 'contact_email',
      key: 'contact_email',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.contact_email > b.contact_email) - (a.contact_email < b.contact_email)
    },
    {
      title: 'Teléfono',
      dataIndex: 'contact_phone',
      key: 'contact_phone',
      width: '10%',
      align: 'center',
      sorter: (a, b) => (a.contact_phone > b.contact_phone) - (a.contact_phone < b.contact_phone)
    },
    {
      title: 'Teléfono móvil',
      dataIndex: 'contact_cellphone',
      key: 'contact_cellphone',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.contact_cellphone > b.contact_cellphone) - (a.contact_cellphone < b.contact_cellphone)
    },
    {
      title: 'Sitio web',
      dataIndex: 'contact_website',
      key: 'contact_website',
      width: '20%',
      align: 'center',
      sorter: (a, b) => (a.contact_website > b.contact_website) - (a.contact_website < b.contact_website)
    },
    {
      title: 'Dirección',
      dataIndex: 'contact_address',
      key: 'contact_address',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.contact_address > b.contact_address) - (a.contact_address < b.contact_address)
    },
    {
      title: 'Código postal',
      dataIndex: 'contact_postal_code',
      key: 'contact_postal_code',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.contact_postal_code > b.contact_postal_code) - (a.contact_postal_code < b.contact_postal_code)
    },
    {
      title: 'País',
      dataIndex: 'countries.country_name',
      key: 'countries.id_country',
      width: '10%',
      align: 'center',
      sorter: (a, b) => (a.countries.country_name > b.countries.country_name) - (a.countries.country_name < b.countries.country_name)
    },
    {
      title: 'Provincia',
      dataIndex: 'states.state_desc',
      key: 'states.id_state',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.states.state_desc > b.states.state_desc) - (a.states.state_desc < b.states.state_desc)
    },
    {
      title: 'Referencias internas',
      dataIndex: 'contact_internal_reference',
      key: 'contact_internal_reference',
      width: '20%',
      align: 'center',
      sorter: (a, b) => (a.contact_internal_reference > b.contact_internal_reference) - (a.contact_internal_reference < b.contact_internal_reference)
    },
    {
      title: 'Idioma',
      dataIndex: 'languages.language_desc',
      key: 'languages.id_language',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.languages.language_desc > b.languages.language_desc) - (a.languages.language_desc < b.languages.language_desc)
    },
    {
      title: 'Moneda',
      dataIndex: 'currencies.currency_siglas',
      key: 'currencies.id_currency',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.currencies.currency_siglas > b.currencies.currency_siglas) - (a.currencies.currency_siglas < b.currencies.currency_siglas)
    },
    {
      title: 'Tipo de pago',
      dataIndex: 'payment_types.payment_type_siglas',
      key: 'payment_types.id_payment_type',
      width: 100,
      align: 'center',
      sorter: (a, b) => (a.payment_types.payment_type_siglas > b.payment_types.payment_type_siglas) - (a.payment_types.payment_type_siglas < b.payment_types.payment_type_siglas)
    },
    {
      title: 'Estado',
      dataIndex: 'contact_status.contact_status_siglas',
      key: 'contact_status.id_contact_status',
      width: '8%',
      align: 'center',
      sorter: (a, b) => (a.contact_status.contact_status_siglas > b.contact_status.contact_status_siglas) - (a.contact_status.contact_status_siglas < b.contact_status.contact_status_siglas)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: 100,
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: 100,
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: 100,
      fixed: 'right',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_contact
  }

  class_name () {
    return 'Contacts'
  }
}

