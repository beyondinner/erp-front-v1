/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Sep 07 15:53:43 GMT-04:00 2020
 *@time Mon Sep 07 15:53:43 GMT-04:00 2020
 */
import {email, integer, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'
import Users from '../security/users.model'
import moment from 'moment'

const url = 'managment/club_members'

export default class Club_members extends BaseModel {

  id_club_members
  club_member_code
  club_members_name
  club_member_last_name
  club_members_dni
  club_members_birthday
  club_members_email
  club_members_phone
  club_members_address
  clum_members_currency
  club_members_picture
  created_at
  updated_at
  id_languages
  id_adviser
  id_state
  id_country
  postal_code

  constructor (attributes = null) {
    super()
    if (attributes != null) {
      this.id_club_members = attributes.id_club_members || undefined
      this.club_member_code = attributes.club_member_code || ''
      this.club_members_name = attributes.club_members_name || ''
      this.club_member_last_name = attributes.club_member_last_name || ''
      this.club_members_dni = attributes.club_members_dni || ''
      this.club_members_birthday = attributes.club_members_birthday ? moment(attributes.club_members_birthday) : null
      this.club_members_email = attributes.club_members_email || ''
      this.club_members_phone = attributes.club_members_phone || ''
      this.club_members_address = attributes.club_members_address || ''
      this.clum_members_currency = attributes.clum_members_currency || ''
      this.id_languages = attributes.id_languages || null
      this.id_user = attributes.id_user || null
      this.created_at = attributes.created_at ? moment(attributes.created_at).format() : moment().format()
      this.updated_at = attributes.updated_at ? moment(attributes.updated_at).format() : moment().format()
      this.id_adviser = attributes.id_adviser || ''
      this.id_state = attributes.id_state|| null
      this.id_country = attributes.id_country|| null
      this.postal_code = attributes.postal_code|| ''
      this.club_members_picture = attributes.club_members_picture|| ''
    } else {
      this.club_members_picture = 'club_members_picture.jpg'
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {
      this.id_club_members = attributes.id_club_members
      this.club_member_code = attributes.club_member_code
      this.club_members_name = attributes.club_members_name
      this.club_member_last_name = attributes.club_member_last_name
      this.club_members_dni = attributes.club_members_dni
      this.club_members_birthday = moment(attributes.club_members_birthday)
      this.club_members_email = attributes.club_members_email
      this.club_members_phone = attributes.club_members_phone
      this.club_members_address = attributes.club_members_address
      this.clum_members_currency = attributes.clum_members_currency
      this.club_members_picture = attributes.club_members_picture
      this.created_at = moment(attributes.created_at).format()
      this.updated_at = moment(attributes.updated_at).format()
      this.id_languages = attributes.id_languages
      this.id_user = attributes.id_user
      this.id_adviser = attributes.id_adviser
      this.id_state = attributes.id_state
      this.id_country = attributes.id_country
      this.postal_code = attributes.postal_code
    }
  }

  static validations = {
    club_members: {
      club_members_name: {
        required,
      },
      club_member_last_name: {},
      club_members_dni: {
        required,
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_club_members, club_members_dni} = object
          const _scenario = id_club_members ? 'update' : 'create'
          const params = {id_club_members, club_members_dni}
          const validation = await Club_members.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        },
        valid_nif (value, object) {
          return true
          let numero
          let letr
          let letra
          let expresion_regular_dni

          expresion_regular_dni = /^\d{8}[a-zA-Z]$/

          if (expresion_regular_dni.test(value) == true) {
            numero = value.substr(0, value.length - 1)
            letr = value.substr(value.length - 1, 1)
            numero = numero % 23
            letra = 'TRWAGMYFPDXBNJZSQVHLCKET'
            letra = letra.substring(numero, numero + 1)
            if (letra != letr.toUpperCase()) {
              return false
            } else {
              return true
            }
          } else {
            return false
          }
        }

      },
      id_state: {
        integer,
      },
      id_country: {
        integer,
      },
      postal_code: {
      },
      club_members_birthday: {
        required,
      },
      club_members_email: {
        required,
        email,
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_club_members, club_members_email, id_user} = object
          const _scenario = id_club_members ? 'update' : 'create'
          const params = {id_club_members, club_members_email}
          const params_user = {id_user: id_user, email: club_members_email}
          const validation = await Club_members.validate({...params, _scenario, _specific})
          const user_validation = await Users.validate({...params_user, _scenario, _specific})
          return !validation.data || !user_validation.data ? false : validation.data.success && user_validation.data.success
        }
      },
      club_members_phone: {
        required,
      },
      club_members_address: {
        required,
      },
      club_members_picture: {},
      created_at: {
        required,
      },
      updated_at: {
        required,
      },
      club_member_code: {},
      clum_members_currency: {
        required,
        integer,
      },
      id_languages: {
        required,
        integer,
      },
    },
  }

  get_full_name () {
    return this.club_members_name?this.club_members_name + ' ' + this.club_member_last_name:''
  }

  static feedbacks = {
    club_members: {
      id_club_members: {
        isUnique: 'This id_club_members has been taken'

      },
      club_members_dni: {
        isUnique: 'Este dni ya esta siendo usado',
        valid_nif: 'Entre un dni válido'

      },
      club_members_email: {
        isUnique: 'Este correo ya esta asignado a algún usuario'

      },
    },
  }

  static getImageURL (club_members_picture) {
    club_members_picture = club_members_picture || 'user.jpg'
    return process.env.BASE_URL + 'assets/users/' + club_members_picture
  }

  static columns = [
    {
      title: '',
      key: 'club_members_picture',
      hide_on_export:true,
      scopedSlots: {customRender: 'club_members_picture'},
      width: '5%',
      align: 'center',
      sorter: (a, b) => (a.club_members_picture > b.club_members_picture) - (a.club_members_picture < b.club_members_picture)
    },
    {
      title: 'Código',
      key: 'club_member_code',
      scopedSlots: {
        customRender: 'code'
      },
      width: '10%',
      align: 'center',
      sorter: (a, b) => (a.club_member_code > b.club_member_code) - (a.club_member_code < b.club_member_code)
    },
    {
      title: 'Nombre ',
      dataIndex: 'club_members_name',
      key: 'club_members_name',
      width: '12%',
      align: 'center',
      sorter: (a, b) => (a.club_members_name > b.club_members_name) - (a.club_members_name < b.club_members_name)
    },
    {
      title: 'Apellidos ',
      dataIndex: 'club_member_last_name',
      key: 'club_member_last_name',
      width: '13%',
      align: 'center',
      sorter: (a, b) => (a.club_member_last_name > b.club_member_last_name) - (a.club_member_last_name < b.club_member_last_name)
    },
    {
      title: 'DNI',
      dataIndex: 'club_members_dni',
      key: 'club_members_dni',
      width: '10%',
      align: 'center',
      sorter: (a, b) => (a.club_members_dni > b.club_members_dni) - (a.club_members_dni < b.club_members_dni)
    },
    {
      title: 'Nacimiento',
      customRender: (element) => {
        return moment(element.club_members_birthday).format('DD/MM/YYYY')
      },
      key: 'club_members_birthday',
      width: '15%',
      align: 'center',
      sorter: (a, b) => a.club_members_birthday - b.club_members_birthday
    },
    {
      title: 'Correo',
      scopedSlots: {
        customRender: 'email'
      },
      key: 'club_members_email',
      use_filter:true,
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.club_members_email > b.club_members_email) - (a.club_members_email < b.club_members_email)
    },
    {
      title: 'Idioma',
      dataIndex:'languages.language_desc',
      key: 'languages.language_desc',
      use_filter:true,
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.languages.language_desc > b.languages.language_desc) - (a.languages.language_desc < b.languages.language_desc)
    },
    {
      title: 'Cuenta',
      // customRender: (element) => {
      //   return element.clum_members_currency + '€'
      // },
      scopedSlots: {
        customRender: 'amount'
      },
      key: 'clum_members_currency',
      width: '10%',
      align: 'center',
      sorter: (a, b) => a.clum_members_currency - b.clum_members_currency
    },
    {
      title: 'State',
      dataIndex: 'state.state_name',
      align:'center',
      key: 'state.state_name',
      width: '20%',
      sorter: (a, b) =>  (a.state.state_name > b.state.state_name)-(a.state.state_name < b.state.state_name)
    },
    {
      title: 'País',
      dataIndex: 'country.country_name',
      align:'center',
      key: 'country.country_name',
      width: '20%',
      sorter: (a, b) =>  (a.country.country_name > b.country.country_name)-(a.country.country_name < b.country.country_name)
    },
    {
      title: 'Postal_code',
      dataIndex: 'postal_code',
      align:'center',
      key: 'postal_code',
      width: '20%',
      sorter: (a, b) =>  (a.postal_code > b.postal_code)-(a.postal_code < b.postal_code)
    },
    {
      title: 'Teléfono',
      scopedSlots: {
        customRender: 'phone'
      },
      key: 'club_members_phone',
      width: '12%',
      align: 'center',
      sorter: (a, b) => (a.club_members_phone > b.club_members_phone) - (a.club_members_phone < b.club_members_phone)
    },
    {
      title: 'Dirección',
      dataIndex: 'club_members_address',
      key: 'club_members_address',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.club_members_address > b.club_members_address) - (a.club_members_address < b.club_members_address)
    },
    {
      title: 'Asesor',
      dataIndex: 'adviser.nombre_usuario',
      align: 'center',
      key: 'adviser.adviser',
      sorter: (a, b) => (a.adviser.adviser > b.adviser.adviser) - (a.adviser.adviser < b.adviser.adviser),
      width: '10%',
    },
    {
      title: 'Fecha creación',
      dataIndex: 'created_at',
      key: 'created_at',
      width: 120,
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Updated_at',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '130px',
      fixed: 'right',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_club_members
  }

  class_name () {
    return 'Club_members'
  }

}

