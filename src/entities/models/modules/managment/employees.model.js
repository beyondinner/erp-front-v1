/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import { required, integer, between, maxLength, minLength, email } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/employees';

    export default class Employees extends BaseModel {

       id_employee
       employee_name
       employee_last_name
       employee_nif
       employee_email_owned
       employee_email_work
       employee_address
       employee_postal_code
       country_id
       state_id
       language_id
       payment_type_id
       currency_id
       work_country_id
       employee_work_zone
       employee_picture
       employee_attachment
       employee_status_id
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_employee = attributes.id_employee|| undefined
        this.employee_name = attributes.employee_name|| ''
        this.employee_last_name = attributes.employee_last_name|| ''
        this.employee_nif = attributes.employee_nif|| ''
        this.employee_email_owned = attributes.employee_email_owned|| ''
        this.employee_email_work = attributes.employee_email_work|| ''
        this.employee_address = attributes.employee_address|| ''
        this.employee_postal_code = attributes.employee_postal_code|| ''
        this.country_id = attributes.country_id|| ''
        this.state_id = attributes.state_id|| ''
        this.language_id = attributes.language_id|| ''
        this.payment_type_id = attributes.payment_type_id|| ''
        this.currency_id = attributes.currency_id|| ''
        this.work_country_id = attributes.work_country_id|| ''
        this.employee_work_zone = attributes.employee_work_zone|| ''
        this.employee_picture = attributes.employee_picture || 'employee.jpg'
        this.employee_attachment = attributes.employee_attachment|| ''
        this.employee_status_id = attributes.employee_status_id|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment().format()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment().format()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_employee = attributes.id_employee
        this.employee_name = attributes.employee_name
        this.employee_last_name = attributes.employee_last_name
        this.employee_nif = attributes.employee_nif
        this.employee_email_owned = attributes.employee_email_owned
        this.employee_email_work = attributes.employee_email_work
        this.employee_address = attributes.employee_address
        this.employee_postal_code = attributes.employee_postal_code
        this.country_id = attributes.country_id
        this.state_id = attributes.state_id
        this.language_id = attributes.language_id
        this.payment_type_id = attributes.payment_type_id
        this.currency_id = attributes.currency_id
        this.work_country_id = attributes.work_country_id
        this.employee_work_zone = attributes.employee_work_zone
        this.employee_picture = attributes.employee_picture
        this.employee_attachment = attributes.employee_attachment
        this.employee_status_id = attributes.employee_status_id
        debug
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      employees: {
        employee_name: {
          required,
          maxLength: maxLength(50)
        },
        employee_last_name: {
          required,
          maxLength: maxLength(50)
        },
        employee_nif: {
          required,
          maxLength: maxLength(11),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_employee,employee_nif}=object
               const _scenario=id_employee?'update':'create'
               const params={id_employee,employee_nif}
               const validation= await Employees.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        },
          valid_nif(value,object){
            let numero;
            let letr;
            let letra;
            let expresion_regular_dni;

            expresion_regular_dni = /^\d{8}[a-zA-Z]$/;

            if (expresion_regular_dni.test(value) == true) {
              numero = value.substr(0, value.length - 1);
              letr = value.substr(value.length - 1, 1);
              numero = numero % 23;
              letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
              letra = letra.substring(numero, numero + 1);
              if (letra != letr.toUpperCase()) {
                return false
              } else {
                return true;
              }
            } else {
              return false
            }
          }
        },
        employee_email_owned: {
          email,
            // async isUnique(value,object) {
            //      if(!value)
            //          return true
            //      const _specific=true
            //      const {id_employee,employee_email_owned}=object
            //      const _scenario=id_employee?'update':'create'
            //      const params={id_employee,employee_email_owned}
            //      const validation= await Employees.validate({...params,_scenario,_specific})
            //      return !validation.data?false:validation.data.success
            // }
        },
        employee_email_work: {
          required,
          email,
          async isUnique(value,object) {
             if(!value)
                 return true
             const _specific=true
             const {id_employee,employee_email_work}=object
             const _scenario=id_employee?'update':'create'
             const params={id_employee,employee_email_work}
             const validation= await Employees.validate({...params,_scenario,_specific})
             return !validation.data?false:validation.data.success
          }
        },
        employee_address: {
          required,
          maxLength: maxLength(50)
        },
        employee_postal_code: {
          required,
          integer,
          maxLength: maxLength(6)
        },
        country_id: {
          required,
          integer
        },
        state_id: {
          required,
          integer
        },
        language_id: {
          required,
          integer
        },
        payment_type_id: {
          required,
          integer
        },
        currency_id: {
          required,
          integer
        },
        work_country_id: {
          required,
          integer
        },
        employee_work_zone: {
          required,
          maxLength: maxLength(50)
        },
        employee_picture: {
          maxLength: maxLength(40)
        },
        employee_attachment: {
          maxLength: maxLength(40),
        },
        employee_status_id: {
          required,
          integer
        },
        created_at: {
          required
        },
        updated_at: {
          required
        },
      },
    }

      static getImageURL (employee_picture) {
        return process.env.BASE_URL + 'assets/employees/' + employee_picture
      }

      static getDocumentURL (employee_attachment) {
        return process.env.BASE_URL + 'assets/employees/' + employee_attachment
      }

    static feedbacks = {
      employees: {
        id_employee: {
          isUnique: 'Ya existe un empleado con este ID'

        },
        employee_nif: {
          isUnique: 'Ya existe un empleado con este nif',
          valid_nif: 'Entre un nif válido',

        },
        employee_email_work: {
          isUnique: 'Ya existe un empleado con este'

        },
      },
    }

  static columns = [
    {
      title: '',
      key: 'employee_picture',
      scopedSlots: {customRender: 'employee_picture'},
      width: '3%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_picture > b.employee_picture)-(a.employee_picture < b.employee_picture)
    },
    {
      title: 'Nombre',
      dataIndex: 'employee_name',
      key: 'employee_name',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_name > b.employee_name)-(a.employee_name < b.employee_name)
    },
    {
      title: 'Apellidos',
      dataIndex: 'employee_last_name',
      key: 'employee_last_name',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_last_name > b.employee_last_name)-(a.employee_last_name < b.employee_last_name)
    },
    {
      title: 'NIF',
      dataIndex: 'employee_nif',
      key: 'employee_nif',
      width: '10%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_nif > b.employee_nif)-(a.employee_nif < b.employee_nif)
    },
    {
      title: 'Email personal',
      dataIndex: 'employee_email_owned',
      key: 'employee_email_owned',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_email_owned > b.employee_email_owned)-(a.employee_email_owned < b.employee_email_owned)
    },
    {
      title: 'Email',
      dataIndex: 'employee_email_work',
      key: 'employee_email_work',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_email_work > b.employee_email_work)-(a.employee_email_work < b.employee_email_work)
    },
    {
      title: 'Dirección',
      dataIndex: 'employee_address',
      key: 'employee_address',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_address > b.employee_address)-(a.employee_address < b.employee_address)
    },
    {
      title: 'Código postal',
      dataIndex: 'employee_postal_code',
      key: 'employee_postal_code',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_postal_code > b.employee_postal_code)-(a.employee_postal_code < b.employee_postal_code)
    },
    {
      title: 'País',
      dataIndex: 'country.country_siglas',
      key: 'countries.id_country',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.country.country_siglas > b.country.country_siglas)-(a.country.country_siglas < b.country.country_siglas)
    },
    {
      title: 'Estado/Provincia',
      dataIndex: 'states.state_siglas',
      key: 'states.id_state',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.states.state_siglas > b.states.state_siglas)-(a.states.state_siglas < b.states.state_siglas)
    },
    {
      title: 'Idioma',
      dataIndex: 'language.language_siglas',
      key: 'language.id_language',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.language.language_siglas > b.language.language_siglas)-(a.language.language_siglas < b.language.language_siglas)
    },
    {
      title: 'Tipo de pago',
      dataIndex: 'payment_type.payment_type_siglas',
      key: 'payment_types.id_payment_type',
      width: '12%',
      align: 'center',
      sorter: (a, b) =>  (a.payment_type.payment_type_siglas > b.payment_type.payment_type_siglas)-(a.payment_type.payment_type_siglas < b.payment_type.payment_type_siglas)
    },
    {
      title: 'Moneda',
      dataIndex: 'currency.currency_siglas',
      key: 'currencies.id_currency',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.currency.currency_siglas > b.currency.currency_siglas)-(a.currency.currency_siglas < b.currency.currency_siglas)
    },
    {
      title: 'País de trabajo',
      dataIndex: 'work_country.country_siglas',
      key: 'countries.id_country',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.work_country.country_siglas > b.work_country.country_siglas)-(a.work_country.country_siglas < b.work_country.country_siglas)
    },
    {
      title: 'Zona de trabajo',
      dataIndex: 'employee_work_zone',
      key: 'employee_work_zone',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_work_zone > b.employee_work_zone)-(a.employee_work_zone < b.employee_work_zone)
    },
    {
      title: 'Doc',
      key: 'employee_attachment',
      scopedSlots: {customRender: 'employee_attachment'},
      width: '10%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_attachment > b.employee_attachment)-(a.employee_attachment < b.employee_attachment)
    },
    {
      title: 'Estado',
      dataIndex: 'employee_status.employee_status_siglas',
      key: 'employee_status.id_employee_status',
      width: '12%',
      align: 'center',
      sorter: (a, b) =>  (a.employee_status.employee_status_siglas > b.employee_status.employee_status_siglas)-(a.employee_status.employee_status_siglas < b.employee_status.employee_status_siglas)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_employee;
    }

      class_name() {
        return 'Employees'
      }

   }

