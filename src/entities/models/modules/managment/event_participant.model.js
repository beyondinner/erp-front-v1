/**Generate by ASGENS
*@author Charlietyn
*@date Thu Oct 08 21:28:07 GMT-04:00 2020
*@time Thu Oct 08 21:28:07 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  const url = 'managment/event_participant';

    export default class Event_participant extends BaseModel {

       id_event_participant
       id_event
       id_participant
       days
       consume_products
       id_payment_types

    constructor(attributes = null) {
      super();

      if (attributes != null) {
        this.id_event_participant = attributes.id_event_participant|| undefined
        this.id_event = attributes.id_event|| ''
        this.id_participant = attributes.id_participant|| ''
        this.days = attributes.days|| ''
        this.consume_products = attributes.consume_products>0|| false
        this.id_payment_types = attributes.id_payment_types|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_event_participant = attributes.id_event_participant
        this.id_event = attributes.id_event
        this.id_participant = attributes.id_participant
        this.days = attributes.days
        this.consume_products = attributes.consume_products
        this.id_payment_types = attributes.id_payment_types
      }
    }

    static validations = {
      event_participant: {
        id_event: {
          required,
          integer,
        },
        id_participant: {
          required,
          integer,
        },
        days: {
          required,
          integer,
        },
        consume_products: {
          required,
        },
        id_payment_types: {
          required,
          integer,
        },
      },
    }

    static feedbacks = {
      event_participant: {
      id_event_participant: {
        isUnique: 'This id_event_participant has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Event',
      dataIndex: 'event.event_name',
      key: 'event.event_name',
      align:'center',
      width: '20%',
      sorter: (a, b) =>  (a.event.event_name > b.event.event_name)-(a.event.event_name < b.event.event_name)
    },
    {
      title: 'Participante',
      customRender(element){
        return  element.participant.participant_name+' '+element.participant.apellidos
      },
      key: 'participant.participant_name',
      align:'center',
      width: '25%',
      sorter: (a, b) =>  (a.participant.participant_name > b.participant.participant_name)-(a.participant.participant_name < b.participant.participant_name)
    },
    {
      title: 'Días',
      dataIndex: 'days',
      key: 'days',
      width: '10%',
      align:'center',
      sorter: (a, b) => a.days - b.days
    },
    {
      title: 'Consume',
      customRender(element){
        return  element.consume_products==='0'?'No':'Si'
      },
      key: 'consume_products',
      align:'center',
      width: '10%',
    },
    {
      title: 'Tipo de pago',
      dataIndex: 'payment_types.payment_type_siglas',
      align:'center',
      key: 'payment_types.payment_type_siglas',
      width: '20%',
      sorter: (a, b) =>  (a.payment_types.payment_type_siglas > b.payment_types.payment_type_siglas)-(a.payment_types.payment_type_siglas < b.payment_types.payment_type_siglas)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      align:'center',
      // fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_event_participant;
    }
    class_name() {
        return 'Event_participant'
      }


   }

