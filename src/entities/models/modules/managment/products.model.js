/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import { required, integer, between, maxLength, minLength } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/products';

    export default class Products extends BaseModel {

       id_product
       product_name
       product_desc
       contact_id
       product_tax_buy
       product_cost
       product_subtotal
       product_tax_sale
       product_total
       product_stock
       product_sku
       product_bar_code
       product_factory_code
       product_weigth
       product_status_id
       product_picture
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {
        this.id_product = attributes.id_product|| undefined
        this.product_name = attributes.product_name|| ''
        this.product_desc = attributes.product_desc|| ''
        this.contact_id = attributes.contact_id|| ''
        this.product_tax_buy = attributes.product_tax_buy|| ''
        this.product_cost = attributes.product_cost|| ''
        this.product_subtotal = attributes.product_subtotal|| ''
        this.product_tax_sale = attributes.product_tax_sale|| ''
        this.product_total = attributes.product_total|| ''
        this.product_stock = attributes.product_stock|| ''
        this.product_sku = attributes.product_sku|| ''
        this.product_bar_code = attributes.product_bar_code|| ''
        this.product_factory_code = attributes.product_factory_code|| ''
        this.product_weigth = attributes.product_weigth|| ''
        this.product_status_id = attributes.product_status_id|| ''
        this.product_picture = attributes.product_picture || 'products.jpg'
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment().format()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment().format()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {
        this.id_product = attributes.id_product
        this.product_name = attributes.product_name
        this.product_desc = attributes.product_desc
        this.contact_id = attributes.contact_id
        this.product_tax_buy = attributes.product_tax_buy
        this.product_cost = attributes.product_cost
        this.product_subtotal = attributes.product_subtotal
        this.product_tax_sale = attributes.product_tax_sale
        this.product_total = attributes.product_total
        this.product_stock = attributes.product_stock
        this.product_sku = attributes.product_sku
        this.product_bar_code = attributes.product_bar_code
        this.product_factory_code = attributes.product_factory_code
        this.product_weigth = attributes.product_weigth
        this.product_status_id = attributes.product_status_id
        this.product_picture = attributes.product_picture
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      products: {
        product_name: {
          required,
          maxLength: maxLength(50),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_product,product_name,contact_id}=object
               const _scenario=id_product?'update':'create'
               const params={id_product,product_name,contact_id}
               const validation= await Products.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        product_desc: {
          maxLength: maxLength(65535),
        },
        contact_id: {
          required,
          integer,
          /*async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_product,product_name,contact_id}=object
               const _scenario=id_product?'update':'create'
               const params={id_product,product_name,contact_id}
               const validation= await Products.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }*/
        },
        product_tax_buy: {
          required,
          integer
        },
        product_cost: {
          integer,
          required,
        },
        product_subtotal: {
          required,
          integer
        },
        product_tax_sale: {
          required,
          integer
        },
        product_total: {
          required,
          integer
        },
        product_stock: {
          required,
          integer
        },
        product_sku: {
        },
        product_bar_code: {
        },
        product_factory_code: {
        },
        product_weigth: {
          required,
          integer
        },
        product_status_id: {
          required,
          integer,
        },
        product_picture: {
          maxLength: maxLength(40),
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

      static getImageURL (product_picture) {
         product_picture=product_picture||'products.jpg'
        return process.env.BASE_URL + 'assets/products/' + product_picture
      }

    static feedbacks = {
      products: {
      id_product: {
        isUnique: 'Ya existe un producto con este ID'

      },
      product_name: {
        isUnique: 'Ya existe este proveedor con este producto'

      },
      contact_id: {
        isUnique: 'Ya existe este proveedor con este producto'

      },
      },
    }

  static columns = [
    {
      title: '',
      key: 'product_picture',
      scopedSlots: {customRender: 'product_picture'},
      width: '3%',
      align: 'center',
      sorter: (a, b) =>  (a.product_picture > b.product_picture)-(a.product_picture < b.product_picture)
    },
    {
      title: 'Nombre',
      dataIndex: 'product_name',
      key: 'product_name',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.product_name > b.product_name)-(a.product_name < b.product_name)
    },
    {
      title: 'SKU',
      dataIndex: 'product_sku',
      key: 'product_sku',
      width: '10%',
      align: 'center',
      sorter: (a, b) =>  (a.product_sku > b.product_sku)-(a.product_sku < b.product_sku)
    },
    {
      title: 'Stock',
      dataIndex: 'product_stock',
      key: 'product_stock',
      width: '10%',
      align: 'center'
    },
    {
      title: 'Descripción',
      dataIndex: 'product_desc',
      key: 'product_desc',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.product_desc > b.product_desc)-(a.product_desc < b.product_desc)
    },
    {
      title: 'Proveedor',
      dataIndex: 'contact.contact_full_name',
      key: 'contact.id_contact',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.contact.contact_full_name > b.contact.contact_full_name)-(a.contact.contact_full_name < b.contact.contact_full_name)
    },
    {
      title: 'Impuesto de compra',
      dataIndex: 'product_tax_buy',
      key: 'product_tax_buy',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Coste de Compra',
      dataIndex: 'product_cost',
      key: 'product_cost',
      width: '15%',
      align: 'center',
    },
    {
      title: 'Subtotal',
      dataIndex: 'product_subtotal',
      key: 'product_subtotal',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Impuesto de venta',
      dataIndex: 'product_tax_sale',
      key: 'product_tax_sale',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Precio de venta',
      dataIndex: 'product_total',
      key: 'product_total',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Código de barra',
      dataIndex: 'product_bar_code',
      key: 'product_bar_code',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.product_bar_code > b.product_bar_code)-(a.product_bar_code < b.product_bar_code)
    },
    {
      title: 'Código de fábrica',
      dataIndex: 'product_factory_code',
      key: 'product_factory_code',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.product_factory_code > b.product_factory_code)-(a.product_factory_code < b.product_factory_code)
    },
    {
      title: 'Cantidad (Peso)',
      dataIndex: 'product_weigth',
      key: 'product_weigth',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Estado',
      key: 'product_status.id_product_status',
      scopedSlots: {
        customRender: 'status'
      },
      width: '10%',
      align: 'center',
      sorter: (a, b) =>  (a.product_status.product_status_siglas > b.product_status.product_status_siglas)-(a.product_status.product_status_siglas < b.product_status.product_status_siglas)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: 100,
      align: 'center',
      fixed:'right',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_product;
    }

    class_name() {
        return 'Products'
      }

   }

