/**Generate by ASGENS
*@author Charlietyn 
*@date Mon Oct 26 00:59:43 GMT-04:00 2020  
*@time Mon Oct 26 00:59:43 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/contact_service_history';

    export default class Contact_service_history extends BaseModel {

       id_contact_service_history
       id_contact
       id_services
       contact_service_history_desc
       type
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_contact_service_history = attributes.id_contact_service_history|| undefined
        this.id_contact = attributes.id_contact|| ''
        this.id_services = attributes.id_services|| ''
        this.contact_service_history_desc = attributes.contact_service_history_desc|| ''
        this.type = attributes.type|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_contact_service_history = attributes.id_contact_service_history
        this.id_contact = attributes.id_contact
        this.id_services = attributes.id_services
        this.contact_service_history_desc = attributes.contact_service_history_desc
        this.type = attributes.type
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
      }
    }

    static validations = {
      contact_service_history: {
        id_contact: {
          required,
          integer,
        },
        id_services: {
          integer,
        },
        contact_service_history_desc: {
          maxLength: maxLength(65535),
        },
        type: {
          integer,
        },
        created_at: {
        },
        updated_at: {
        },
      },
    }

    static feedbacks = {
      contact_service_history: {
      id_contact_service_history: {
        isUnique: 'This id_contact_service_history has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Contact',
      dataIndex: 'contact.fiscal_name',
      align:'center',
      key: 'contact.fiscal_name',
      width: '20%',
      sorter: (a, b) =>  (a.contact.fiscal_name > b.contact.fiscal_name)-(a.contact.fiscal_name < b.contact.fiscal_name)
    },
    {
      title: 'Id_services',
      dataIndex: 'id_services',
      align:'center',
      key: 'id_services',
      width: '20%',
      sorter: (a, b) => a.id_services - b.id_services
    },
    {
      title: 'Contact_service_history_desc',
      dataIndex: 'contact_service_history_desc',
      align:'center',
      key: 'contact_service_history_desc',
      width: '20%',
      sorter: (a, b) =>  (a.contact_service_history_desc > b.contact_service_history_desc)-(a.contact_service_history_desc < b.contact_service_history_desc)
    },
    {
      title: 'Type',
      dataIndex: 'type',
      align:'center',
      key: 'type',
      width: '20%',
      sorter: (a, b) => a.type - b.type
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id_contact_service_history;
    }
    class_name() {
        return 'Contact_service_history'
      }
  

   }

