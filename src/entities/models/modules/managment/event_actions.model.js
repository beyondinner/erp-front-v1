/**Generate by ASGENS
*@author Charlietyn
*@date Thu Oct 15 08:21:27 GMT-04:00 2020
*@time Thu Oct 15 08:21:27 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/event_actions';

    export default class Event_actions extends BaseModel {

       id_event_actions
       date_at
       id_user
       action
       id_event
       description

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_event_actions = attributes.id_event_actions|| undefined
        this.date_at = attributes.date_at?moment(attributes.date_at):null
        this.id_user = attributes.id_user|| ''
        this.action = attributes.action|| ''
        this.id_event = attributes.id_event|| ''
        this.description = attributes.description|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_event_actions = attributes.id_event_actions
        this.date_at = moment(attributes.date_at)
        this.id_user = attributes.id_user
        this.action = attributes.action
        this.id_event = attributes.id_event
        this.description = attributes.description
      }
    }

    static validations = {
      event_actions: {
        date_at: {
          required,
        },
        id_user: {
          required,
          integer,
        },
        action: {
          required,
        },
        id_event: {
          required,
          integer,
        },
        date_init: {
          required,
        },
        date_end: {
          required,
        },
        description: {
          required,
          maxLength: maxLength(65535),
        },
      },
    }

    static feedbacks = {
      event_actions: {
      id_event_actions: {
        isUnique: 'This id_event_actions has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Date_at',
      dataIndex: 'date_at',
      align:'center',
      key: 'date_at',
      width: '20%',
      sorter: (a, b) => a.date_at - b.date_at
    },
    {
      title: 'User',
      dataIndex: 'user.nombre_usuario',
      align:'center',
      key: 'user.nombre_usuario',
      width: '20%',
      sorter: (a, b) =>  (a.user.nombre_usuario > b.user.nombre_usuario)-(a.user.nombre_usuario < b.user.nombre_usuario)
    },
    {
      title: 'Action',
      dataIndex: 'action',
      align:'center',
      key: 'action',
      width: '20%',
      sorter: (a, b) =>  (a.action > b.action)-(a.action < b.action)
    },
    {
      title: 'Event',
      dataIndex: 'event.event_name',
      align:'center',
      key: 'event.event_name',
      width: '20%',
      sorter: (a, b) =>  (a.event.event_name > b.event.event_name)-(a.event.event_name < b.event.event_name)
    },
    {
      title: 'Description',
      dataIndex: 'description',
      align:'center',
      key: 'description',
      width: '20%',
      sorter: (a, b) =>  (a.description > b.description)-(a.description < b.description)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_event_actions;
    }
    class_name() {
        return 'Event_actions'
      }


   }

