/**Generate by ASGENS
*@author Charlietyn
*@date Thu Oct 08 21:28:07 GMT-04:00 2020
*@time Thu Oct 08 21:28:07 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/clientes';

    export default class Clientes extends BaseModel {

       id
       fiscal_name
       comercial_name
       name
       last_name
       email
       phone
       use_products
       state
       wallet
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id = attributes.id|| undefined
        this.fiscal_name = attributes.fiscal_name|| ''
        this.comercial_name = attributes.comercial_name|| ''
        this.name = attributes.name|| ''
        this.last_name = attributes.last_name|| ''
        this.email = attributes.email|| ''
        this.phone = attributes.phone|| ''
        this.use_products = attributes.use_products|| ''
        this.state = attributes.state|| ''
        this.wallet = attributes.wallet|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id = attributes.id
        this.fiscal_name = attributes.fiscal_name
        this.comercial_name = attributes.comercial_name
        this.name = attributes.name
        this.last_name = attributes.last_name
        this.email = attributes.email
        this.phone = attributes.phone
        this.use_products = attributes.use_products
        this.state = attributes.state
        this.wallet = attributes.wallet
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
      }
    }

    static validations = {
      clientes: {
        fiscal_name: {
        },
        comercial_name: {
        },
        name: {
        },
        last_name: {
        },
        email: {
        },
        phone: {
        },
        use_products: {
          integer,
        },
        state: {
          required,
          integer,
        },
        wallet: {
          required,
          integer,
        },
        created_at: {
        },
        updated_at: {
        },
      },
    }

    static feedbacks = {
      clientes: {
      id: {
        isUnique: 'This id has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Fiscal_name',
      dataIndex: 'fiscal_name',
      key: 'fiscal_name',
      width: '20%',
      sorter: (a, b) =>  (a.fiscal_name > b.fiscal_name)-(a.fiscal_name < b.fiscal_name)
    },
    {
      title: 'Comercial_name',
      dataIndex: 'comercial_name',
      key: 'comercial_name',
      width: '20%',
      sorter: (a, b) =>  (a.comercial_name > b.comercial_name)-(a.comercial_name < b.comercial_name)
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '20%',
      sorter: (a, b) =>  (a.name > b.name)-(a.name < b.name)
    },
    {
      title: 'Last_name',
      dataIndex: 'last_name',
      key: 'last_name',
      width: '20%',
      sorter: (a, b) =>  (a.last_name > b.last_name)-(a.last_name < b.last_name)
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      width: '20%',
      sorter: (a, b) =>  (a.email > b.email)-(a.email < b.email)
    },
    {
      title: 'Phone',
      dataIndex: 'phone',
      key: 'phone',
      width: '20%',
      sorter: (a, b) =>  (a.phone > b.phone)-(a.phone < b.phone)
    },
    {
      title: 'Use_products',
      dataIndex: 'use_products',
      key: 'use_products',
      width: '20%',
      sorter: (a, b) =>  (a.use_products > b.use_products)-(a.use_products < b.use_products)
    },
    {
      title: 'State',
      dataIndex: 'state',
      key: 'state',
      width: '20%',
      sorter: (a, b) =>  (a.state > b.state)-(a.state < b.state)
    },
    {
      title: 'Wallet',
      dataIndex: 'wallet',
      key: 'wallet',
      width: '20%',
      sorter: (a, b) => a.wallet - b.wallet
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id;
    }
    class_name() {
        return 'Clientes'
      }


   }

