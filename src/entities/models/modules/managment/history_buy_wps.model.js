/**Generate by ASGENS
*@author Charlietyn 
*@date Sat Oct 31 01:02:06 GMT-04:00 2020  
*@time Sat Oct 31 01:02:06 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/history_buy_wps';

    export default class History_buy_wps extends BaseModel {

       id
       email
       products
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id = attributes.id|| undefined
        this.email = attributes.email|| ''
        this.products = attributes.products|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id = attributes.id
        this.email = attributes.email
        this.products = attributes.products
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
      }
    }

    static validations = {
      history_buy_wps: {
        email: {
          required,
        },
        products: {
          required,
        },
        created_at: {
        },
        updated_at: {
        },
      },
    }

    static feedbacks = {
      history_buy_wps: {
      id: {
        isUnique: 'This id has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Email',
      dataIndex: 'email',
      align:'center',
      key: 'email',
      width: '20%',
      sorter: (a, b) =>  (a.email > b.email)-(a.email < b.email)
    },
    {
      title: 'Products',
      dataIndex: 'products',
      align:'center',
      key: 'products',
      width: '20%',
      sorter: (a, b) =>  (a.products > b.products)-(a.products < b.products)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id;
    }
    class_name() {
        return 'History_buy_wps'
      }
  

   }

