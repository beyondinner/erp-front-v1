/**Generate by ASGENS
 *@author Charlietyn
 *@date Thu Oct 08 21:28:07 GMT-04:00 2020
 *@time Thu Oct 08 21:28:07 GMT-04:00 2020
 */
import {email, integer, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

const url = 'managment/participants'

export default class Participants extends BaseModel {

  id_participant
  participant_name
  apellidos
  participant_email
  participant_phone
  id_payment_types
  id_contact
  id_club_member

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_participant = attributes.id_participant || undefined
      this.participant_name = attributes.participant_name || ''
      this.apellidos = attributes.apellidos || ''
      this.participant_email = attributes.participant_email || ''
      this.participant_phone = attributes.participant_phone || ''
      this.id_payment_types = attributes.id_payment_types || ''
      this.id_contact = attributes.id_contact || ''
      this.id_club_member = attributes.id_club_member || ''
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_participant = attributes.id_participant
      this.participant_name = attributes.participant_name
      this.apellidos = attributes.apellidos
      this.participant_email = attributes.participant_email
      this.participant_phone = attributes.participant_phone
      this.id_payment_types = attributes.id_payment_types
      this.id_contact = attributes.id_contact
      this.id_club_member = attributes.id_club_member
    }
  }

  static validations = {
    participants: {
      participant_name: {
        required,
      },
      apellidos: {
        required,
      },
      participant_email: {
        required,
        email
      },
      participant_phone: {
        required,
      },
      id_payment_types: {
        integer,
      },
    },
  }

  static feedbacks = {
    participants: {
      id_participant: {
        isUnique: 'This id_participant has been taken'

      },
      participant_email: {
        isUnique: 'This participant_email has been taken'

      },
    },
  }

  static columns = [
    {
      title: 'Participant_name',
      dataIndex: 'participant_name',
      key: 'participant_name',
      width: '20%',
      sorter: (a, b) => (a.participant_name > b.participant_name) - (a.participant_name < b.participant_name)
    },
    {
      title: 'Apellidos',
      dataIndex: 'apellidos',
      key: 'apellidos',
      width: '20%',
      sorter: (a, b) => (a.apellidos > b.apellidos) - (a.apellidos < b.apellidos)
    },
    {
      title: 'Participant_email',
      dataIndex: 'participant_email',
      key: 'participant_email',
      width: '20%',
      sorter: (a, b) => (a.participant_email > b.participant_email) - (a.participant_email < b.participant_email)
    },
    {
      title: 'Participant_phone',
      dataIndex: 'participant_phone',
      key: 'participant_phone',
      width: '20%',
      sorter: (a, b) => a.participant_phone - b.participant_phone
    },
    {
      title: 'Payment_types',
      dataIndex: 'payment_types.payment_type_siglas',
      key: 'payment_types.payment_type_siglas',
      width: '20%',
      sorter: (a, b) => (a.payment_types.payment_type_siglas > b.payment_types.payment_type_siglas) - (a.payment_types.payment_type_siglas < b.payment_types.payment_type_siglas)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_participant
  }

  class_name () {
    return 'Participants'
  }

}

