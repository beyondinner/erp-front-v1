/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import { required, integer, between, maxLength, minLength } from 'vuelidate/lib/validators';
  import Vue from 'vue'
  import BaseModel from '../../base.model';
  import moment from 'moment';
  import * as model_factory from '../../index'

  const url = 'managment/payrolls';

    export default class Payrolls extends BaseModel {
       id_user
       id_payroll
       pId
       employee_id
       payroll_date
       payroll_desc
       payroll_concept
       payroll_account
       payroll_tax
       payroll_total
       payroll_tags
       payroll_default_account
       payroll_attachment
       payroll_status_id
       created_at
       updated_at

      constructor(attributes = null) {
      super();
      if (attributes != null) {
        this.id_user = this.userId
        this.id_payroll = attributes.id_payroll|| undefined
        this.employee_id = attributes.employee_id|| ''
        this.payroll_date = attributes.payroll_date?moment(attributes.payroll_date).format():null
        this.payroll_desc = attributes.payroll_desc|| ''
        this.payroll_concept = attributes.payroll_concept|| ''
        this.payroll_account = attributes.payroll_account|| ''
        this.payroll_tax = attributes.payroll_tax|| ''
        this.payroll_total = attributes.payroll_total|| ''
        this.payroll_tags = attributes.payroll_tags|| ''
        this.payroll_default_account = attributes.payroll_default_account|| ''
        this.payroll_attachment = attributes.payroll_attachment|| ''
        this.payroll_status_id = attributes.payroll_status_id|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment().format()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment().format()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {
        this.id_user = this.userId
        this.id_payroll = attributes.id_payroll
        this.employee_id = attributes.employee_id
        this.payroll_date = moment(attributes.payroll_date).format()
        this.payroll_desc = attributes.payroll_desc
        this.payroll_concept = attributes.payroll_concept
        this.payroll_account = attributes.payroll_account
        this.payroll_tax = attributes.payroll_tax
        this.payroll_total = attributes.payroll_total
        this.payroll_tags = attributes.payroll_tags
        this.payroll_default_account = attributes.payroll_default_account
        this.payroll_attachment = attributes.payroll_attachment
        this.payroll_status_id = attributes.payroll_status_id
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      payrolls: {
        id_user:{
          integer,
        },
        pId:{
          required,
        },
        employee_id: {
          required,
        },
        payroll_date: {
          required,
        },
        payroll_desc: {
          maxLength: maxLength(65535),
        },
        payroll_concept: {
          required,
        },
        payroll_account: {
          required,
        },
        payroll_tax: {
        },
        payroll_total: {
          required,
          integer,
        },
        payroll_tags: {
        },
        payroll_default_account: {
          required,
        },
        payroll_attachment: {
        },
        payroll_status_id: {
          integer,
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

      static getDocumentURL (payroll_attachment) {
        return process.env.BASE_URL + 'assets/payrolls/' + payroll_attachment
      }

    static feedbacks = {
      payrolls: {
        id_payroll: {
          isUnique: 'Ya existe una nómina con es ID'
        },
      },
    }

  static columns = [
    {
      title: 'Nombre',
      dataIndex: 'employees.employee_name',
      key: 'employees.employee_name',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.employees.employee_name > b.employees.employee_name)-(a.employees.employee_name < b.employees.employee_name)
    },
    {
      title: 'Apellidos',
      dataIndex: 'employees.employee_last_name',
      key: 'employees.employee_last_name',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.employees.employee_last_name > b.employees.employee_last_name)-(a.employees.employee_last_name < b.employees.employee_last_name)
    },
    {
      title: 'NIF',
      dataIndex: 'employees.employee_nif',
      key: 'employees.employee_nif',
      width: '12%',
      align: 'center',
      sorter: (a, b) =>  (a.employees.employee_nif > b.employees.employee_nif)-(a.employees.employee_nif < b.employees.employee_nif)
    },
    {
      title: 'Fecha',
      dataIndex: 'payroll_date',
      key: 'payroll_date',
      width: '12%',
      align: 'center',
      sorter: (a, b) => a.payroll_date - b.payroll_date
    },
    {
      title: 'Descripción',
      dataIndex: 'payroll_desc',
      key: 'payroll_desc',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.payroll_desc > b.payroll_desc)-(a.payroll_desc < b.payroll_desc)
    },
    {
      title: 'Concepto',
      dataIndex: 'payroll_concept',
      key: 'payroll_concept',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.payroll_concept > b.payroll_concept)-(a.payroll_concept < b.payroll_concept)
    },
    {
      title: 'Cuenta',
      dataIndex: 'payroll_account',
      key: 'payroll_account',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.payroll_account > b.payroll_account)-(a.payroll_account < b.payroll_account)
    },
    {
      title: 'Impuesto',
      dataIndex: 'payroll_tax',
      key: 'payroll_tax',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Total',
      dataIndex: 'payroll_total',
      key: 'payroll_total',
      width: '10%',
      align: 'center',
      sorter: (a, b) => a.payroll_total - b.payroll_total
    },
    {
      title: 'Etiqueta',
      dataIndex: 'payroll_tags',
      key: 'payroll_tags',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.payroll_tags > b.payroll_tags)-(a.payroll_tags < b.payroll_tags)
    },
    {
      title: 'Cuenta por defecto',
      dataIndex: 'payroll_default_account',
      key: 'payroll_default_account',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.payroll_default_account > b.payroll_default_account)-(a.payroll_default_account < b.payroll_default_account)
    },
    {
      title: 'Documento',
      dataIndex: 'payroll_attachment',
      key: 'payroll_attachment',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.payroll_attachment > b.payroll_attachment)-(a.payroll_attachment < b.payroll_attachment)
    },
    {
      title: 'Estado',
      dataIndex: 'payroll_status.payroll_status_siglas',
      key: 'payroll_status.id_payroll_status',
      width: '10%',
      align: 'center',
      sorter: (a, b) =>  (a.payroll_status.payroll_status_siglas > b.payroll_status.payroll_status_siglas)-(a.payroll_status.payroll_status_siglas < b.payroll_status.payroll_status_siglas)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_payroll;
    }

    get_employee_id() {
      return this.employee_id;
    }

      class_name() {
        return 'Payrolls'
      }

   }

