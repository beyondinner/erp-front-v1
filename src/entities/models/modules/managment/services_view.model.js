/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required, integer, between, maxLength, minLength, email } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/services';

    export default class Services extends BaseModel {

     id_service
     service_name
     service_code
     service_desc
     service_cost
     service_subtotal
     service_tax_sale
     service_total
     service_picture
     service_status_id
     created_at
     updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_service = attributes.id_service|| undefined
        this.service_name = attributes.service_name|| ''
        this.service_code = attributes.service_code|| ''
        this.service_desc = attributes.service_desc|| ''
        this.service_cost = attributes.service_cost|| 0
        this.service_subtotal = attributes.service_subtotal|| attributes.service_cost||0
        this.service_tax_sale = attributes.service_tax_sale|| 0
        this.service_total = attributes.service_total|| 0
        this.service_picture = attributes.service_picture|| 'services.jpg'
        this.service_status_id = attributes.service_status_id|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment().format()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment().format()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_service = attributes.id_service
        this.service_name = attributes.service_name
        this.service_code = attributes.service_code
        this.service_desc = attributes.service_desc
        this.service_cost = attributes.service_cost
        this.service_subtotal = attributes.service_subtotal|| attributes.service_cost||0
        this.service_tax_sale = attributes.service_tax_sale
        this.service_total = attributes.service_total
        this.service_picture = attributes.service_picture
        this.service_status_id = attributes.service_status_id
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      services: {
        service_name: {
          required,
          maxLength: maxLength(50),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_service,service_name}=object
               const _scenario=id_service?'update':'create'
               const params={id_service,service_name}
               const validation= await Services.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        service_code: {
          required,
        },
        service_desc: {
          maxLength: maxLength(65535),
        },
        service_cost: {
          required,
          integer
        },
        service_subtotal: {
          required,
          integer
        },
        service_tax_sale: {
          required,
          integer
        },
        service_total: {
          required,
          integer
        },
        service_picture: {
          maxLength: maxLength(40),
        },
        service_status_id: {
          required,
          integer,
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

      static getImageURL (service_picture) {
        service_picture=service_picture|| 'services.jpg'
        return process.env.BASE_URL + 'assets/services/' + service_picture
      }

    static feedbacks = {
      services: {
        id_service: {
          isUnique: 'Ya existe un servicio con este ID'
        },
        service_name: {
          isUnique: 'Ya existe un servicio con este nombre'
        },
      },
    }

  static columns = [
    {
      title: '',
      key: 'service_picture',
      scopedSlots: {customRender: 'service_picture'},
      width: '5%',
      align: 'center',
      sorter: (a, b) =>  (a.service_picture > b.service_picture)-(a.service_picture < b.service_picture)
    },
    {
      title: 'Nombre',
      dataIndex: 'service_name',
      key: 'service_name',
      width: 90,
      align: 'left',
      sorter: (a, b) =>  (a.service_name > b.service_name)-(a.service_name < b.service_name)
    },
    {
      title: 'Código',
      dataIndex: 'service_code',
      key: 'service_code',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.service_code > b.service_code)-(a.service_code < b.service_code)
    },
    {
      title: 'Service_desc',
      dataIndex: 'service_desc',
      key: 'service_desc',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.service_desc > b.service_desc)-(a.service_desc < b.service_desc)
    },
    {
      title: 'Coste de compra',
      dataIndex: 'service_cost',
      key: 'service_cost',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Service_subtotal',
      dataIndex: 'service_subtotal',
      key: 'service_subtotal',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Service_tax_sale',
      dataIndex: 'service_tax_sale',
      key: 'service_tax_sale',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Precio de venta',
      dataIndex: 'service_total',
      key: 'service_total',
      align: 'center',
      width: 100
    },
    {
      title: 'Estado',
      scopedSlots: {
        customRender: 'status'
      },
      key: 'service_status.id_service_status',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.service_status.service_status_siglas > b.service_status.service_status_siglas)-(a.service_status.service_status_siglas < b.service_status.service_status_siglas)
    },
    {
      title: 'Created_at',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Updated_at',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: 150,
      fixed: 'right',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_service;
    }

      class_name() {
        return 'Services_view'
      }


    }

