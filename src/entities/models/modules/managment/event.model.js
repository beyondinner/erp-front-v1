/**Generate by ASGENS
 *@author Charlietyn
 *@date Wed Oct 07 18:06:37 GMT-04:00 2020
 *@time Wed Oct 07 18:06:37 GMT-04:00 2020
 */
import {decimal, integer, maxLength, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

import moment from 'moment'

const url = 'managment/event'

export default class Event extends BaseModel {

  id_event
  event_name
  event_price
  is_all_day
  event_duration
  emails
  phones
  id_location
  state
  created_at
  date_init
  date_end
  id_register_type
  id_event_type
  documents
  amount_guess

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_event = attributes.id_event || undefined
      this.event_name = attributes.event_name || ''
      this.event_price = attributes.event_price || 0
      this.amount_guess = attributes.amount_guess || 0
      this.is_all_day = attributes.is_all_day !== 0 || ''
      this.event_duration = attributes.event_duration ? attributes.event_duration : null
      this.emails = attributes.emails || ''
      this.phones = attributes.phones || ''
      this.id_location = attributes.id_location || ''
      this.state = attributes.state || 'Activo'
      this.created_at = moment(attributes.created_at)
      this.date_init = attributes.date_init ? moment(attributes.date_init) : null
      this.date_end = attributes.date_end ? moment(attributes.date_end) : null
      this.id_register_type = attributes.id_register_type || ''
      this.id_event_type = attributes.id_event_type || ''
      this.documents = attributes.documents || '--'
    } else {
      this.state = 'Activo'
      this.created_at = moment()
      this.is_all_day = false
      this.date_init = moment()
      this.date_end = moment()
null
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_event = attributes.id_event
      this.event_name = attributes.event_name
      this.event_price = attributes.event_price
      this.amount_guess = attributes.amount_guess
      this.is_all_day = attributes.is_all_day
      this.event_duration = moment(attributes.event_duration)
      this.emails = attributes.emails
      this.phones = attributes.phones
      this.id_location = attributes.id_location
      this.state = attributes.state
      this.created_at = moment(attributes.created_at)
      this.date_init = moment(attributes.date_init)
      this.date_end = moment(attributes.date_end)
      this.id_register_type = attributes.id_register_type
      this.id_event_type = attributes.id_event_type
      this.documents = attributes.documents
    }
  }

  static validations = {
    event: {
      event_name: {
        required,
      },
      event_price: {
        required,
        decimal,
      },
      amount_guess: {
        required,
        integer,
      },
      is_all_day: {
        required,
      },
      event_duration: {
        required,
      },
      emails: {
        required,
        maxLength: maxLength(65535),
      },
      phones: {
        required,
        maxLength: maxLength(65535),
      },
      id_location: {
        required,
        integer,
      },
      state: {
        required,
      },
      created_at: {
        required,
      },
      date_init: {
        required,
      },
      date_end: {
        required,
      },
      id_register_type: {
        required,
        integer,
      },
      id_event_type: {
        required,
        integer,
      },
      documents: {
        maxLength: maxLength(65535),
      },
    },
  }
  static getDocumentURL(documents) {
    return process.env.BASE_URL + "assets/events/" + documents
  }

  static feedbacks = {
    event: {
      id_event: {
        isUnique: 'This id_event has been taken'

      },
    },
  }

  static columns = [
    {
      title: 'Id',
      align: 'center',
      dataIndex: 'id_event',
      key: 'id_event',
      width: '200px',
      sorter: (a, b) => (a.event_name > b.event_name) - (a.event_name < b.event_name)
    },
    {
      title: 'Nombre del Evento',
      align: 'center',
      dataIndex: 'event_name',
      key: 'event_name',
      width: '200px',
      sorter: (a, b) => (a.event_name > b.event_name) - (a.event_name < b.event_name)
    },
    {
      title: 'Precio',
      dataIndex: 'event_price',
      align: 'center',
      key: 'event_price',
      width: '100px',
      sorter: (a, b) => a.event_price - b.event_price
    },
    {
      title: 'Dia completo',
      customRender: (text, row, index) => {
        return text.is_all_day === '0' ? 'No' : 'Si'
      },
      align: 'center',
      key: 'is_all_day',
      width: '200px',

    },
    {
      title: 'Duración',
      dataIndex: 'event_duration',
      align: 'center',
      key: 'event_duration',
      width: '150px',
      sorter: (a, b) => a.event_duration - b.event_duration
    },
    {
      title: 'Participantes',
      dataIndex: 'amount_guess',
      align: 'center',
      key: 'amount_guess',
      width: '150px',
      sorter: (a, b) => a.amount_guess - b.amount_guess
    },
    {
      title: 'Correos',
      dataIndex: 'emails',
      align: 'center',
      key: 'emails',
      width: '150px',
      sorter: (a, b) => (a.emails > b.emails) - (a.emails < b.emails)
    },
    {
      title: 'Teléfonos',
      dataIndex: 'phones',
      align: 'center',
      key: 'phones',
      width: '200px',
      sorter: (a, b) => (a.phones > b.phones) - (a.phones < b.phones)
    },
    {
      title: 'Lugar',
      dataIndex: 'location.location',
      align: 'center',
      key: 'location.location',
      width: '150px',
      sorter: (a, b) => (a.location.location > b.location.location) - (a.location.location < b.location.location)
    },
    {
      title: 'Estado',
      scopedSlots: {
        customRender: 'state'
      },
      align: 'center',
      key: 'state',
      width: '100px',
      sorter: (a, b) => (a.state > b.state) - (a.state < b.state)
    },
    {
      title: 'Fecha inicio',
      align: 'center',
      customRender: (text, row, index) => {
        return moment(text.date_init).format('DD/MM/YYYY')
      },
      key: 'date_init',
      width: '200px',
      sorter: (a, b) => a.date_init - b.date_init
    },
    {
      title: 'Fecha fin',
      align: 'center',
      customRender: (text, row, index) => {
        return moment(text.date_end).format('DD/MM/YYYY')
      },
      key: 'date_end',
      width: '200px',
      sorter: (a, b) => a.date_end - b.date_end
    },
    {
      title: 'Registro',
      dataIndex: 'register_type.resgister_type',
      key: 'register_type.resgister_type',
      align: 'center',
      width: '150px',
      sorter: (a, b) => (a.register_type.resgister_type > b.register_type.resgister_type) - (a.register_type.resgister_type < b.register_type.resgister_type)
    },
    {
      title: 'Evento',
      dataIndex: 'event_type.event_type',
      key: 'event_type.event_type',
      align: 'center',
      width: '150px',
      sorter: (a, b) => (a.event_type.event_type > b.event_type.event_type) - (a.event_type.event_type < b.event_type.event_type)
    },
    {
      title: 'Documento',
      dataIndex: 'documents',
      key: 'documents',
      align: 'center',
      width: '150px',
      sorter: (a, b) => (a.documents > b.documents) - (a.documents < b.documents)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '150px',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_event
  }

  class_name () {
    return 'Event'
  }

}

