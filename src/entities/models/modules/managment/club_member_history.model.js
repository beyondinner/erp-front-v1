/**Generate by ASGENS
 *@author Charlietyn
 *@date Sun Nov 01 00:58:30 GMT-05:00 2020
 *@time Sun Nov 01 00:58:30 GMT-05:00 2020
 */
import {
  required,
  integer,
  between,
  maxLength,
  minLength,
  decimal
} from 'vuelidate/lib/validators';

import BaseModel from '../../base.model';

import moment from 'moment';

const url = 'managment/club_member_history';

export default class Club_member_history extends BaseModel {

  id_club_member_history
  club_member_id
  ammount
  product_id
  created_at
  updated_at
  services_id
  history_description
  cant_element

  constructor(attributes = null) {
    super();
    if (attributes != null) {

      this.id_club_member_history = attributes.id_club_member_history|| undefined
      this.club_member_id = attributes.club_member_id|| null
      this.product_id = attributes.product_id|| null
      this.created_at = attributes.created_at?moment(attributes.created_at).format('YYYY-MM-DD'):null
      this.updated_at = attributes.updated_at?moment(attributes.updated_at).format('YYYY-MM-DD'):null
      this.ammount = attributes.ammount|| null
      this.services_id = attributes.services_id|| null
      this.history_description = attributes.history_description|| null
      this.id_amount_club_member = attributes.id_amount_club_member|| null
      this.cant_element = attributes.cant_element|| null
    }
  }

  set_attributes(attributes = null) {
    if (attributes != null) {

      this.id_club_member_history = attributes.id_club_member_history
      this.club_member_id = attributes.club_member_id
      this.product_id = attributes.product_id
      this.created_at = moment(attributes.created_at).format('YYYY-MM-DD')
      this.updated_at = moment(attributes.updated_at).format('YYYY-MM-DD')
      this.ammount = attributes.ammount
      this.services_id = attributes.services_id
      this.history_description = attributes.history_description
      this.id_amount_club_member = attributes.id_amount_club_member
      this.cant_element = attributes.cant_element
    }
  }


  static validations = {
    club_member_history: {
      club_member_id: {
        required,
        integer,
      },
      product_id: {
        integer,
      },
      created_at: {
        required,
      },
      updated_at: {
        required,
      },
      ammount: {
        required,
        decimal,
      },
      services_id: {
        integer,
      },
      history_description: {
        maxLength: maxLength(65535),
      },
      id_amount_club_member: {
        integer,
      },
      cant_element: {
        required,
        integer,
      },
    },
  }

  static feedbacks = {
    club_member_history: {
      id_club_member_history: {
        isUnique: 'This id_club_member_history has been taken'

      },
    },
  }

  static columns = [
    {
      title: 'Socio',
      dataIndex: 'club_member.club_members_name',
      align:'center',
      key: 'club_member.club_members_name',
      width: 100,
      sorter: (a, b) =>  (a.club_member.club_members_name > b.club_member.club_members_name)-(a.club_member.club_members_name < b.club_member.club_members_name)
    },
    {
      title: 'Producto',
      dataIndex: 'product.product_name',
      align:'center',
      key: 'product.product_name',
      width: '20%',
      sorter: (a, b) =>  (a.product.product_name > b.product.product_name)-(a.product.product_name < b.product.product_name)
    },
    {
      title: 'Importe',
      scopedSlots: {
        customRender: 'price'
      },
      align:'center',
      key: 'price',
      width: '20%',
      sorter: (a, b) =>  (a.product.product_name > b.product.product_name)-(a.product.product_name < b.product.product_name)
    },
    {
      title: 'Cantidad',
      dataIndex: 'cant_element',
      align:'center',
      key: 'cant_element',
      width: '20%',
      sorter: (a, b) => a.cant_element - b.cant_element
    },
    {
      title: 'Monto',
      dataIndex: 'ammount',
      align:'center',
      key: 'ammount',
      width: '20%',
      sorter: (a, b) => a.ammount - b.ammount
    },
    {
      title: 'Fecha',
      customRender:(element)=> { return moment(element.created_at).format('DD/MM/YYYY HH:mm')},
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Servicio',
      dataIndex: 'services.service_name',
      align:'center',
      key: 'services.service_name',
      width: 110,
      sorter: (a, b) =>  (a.services.service_name > b.services.service_name)-(a.services.service_name < b.services.service_name)
    },
    {
      title: 'Descripción',
      dataIndex: 'history_description',
      align:'center',
      key: 'history_description',
      width: 200,
      sorter: (a, b) =>  (a.history_description > b.history_description)-(a.history_description < b.history_description)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: 100,
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

  static get url() {
    return url
  };

  get url() {
    return url
  };

  get_id() {
    return this.id_club_member_history;
  }
  class_name() {
    return 'Club_member_history'
  }


}

