/**Generate by ASGENS
 *@author Charlietyn
 *@date Sun Nov 01 00:58:30 GMT-05:00 2020
 *@time Sun Nov 01 00:58:30 GMT-05:00 2020
 */
import {
  required,
  integer,
  between,
  maxLength,
  minLength,
  decimal
} from 'vuelidate/lib/validators';

import BaseModel from '../../base.model';

import moment from 'moment';

const url = 'managment/club_member_history_other';

export default class Club_member_history_other extends BaseModel {

  history_id
  history_type
  history_member_id
  history_create_at
  history_update_at
  history_description

  constructor(attributes = null) {
    super();
    if (attributes != null) {

      this.history_id = attributes.history_id|| undefined
      this.history_type = attributes.history_type|| null
      this.history_member_id = attributes.history_member_id|| null
      this.history_create_at = attributes.history_create_at?moment(attributes.history_create_at).format('YYYY-MM-DD'):null
      this.history_update_at = attributes.history_update_at?moment(attributes.history_update_at).format('YYYY-MM-DD'):null
      this.history_description = attributes.history_description|| null
    }
  }

  set_attributes(attributes = null) {
    if (attributes != null) {
      this.history_id = attributes.history_id
      this.history_type = attributes.history_type
      this.history_member_id = attributes.history_member_id
      this.history_create_at = moment(attributes.history_create_at).format('YYYY-MM-DD')
      this.history_update_at = moment(attributes.history_update_at).format('YYYY-MM-DD')
      this.history_description = attributes.history_description
    }
  }

  static feedbacks = {
    club_member_history_other: {
      history_member_id: {
        isUnique: 'This id_club_member_history has been taken'

      },
    },
  }

  static columns = [
    {
      title: 'Fecha',
      customRender:(element)=> { return moment(element.val_created).format('DD/MM/YYYY HH:mm')},
      key: 'history_create_at',
      width: '15%',
      align: 'center',
      sorter: (a, b) => a.val_created - b.val_created
    },
    {
      title: 'Tipo',
      dataIndex: 'val_text',
      align:'center',
      key: 'history_type',
      width: '20%',
      sorter: (a, b) => a.history_type - b.history_type
    },
    {
      title: 'Descripción',
      customRender:(element)=> {
        return element.history_description
        // return element.val_text
      },
      align:'left',
      key: 'history_description',
      width: '85%',
      sorter: (a, b) =>  (a.history_description > b.history_description)-(a.history_description < b.history_description)
    }
  ];

  static get url() {
    return url
  };

  get url() {
    return url
  };

  get_id() {
    return this.id_club_member_history;
  }
  class_name() {
    return 'Club_member_history_other'
  }


}

