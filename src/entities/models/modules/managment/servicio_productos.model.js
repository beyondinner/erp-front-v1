/**Generate by ASGENS
*@author Charlietyn
*@date Thu Oct 08 21:28:07 GMT-04:00 2020
*@time Thu Oct 08 21:28:07 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/servicio_productos';

    export default class Servicio_productos extends BaseModel {

       id
       name
       description
       price
       max_part
       state
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id = attributes.id|| undefined
        this.name = attributes.name|| ''
        this.description = attributes.description|| ''
        this.price = attributes.price|| ''
        this.max_part = attributes.max_part|| ''
        this.state = attributes.state|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id = attributes.id
        this.name = attributes.name
        this.description = attributes.description
        this.price = attributes.price
        this.max_part = attributes.max_part
        this.state = attributes.state
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
      }
    }

    static validations = {
      servicio_productos: {
        name: {
          required,
        },
        description: {
          required,
        },
        price: {
          required,
          decimal,
        },
        max_part: {
          required,
          integer,
        },
        state: {
          required,
          integer,
        },
        created_at: {
        },
        updated_at: {
        },
      },
    }

    static feedbacks = {
      servicio_productos: {
      id: {
        isUnique: 'This id has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '20%',
      sorter: (a, b) =>  (a.name > b.name)-(a.name < b.name)
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
      width: '20%',
      sorter: (a, b) =>  (a.description > b.description)-(a.description < b.description)
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      width: '20%',
    },
    {
      title: 'Max_part',
      dataIndex: 'max_part',
      key: 'max_part',
      width: '20%',
      sorter: (a, b) => a.max_part - b.max_part
    },
    {
      title: 'State',
      dataIndex: 'state',
      key: 'state',
      width: '20%',
      sorter: (a, b) =>  (a.state > b.state)-(a.state < b.state)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id;
    }
    class_name() {
        return 'Servicio_productos'
      }


   }

