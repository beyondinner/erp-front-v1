/**Generate by ASGENS
*@author Charlietyn
*@date Thu Oct 08 21:28:07 GMT-04:00 2020
*@time Thu Oct 08 21:28:07 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/cliente_ser_prods';

    export default class Cliente_ser_prods extends BaseModel {

       id
       id_cliente
       id_serv_prod
       comment
       type
       coins
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id = attributes.id|| undefined
        this.id_cliente = attributes.id_cliente|| ''
        this.id_serv_prod = attributes.id_serv_prod|| ''
        this.comment = attributes.comment|| ''
        this.type = attributes.type|| ''
        this.coins = attributes.coins|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id = attributes.id
        this.id_cliente = attributes.id_cliente
        this.id_serv_prod = attributes.id_serv_prod
        this.comment = attributes.comment
        this.type = attributes.type
        this.coins = attributes.coins
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
      }
    }

    static validations = {
      cliente_ser_prods: {
        id_cliente: {
          required,
          integer,
        },
        id_serv_prod: {
          integer,
        },
        comment: {
        },
        type: {
          required,
          integer,
        },
        coins: {
          decimal,
        },
        created_at: {
        },
        updated_at: {
        },
      },
    }

    static feedbacks = {
      cliente_ser_prods: {
      id: {
        isUnique: 'This id has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Id_cliente',
      dataIndex: 'id_cliente',
      key: 'id_cliente',
      width: '20%',
      sorter: (a, b) => a.id_cliente - b.id_cliente
    },
    {
      title: 'Id_serv_prod',
      dataIndex: 'id_serv_prod',
      key: 'id_serv_prod',
      width: '20%',
      sorter: (a, b) => a.id_serv_prod - b.id_serv_prod
    },
    {
      title: 'Comment',
      dataIndex: 'comment',
      key: 'comment',
      width: '20%',
      sorter: (a, b) =>  (a.comment > b.comment)-(a.comment < b.comment)
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
      width: '20%',
      sorter: (a, b) => a.type - b.type
    },
    {
      title: 'Coins',
      dataIndex: 'coins',
      key: 'coins',
      width: '20%',
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id;
    }
    class_name() {
        return 'Cliente_ser_prods'
      }


   }

