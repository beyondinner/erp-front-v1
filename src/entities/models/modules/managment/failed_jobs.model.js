/**Generate by ASGENS
*@author Charlietyn 
*@date Thu Oct 08 21:28:07 GMT-04:00 2020  
*@time Thu Oct 08 21:28:07 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/failed_jobs';

    export default class Failed_jobs extends BaseModel {

       id
       connection
       queue
       payload
       exception
       failed_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id = attributes.id|| undefined
        this.connection = attributes.connection|| ''
        this.queue = attributes.queue|| ''
        this.payload = attributes.payload|| ''
        this.exception = attributes.exception|| ''
        this.failed_at = attributes.failed_at?moment(attributes.failed_at):null
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id = attributes.id
        this.connection = attributes.connection
        this.queue = attributes.queue
        this.payload = attributes.payload
        this.exception = attributes.exception
        this.failed_at = moment(attributes.failed_at)
      }
    }

    static validations = {
      failed_jobs: {
        connection: {
          required,
          maxLength: maxLength(65535),
        },
        queue: {
          required,
          maxLength: maxLength(65535),
        },
        payload: {
          required,
        },
        exception: {
          required,
        },
        failed_at: {
          required,
        },
      },
    }

    static feedbacks = {
      failed_jobs: {
      id: {
        isUnique: 'This id has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Connection',
      dataIndex: 'connection',
      key: 'connection',
      width: '20%',
      sorter: (a, b) =>  (a.connection > b.connection)-(a.connection < b.connection)
    },
    {
      title: 'Queue',
      dataIndex: 'queue',
      key: 'queue',
      width: '20%',
      sorter: (a, b) =>  (a.queue > b.queue)-(a.queue < b.queue)
    },
    {
      title: 'Payload',
      dataIndex: 'payload',
      key: 'payload',
      width: '20%',
      sorter: (a, b) =>  (a.payload > b.payload)-(a.payload < b.payload)
    },
    {
      title: 'Exception',
      dataIndex: 'exception',
      key: 'exception',
      width: '20%',
      sorter: (a, b) =>  (a.exception > b.exception)-(a.exception < b.exception)
    },
    {
      title: 'Failed_at',
      dataIndex: 'failed_at',
      key: 'failed_at',
      width: '20%',
      sorter: (a, b) => a.failed_at - b.failed_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id;
    }
    class_name() {
        return 'Failed_jobs'
      }
  

   }

