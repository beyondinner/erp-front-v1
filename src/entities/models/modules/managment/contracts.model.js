/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import { required, integer, between, maxLength, minLength, email } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'managment/contracts';

    export default class Contracts extends BaseModel {

       id_contract
       employee_id
       contract_start_date
       contract_end_date
       contract_title
       contract_type_id
       payment_type_id
       contract_payment_unity_id
       contract_net_salary
       contract_payment_quantity
       contract_work_hours
       contract_days_per_week
       contract_attachment
       contract_status_id
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_contract = attributes.id_contract|| undefined
        this.employee_id = attributes.employee_id|| ''
        this.contract_start_date = attributes.contract_start_date?moment(attributes.contract_start_date).format():null
        this.contract_end_date = attributes.contract_end_date?moment(attributes.contract_end_date).format():null
        this.contract_title = attributes.contract_title|| ''
        this.contract_type_id = attributes.contract_type_id|| ''
        this.payment_type_id = attributes.payment_type_id|| ''
        this.contract_payment_unity_id = attributes.contract_payment_unity_id|| ''
        this.contract_net_salary = attributes.contract_net_salary|| ''
        this.contract_payment_quantity = attributes.contract_payment_quantity|| ''
        this.contract_work_hours = attributes.contract_work_hours|| ''
        this.contract_days_per_week = attributes.contract_days_per_week|| ''
        this.contract_attachment = attributes.contract_attachment|| ''
        this.contract_status_id = attributes.contract_status_id|| ''
        this.created_at = attributes.created_at ? moment(attributes.created_at).format() : moment().format()
        this.updated_at = attributes.updated_at ? moment(attributes.updated_at).format() : moment().format()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_contract = attributes.id_contract
        this.employee_id = attributes.employee_id
        this.contract_start_date = moment(attributes.contract_start_date).format()
        this.contract_end_date = moment(attributes.contract_end_date).format()
        this.contract_title = attributes.contract_title
        this.contract_type_id = attributes.contract_type_id
        this.payment_type_id = attributes.payment_type_id
        this.contract_payment_unity_id = attributes.contract_payment_unity_id
        this.contract_net_salary = attributes.contract_net_salary
        this.contract_payment_quantity = attributes.contract_payment_quantity
        this.contract_work_hours = attributes.contract_work_hours
        this.contract_days_per_week = attributes.contract_days_per_week
        this.contract_attachment = attributes.contract_attachment
        this.contract_status_id = attributes.contract_status_id
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      contracts: {
        employee_id: {
          required,
          integer,
        },
        contract_start_date: {
          required,
        },
        contract_end_date: {
          required,
        },
        contract_title: {
          required,
          maxLength: maxLength(60)
        },
        contract_type_id: {
          required,
          integer,
        },
        payment_type_id: {
          required,
          integer,
        },
        contract_payment_unity_id: {
          required,
          integer,
        },
        contract_net_salary: {
          required,
          integer
        },
        contract_payment_quantity: {
          required,
          integer,
        },
        contract_work_hours: {
          required,
          integer,
        },
        contract_days_per_week: {
          required,
          integer,
        },
        contract_attachment: {
        },
        contract_status_id: {
          required,
          integer,
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

      static getDocumentURL (contract_attachment) {
        return process.env.BASE_URL + 'assets/contracts/' + contract_attachment
      }

    static feedbacks = {
      contracts: {
      id_contract: {
        isUnique: 'Ya existe un elemento con este ID'

      },
      },
    }

  static columns = [
    {
      title: 'Nombre',
      dataIndex: 'employees.employee_name',
      key: 'employees.id_employee',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.employees.employee_name > b.employees.employee_name)-(a.employees.employee_name < b.employees.employee_name)
    },
    {
      title: 'Apellidos',
      dataIndex: 'employees.employee_last_name',
      key: 'employees.id_employee',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.employees.employee_last_name > b.employees.employee_last_name)-(a.employees.employee_last_name < b.employees.employee_last_name)
    },
    {
      title: 'NIF',
      dataIndex: 'employees.employee_nif',
      key: 'employees.id_employee',
      width: '12%',
      align: 'center',
      sorter: (a, b) =>  (a.employees.employee_nif > b.employees.employee_nif)-(a.employees.employee_nif < b.employees.employee_nif)
    },
    {
      title: 'Fecha de Inicio',
      dataIndex: 'contract_start_date',
      key: 'contract_start_date',
      width: '12%',
      align: 'center',
      sorter: (a, b) => a.contract_start_date - b.contract_start_date
    },
    {
      title: 'Fecha de Fin',
      dataIndex: 'contract_end_date',
      key: 'contract_end_date',
      width: '12%',
      align: 'center',
      sorter: (a, b) => a.contract_end_date - b.contract_end_date
    },
    {
      title: 'Título',
      dataIndex: 'contract_title',
      key: 'contract_title',
      width: '15%',
      align: 'center',
      sorter: (a, b) =>  (a.contract_title > b.contract_title)-(a.contract_title < b.contract_title)
    },
    {
      title: 'Tipo de Contrato',
      dataIndex: 'contract_types.contract_type_siglas',
      key: 'contract_types.id_contract_type',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.contract_types.contract_type_siglas > b.contract_types.contract_type_siglas)-(a.contract_types.contract_type_siglas < b.contract_types.contract_type_siglas)
    },
    {
      title: 'Tipo de pago',
      dataIndex: 'payment_types.payment_type_siglas',
      key: 'payment_types.id_payment_type',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.payment_types.payment_type_siglas > b.payment_types.payment_type_siglas)-(a.payment_types.payment_type_siglas < b.payment_types.payment_type_siglas)
    },
    {
      title: 'Unidad',
      dataIndex: 'contract_payment_unities.contract_payment_unity_siglas',
      key: 'contract_payment_unities.id_contract_payment_unity',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.contract_payment_unities.contract_payment_unity_siglas > b.contract_payment_unities.contract_payment_unity_siglas)-(a.contract_payment_unities.contract_payment_unity_siglas < b.contract_payment_unities.contract_payment_unity_siglas)
    },
    {
      title: 'Salario neto',
      dataIndex: 'contract_net_salary',
      key: 'contract_net_salary',
      width: '20%',
      align: 'center'
    },
    {
      title: 'Cantidad de pagos',
      dataIndex: 'contract_payment_quantity',
      key: 'contract_payment_quantity',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.contract_payment_quantity - b.contract_payment_quantity
    },
    {
      title: 'Horas de trabajo',
      dataIndex: 'contract_work_hours',
      key: 'contract_work_hours',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.contract_work_hours - b.contract_work_hours
    },
    {
      title: 'Días por semana',
      dataIndex: 'contract_days_per_week',
      key: 'contract_days_per_week',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.contract_days_per_week - b.contract_days_per_week
    },
    {
      title: 'Documento',
      dataIndex: 'contract_attachment',
      key: 'contract_attachment',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.contract_attachment > b.contract_attachment)-(a.contract_attachment < b.contract_attachment)
    },
    {
      title: 'Estado',
      dataIndex: 'contract_status.contract_status_siglas',
      key: 'contract_status.id_contract_status',
      width: '10%',
      align: 'center',
      sorter: (a, b) =>  (a.contract_status.contract_status_siglas > b.contract_status.contract_status_siglas)-(a.contract_status.contract_status_siglas < b.contract_status.contract_status_siglas)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_contract;
    }
      class_name() {
        return 'Contract'
      }

   }

