/**Generate by ASGENS
 *@author Charlietyn
 *@date Tue Oct 06 15:02:02 GMT-04:00 2020
 *@time Tue Oct 06 15:02:02 GMT-04:00 2020
 */
import {decimal, integer, maxLength, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

import moment from 'moment'

const url = 'managment/amount_club_member'

export default class Amount_club_member extends BaseModel {

  id_amount_club_member
  id_user
  id_club_member
  amount
  date_at
  amiunt_description

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_amount_club_member = attributes.id_amount_club_member || undefined
      this.id_user = attributes.id_user || ''
      this.id_club_member = attributes.id_club_member || ''
      this.amount = attributes.amount || ''
      this.date_at = attributes.date_at ? moment(attributes.date_at) : null
      this.amiunt_description = attributes.amiunt_description || ''
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_amount_club_member = attributes.id_amount_club_member
      this.id_user = attributes.id_user
      this.id_club_member = attributes.id_club_member
      this.amount = attributes.amount
      this.date_at = moment(attributes.date_at)
      this.amiunt_description = attributes.amiunt_description
    }
  }

  static validations = {
    amount_club_member: {
      id_user: {
        required,
        integer,
      },
      id_club_member: {
        required,
        integer,
      },
      amount: {
        required,
        decimal,
        customValid: async function (value, object) {
          return Number.parseInt(this.$data.club_members.clum_members_currency)+Number.parseInt(value)>=0
        }

      },
      date_at: {
        required,
      },
      amiunt_description: {
        required,
        maxLength: maxLength(65535),
      },
    },
  }

  static feedbacks = {
    amount_club_member: {
      id_amount_club_member: {
        isUnique: 'This id_amount_club_member has been taken'
      },
      amount: {
        customValid: 'El valor debe ser menor o igual que el saldo'

      },
    },
  }

  static columns = [
    {
      title: 'Usuario',
      dataIndex: 'user.username',
      key: 'user.username',
      align:'left',
      width: 100,
      sorter: (a, b) => (a.user.nombre_usuario > b.user.nombre_usuario) - (a.user.nombre_usuario < b.user.nombre_usuario)
    },
    {
      title: 'Miembro',
      dataIndex: 'club_member.club_members_name',
      key: 'club_member.club_members_name',
      width: '20%',
      sorter: (a, b) => (a.club_member.club_members_name > b.club_member.club_members_name) - (a.club_member.club_members_name < b.club_member.club_members_name)
    },
    {
      title: 'Cantidad',
      scopedSlots: {
        customRender: 'amount'
      },
      key: 'amount',
      align:'center',
      width: '15%',
      sorter: (a, b) => a.amount - b.amount
    },
    {
      title: 'Fecha ',
      customRender: (element) => {
        return moment(element.date_at).format('DD/MM/YYYY H:mm a')
      },
      key: 'date_at',
      width: '20%',
      align:'center',
      sorter: (a, b) => a.date_at - b.date_at
    },
    {
      title: 'Descripcion',
      dataIndex: 'amiunt_description',
      key: 'amiunt_description',
      align:'center',
      width: '40%',
      sorter: (a, b) => (a.amiunt_description > b.amiunt_description) - (a.amiunt_description < b.amiunt_description)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_amount_club_member
  }

  class_name () {
    return 'Amount_club_member'
  }

}

