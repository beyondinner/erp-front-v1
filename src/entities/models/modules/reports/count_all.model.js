/**Generate by ASGENS
*@author Charlietyn
*@date Wed Nov 18 01:20:33 GMT-05:00 2020
*@time Wed Nov 18 01:20:33 GMT-05:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  const url = 'reports/count_all';

    export default class Count_all extends BaseModel {

       count_all

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.count_all = attributes.count_all|| undefined
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.count_all = attributes.count_all
      }
    }


  static columns = [
    {
      title: 'Count_all',
      dataIndex: 'count_all',
      align:'center',
      key: 'count_all',
      width: '20%',
      sorter: (a, b) => a.count_all - b.count_all
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.count_all;
    }
    class_name() {
        return 'Count_all'
      }


   }

