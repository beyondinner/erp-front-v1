/**Generate by ASGENS
 *@author Charlietyn
 *@date Tue Dec 15 14:03:29 GMT-05:00 2020
 *@time Tue Dec 15 14:03:29 GMT-05:00 2020
 */
import {
  required,
  integer,
  between,
  maxLength,
  minLength,
  decimal
} from 'vuelidate/lib/validators';

import BaseModel from '../../base.model';

const url = 'reports/cant_transaction';

export default class Cant_transaction extends BaseModel {

  club_members_name
  club_members_email
  count_transaction
  club_members_picture

  constructor(attributes = null) {
    super();
    if (attributes != null) {

      this.club_members_name = attributes.club_members_name|| undefined
      this.club_members_email = attributes.club_members_email|| ''
      this.count_transaction = attributes.count_transaction|| ''
      this.club_members_picture = attributes.club_members_picture|| ''
    }
  }

  set_attributes(attributes = null) {
    if (attributes != null) {

      this.club_members_name = attributes.club_members_name
      this.club_members_email = attributes.club_members_email
      this.count_transaction = attributes.count_transaction
      this.club_members_picture = attributes.club_members_picture
    }
  }



  static columns = [
    {
      title: 'Club_members_name',
      dataIndex: 'club_members_name',
      align:'center',
      key: 'club_members_name',
      width: '20%',
      sorter: (a, b) =>  (a.club_members_name > b.club_members_name)-(a.club_members_name < b.club_members_name)
    },
    {
      title: 'Club_members_email',
      dataIndex: 'club_members_email',
      align:'center',
      key: 'club_members_email',
      width: '20%',
      sorter: (a, b) =>  (a.club_members_email > b.club_members_email)-(a.club_members_email < b.club_members_email)
    },
    {
      title: 'Count_transaction',
      dataIndex: 'count_transaction',
      align:'center',
      key: 'count_transaction',
      width: '20%',
      sorter: (a, b) => a.count_transaction - b.count_transaction
    },
    {
      title: 'Club_members_picture',
      dataIndex: 'club_members_picture',
      align:'center',
      key: 'club_members_picture',
      width: '20%',
      sorter: (a, b) =>  (a.club_members_picture > b.club_members_picture)-(a.club_members_picture < b.club_members_picture)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

  static get url() {
    return url
  };

  get url() {
    return url
  };

  get_id() {
    return this.club_members_name;
  }
  class_name() {
    return 'Cant_transaction'
  }


}

