/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Sep 07 15:53:43 GMT-04:00 2020
 *@time Mon Sep 07 15:53:43 GMT-04:00 2020
 */
import {maxLength, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

import moment from 'moment'

const url = 'nomenclatures/contact_types'

export default class Contact_types extends BaseModel {

  id_contact_type
  contact_type_siglas
  contact_type_desc
  created_at
  updated_at

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_contact_type = attributes.id_contact_type || undefined
      this.contact_type_siglas = attributes.contact_type_siglas || ''
      this.contact_type_desc = attributes.contact_type_desc || ''
      this.created_at = attributes.created_at ? moment(attributes.created_at).format() : moment()
      this.updated_at = attributes.updated_at ? moment(attributes.updated_at).format() : moment()
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_contact_type = attributes.id_contact_type
      this.contact_type_siglas = attributes.contact_type_siglas
      this.contact_type_desc = attributes.contact_type_desc
      this.created_at = moment(attributes.created_at).format()
      this.updated_at = moment(attributes.updated_at).format()
    }
  }

  static validations = {
    contact_types: {
      contact_type_siglas: {
        required,
        maxLength: maxLength(50),
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_contact_type, contact_type_siglas} = object
          const _scenario = id_contact_type ? 'update' : 'create'
          const params = {id_contact_type, contact_type_siglas}
          const validation = await Contact_types.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        }
      },
      contact_type_desc: {
        required,
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_contact_type, contact_type_desc} = object
          const _scenario = id_contact_type ? 'update' : 'create'
          const params = {id_contact_type, contact_type_desc}
          const validation = await Contact_types.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        }
      },
      created_at: {
        required,
      },
      updated_at: {
        required,
      },
    },
  }

  static feedbacks = {
    contact_types: {
      id_contact_type: {
        isUnique: 'Ya existe un tipo con este ID'

      },
      contact_type_siglas: {
        isUnique: 'Ya existe un tipo con este nombre'

      },
      contact_type_desc: {
        isUnique: 'Ya existe un tipo con esta descripción'

      },
    },
  }

  static columns = [
    {
      title: 'Nombre',
      dataIndex: 'contact_type_siglas',
      key: 'contact_type_siglas',
      width: '35%',
      align: 'center',
      sorter: (a, b) => (a.contact_type_siglas > b.contact_type_siglas) - (a.contact_type_siglas < b.contact_type_siglas)
    },
    {
      title: 'Descripción',
      dataIndex: 'contact_type_desc',
      key: 'contact_type_desc',
      width: '40%',
      align: 'center',
      sorter: (a, b) => (a.contact_type_desc > b.contact_type_desc) - (a.contact_type_desc < b.contact_type_desc)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_contact_type
  }

  class_name () {
    return 'Contact_types'
  }

}

