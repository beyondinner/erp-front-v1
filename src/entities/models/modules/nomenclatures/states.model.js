/**Generate by ASGENS
*@author Charlietyn 
*@date Wed Oct 07 18:06:37 GMT-04:00 2020  
*@time Wed Oct 07 18:06:37 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'nomenclatures/states';

    export default class States extends BaseModel {

       id_state
       state_name
       country_code
       district
       country_id
       created_at
       updated_at
       population

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_state = attributes.id_state|| undefined
        this.state_name = attributes.state_name|| ''
        this.country_code = attributes.country_code|| ''
        this.district = attributes.district|| ''
        this.country_id = attributes.country_id|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
        this.population = attributes.population|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_state = attributes.id_state
        this.state_name = attributes.state_name
        this.country_code = attributes.country_code
        this.district = attributes.district
        this.country_id = attributes.country_id
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
        this.population = attributes.population
      }
    }

    static validations = {
      states: {
        state_name: {
          required,
        },
        country_code: {
        },
        district: {
        },
        country_id: {
          integer,
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
        population: {
          integer,
        },
      },
    }

    static feedbacks = {
      states: {
      id_state: {
        isUnique: 'This id_state has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'State_name',
      dataIndex: 'state_name',
      key: 'state_name',
      width: '20%',
      sorter: (a, b) =>  (a.state_name > b.state_name)-(a.state_name < b.state_name)
    },
    {
      title: 'Country_code',
      dataIndex: 'country_code',
      key: 'country_code',
      width: '20%',
      sorter: (a, b) =>  (a.country_code > b.country_code)-(a.country_code < b.country_code)
    },
    {
      title: 'District',
      dataIndex: 'district',
      key: 'district',
      width: '20%',
      sorter: (a, b) =>  (a.district > b.district)-(a.district < b.district)
    },
    {
      title: 'Country',
      dataIndex: 'country.country_siglas',
      key: 'country.country_siglas',
      width: '20%',
      sorter: (a, b) =>  (a.country.country_siglas > b.country.country_siglas)-(a.country.country_siglas < b.country.country_siglas)
    },
    {
      title: 'Population',
      dataIndex: 'population',
      key: 'population',
      width: '20%',
      sorter: (a, b) => a.population - b.population
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id_state;
    }
    class_name() {
        return 'States'
      }
  

   }

