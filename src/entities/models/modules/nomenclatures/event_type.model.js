/**Generate by ASGENS
*@author Charlietyn 
*@date Wed Oct 07 18:06:37 GMT-04:00 2020  
*@time Wed Oct 07 18:06:37 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  const url = 'nomenclatures/event_type';

    export default class Event_type extends BaseModel {

       id_event_type
       event_type
       event_type_description

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_event_type = attributes.id_event_type|| undefined
        this.event_type = attributes.event_type|| ''
        this.event_type_description = attributes.event_type_description|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_event_type = attributes.id_event_type
        this.event_type = attributes.event_type
        this.event_type_description = attributes.event_type_description
      }
    }

    static validations = {
      event_type: {
        event_type: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_event_type,event_type}=object
               const _scenario=id_event_type?'update':'create'
               const params={id_event_type,event_type}
               const validation= await Event_type.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        event_type_description: {
          maxLength: maxLength(65535),
        },
      },
    }

    static feedbacks = {
      event_type: {
      id_event_type: {
        isUnique: 'This id_event_type has been taken' 

      },
      event_type: {
        isUnique: 'This event_type has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Event_type',
      dataIndex: 'event_type',
      key: 'event_type',
      width: '20%',
      sorter: (a, b) =>  (a.event_type > b.event_type)-(a.event_type < b.event_type)
    },
    {
      title: 'Event_type_description',
      dataIndex: 'event_type_description',
      key: 'event_type_description',
      width: '20%',
      sorter: (a, b) =>  (a.event_type_description > b.event_type_description)-(a.event_type_description < b.event_type_description)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id_event_type;
    }
    class_name() {
        return 'Event_type'
      }
  

   }

