/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'nomenclatures/service_status';

    export default class Service_status extends BaseModel {

       id_service_status
       service_status_siglas
       service_status_desc
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_service_status = attributes.id_service_status|| undefined
        this.service_status_siglas = attributes.service_status_siglas|| ''
        this.service_status_desc = attributes.service_status_desc|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_service_status = attributes.id_service_status
        this.service_status_siglas = attributes.service_status_siglas
        this.service_status_desc = attributes.service_status_desc
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      service_status: {
        service_status_siglas: {
          required,
          maxLength: maxLength(50),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_service_status,service_status_siglas}=object
               const _scenario=id_service_status?'update':'create'
               const params={id_service_status,service_status_siglas}
               const validation= await Service_status.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        service_status_desc: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_service_status,service_status_desc}=object
               const _scenario=id_service_status?'update':'create'
               const params={id_service_status,service_status_desc}
               const validation= await Service_status.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

    static feedbacks = {
      service_status: {
      id_service_status: {
        isUnique: 'Ya existe un estado con este ID'

      },
      service_status_siglas: {
        isUnique: 'Ya existe un estado con este nombre'
      },
      service_status_desc: {
        isUnique: 'Ya existe un estado con esta descripción'

      },
      },
    }

  static columns = [
    {
      title: 'Nombre',
      dataIndex: 'service_status_siglas',
      key: 'service_status_siglas',
      width: '35%',
      align: 'center',
      sorter: (a, b) =>  (a.service_status_siglas > b.service_status_siglas)-(a.service_status_siglas < b.service_status_siglas)
    },
    {
      title: 'Descripción',
      dataIndex: 'service_status_desc',
      key: 'service_status_desc',
      width: '40%',
      align: 'center',
      sorter: (a, b) =>  (a.service_status_desc > b.service_status_desc)-(a.service_status_desc < b.service_status_desc)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_service_status;
    }

      class_name() {
        return 'Service_status'
      }


    }

