/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'nomenclatures/languages';

    export default class Languages extends BaseModel {

       id_language
       language_siglas
       language_desc
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_language = attributes.id_language|| undefined
        this.language_siglas = attributes.language_siglas|| ''
        this.language_desc = attributes.language_desc|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_language = attributes.id_language
        this.language_siglas = attributes.language_siglas
        this.language_desc = attributes.language_desc
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      languages: {
        language_siglas: {
          required,
          maxLength: maxLength(4),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_language,language_siglas}=object
               const _scenario=id_language?'update':'create'
               const params={id_language,language_siglas}
               const validation= await Languages.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        language_desc: {
          required,
          maxLength: maxLength(50),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_language,language_desc}=object
               const _scenario=id_language?'update':'create'
               const params={id_language,language_desc}
               const validation= await Languages.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

    static feedbacks = {
      languages: {
      id_language: {
        isUnique: 'Ya existe un idioma con este ID'

      },
      language_siglas: {
        isUnique: 'Ya existe un idioma con estas siglas'

      },
      language_desc: {
        isUnique: 'Ya existe un idioma con este nombre'

      },
      },
    }

  static columns = [
    {
      title: 'Siglas',
      dataIndex: 'language_siglas',
      key: 'language_siglas',
      width: '35%',
      align: 'center',
      sorter: (a, b) =>  (a.language_siglas > b.language_siglas)-(a.language_siglas < b.language_siglas)
    },
    {
      title: 'Nombre',
      dataIndex: 'language_desc',
      key: 'language_desc',
      width: '40%',
      align: 'center',
      sorter: (a, b) =>  (a.language_desc > b.language_desc)-(a.language_desc < b.language_desc)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_language;
    }

      class_name() {
        return 'Languages'
      }


    }

