/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'nomenclatures/contact_status';

    export default class Contact_status extends BaseModel {

       id_contact_status
       contact_status_siglas
       contact_status_desc
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_contact_status = attributes.id_contact_status|| undefined
        this.contact_status_siglas = attributes.contact_status_siglas|| ''
        this.contact_status_desc = attributes.contact_status_desc|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_contact_status = attributes.id_contact_status
        this.contact_status_siglas = attributes.contact_status_siglas
        this.contact_status_desc = attributes.contact_status_desc
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      contact_status: {
        contact_status_siglas: {
          required,
          maxLength: maxLength(50),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_contact_status,contact_status_siglas}=object
               const _scenario=id_contact_status?'update':'create'
               const params={id_contact_status,contact_status_siglas}
               const validation= await Contact_status.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        contact_status_desc: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_contact_status,contact_status_desc}=object
               const _scenario=id_contact_status?'update':'create'
               const params={id_contact_status,contact_status_desc}
               const validation= await Contact_status.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

    static feedbacks = {
      contact_status: {
      id_contact_status: {
        isUnique: 'Ya existe un estado con este ID'

      },
      contact_status_siglas: {
        isUnique: 'Ya existe un estado con este nombre'

      },
      contact_status_desc: {
        isUnique: 'Ya existe un estado con esta descripción'

      },
      },
    }

  static columns = [
    {
      title: 'Nombre',
      dataIndex: 'contact_status_siglas',
      key: 'contact_status_siglas',
      width: '35%',
      align: 'center',
      sorter: (a, b) =>  (a.contact_status_siglas > b.contact_status_siglas)-(a.contact_status_siglas < b.contact_status_siglas)
    },
    {
      title: 'Descripción',
      dataIndex: 'contact_status_desc',
      key: 'contact_status_desc',
      width: '40%',
      align: 'center',
      sorter: (a, b) =>  (a.contact_status_desc > b.contact_status_desc)-(a.contact_status_desc < b.contact_status_desc)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_contact_status;
    }

      class_name() {
        return 'Contact_status'
      }

   }

