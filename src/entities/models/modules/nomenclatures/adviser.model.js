/**Generate by ASGENS
*@author Charlietyn 
*@date Mon Oct 26 02:17:05 GMT-04:00 2020  
*@time Mon Oct 26 02:17:05 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  const url = 'nomenclatures/adviser';

    export default class Adviser extends BaseModel {

       id_adviser
       adviser

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_adviser = attributes.id_adviser|| undefined
        this.adviser = attributes.adviser|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_adviser = attributes.id_adviser
        this.adviser = attributes.adviser
      }
    }

    static validations = {
      adviser: {
        adviser: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_adviser,adviser}=object
               const _scenario=id_adviser?'update':'create'
               const params={id_adviser,adviser}
               const validation= await Adviser.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
      },
    }

    static feedbacks = {
      adviser: {
      id_adviser: {
        isUnique: 'This id_adviser has been taken' 

      },
      adviser: {
        isUnique: 'This adviser has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Adviser',
      dataIndex: 'adviser',
      align:'center',
      key: 'adviser',
      width: '20%',
      sorter: (a, b) =>  (a.adviser > b.adviser)-(a.adviser < b.adviser)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id_adviser;
    }
    class_name() {
        return 'Adviser'
      }
  

   }

