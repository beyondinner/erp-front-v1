/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'nomenclatures/currencies';

    export default class Currencies extends BaseModel {

       id_currency
       currency_siglas
       currency_desc
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_currency = attributes.id_currency|| undefined
        this.currency_siglas = attributes.currency_siglas|| ''
        this.currency_desc = attributes.currency_desc|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_currency = attributes.id_currency
        this.currency_siglas = attributes.currency_siglas
        this.currency_desc = attributes.currency_desc
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      currencies: {
        currency_siglas: {
          required,
          maxLength: maxLength(4),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_currency,currency_siglas}=object
               const _scenario=id_currency?'update':'create'
               const params={id_currency,currency_siglas}
               const validation= await Currencies.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        currency_desc: {
          required,
          maxLength: maxLength(50),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_currency,currency_desc}=object
               const _scenario=id_currency?'update':'create'
               const params={id_currency,currency_desc}
               const validation= await Currencies.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

    static feedbacks = {
      currencies: {
      id_currency: {
        isUnique: 'Ya existe una moneda con este ID'

      },
      currency_siglas: {
        isUnique: 'Ya existe una moneda con estas siglas'

      },
      currency_desc: {
        isUnique: 'Ya existe una moneda con este nombre'

      },
      },
    }

  static columns = [
    {
      title: 'Siglas',
      dataIndex: 'currency_siglas',
      key: 'currency_siglas',
      width: '35%',
      align: 'center',
      sorter: (a, b) =>  (a.currency_siglas > b.currency_siglas)-(a.currency_siglas < b.currency_siglas)
    },
    {
      title: 'Nombre',
      dataIndex: 'currency_desc',
      key: 'currency_desc',
      width: '40%',
      align: 'center',
      sorter: (a, b) =>  (a.currency_desc > b.currency_desc)-(a.currency_desc < b.currency_desc)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_currency;
    }

      class_name() {
        return 'Currencies'
      }


   }

