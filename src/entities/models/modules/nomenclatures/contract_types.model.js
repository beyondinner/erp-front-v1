/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'nomenclatures/contract_types';

    export default class Contract_types extends BaseModel {

       id_contract_type
       contract_type_siglas
       contract_type_desc
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_contract_type = attributes.id_contract_type|| undefined
        this.contract_type_siglas = attributes.contract_type_siglas|| ''
        this.contract_type_desc = attributes.contract_type_desc|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_contract_type = attributes.id_contract_type
        this.contract_type_siglas = attributes.contract_type_siglas
        this.contract_type_desc = attributes.contract_type_desc
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      contract_types: {
        contract_type_siglas: {
          required,
          maxLength: maxLength(50),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_contract_type,contract_type_siglas}=object
               const _scenario=id_contract_type?'update':'create'
               const params={id_contract_type,contract_type_siglas}
               const validation= await Contract_types.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        contract_type_desc: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_contract_type,contract_type_desc}=object
               const _scenario=id_contract_type?'update':'create'
               const params={id_contract_type,contract_type_desc}
               const validation= await Contract_types.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

    static feedbacks = {
      contract_types: {
      id_contract_type: {
        isUnique: 'Ya existe un tipo con este ID'

      },
      contract_type_siglas: {
        isUnique: 'Ya existe un tipo con este nombre'

      },
      contract_type_desc: {
        isUnique: 'Ya existe un tipo con esta descripción'

      },
      },
    }

  static columns = [
    {
      title: 'Nombre',
      dataIndex: 'contract_type_siglas',
      key: 'contract_type_siglas',
      width: '35%',
      align: 'center',
      sorter: (a, b) =>  (a.contract_type_siglas > b.contract_type_siglas)-(a.contract_type_siglas < b.contract_type_siglas)
    },
    {
      title: 'Descripción',
      dataIndex: 'contract_type_desc',
      key: 'contract_type_desc',
      width: '40%',
      align: 'center',
      sorter: (a, b) =>  (a.contract_type_desc > b.contract_type_desc)-(a.contract_type_desc < b.contract_type_desc)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_contract_type;
    }

      class_name() {
        return 'Contract_types'
      }

   }

