/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'nomenclatures/countries';

    export default class Countries extends BaseModel {

       id_country
       country_siglas
       country_name
       country_code
       created_at
       updated_at

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_country = attributes.id_country|| undefined
        this.country_siglas = attributes.country_siglas|| ''
        this.country_name = attributes.country_name|| ''
        this.country_code = attributes.country_code|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at).format():moment()
        this.updated_at = attributes.updated_at?moment(attributes.updated_at).format():moment()
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_country = attributes.id_country
        this.country_siglas = attributes.country_siglas
        this.country_name = attributes.country_name
        this.country_code = attributes.country_code
        this.created_at = moment(attributes.created_at).format()
        this.updated_at = moment(attributes.updated_at).format()
      }
    }

    static validations = {
      countries: {
        country_siglas: {
          required,
          maxLength: maxLength(7),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_country,country_siglas}=object
               const _scenario=id_country?'update':'create'
               const params={id_country,country_siglas}
               const validation= await Countries.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        country_name: {
          required,
          maxLength: maxLength(50),
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_country,country_name}=object
               const _scenario=id_country?'update':'create'
               const params={id_country,country_name}
               const validation= await Countries.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        country_code: {
          integer,
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
      },
    }

    static feedbacks = {
      countries: {
      id_country: {
        isUnique: 'Ya existe un país con este ID'

      },
      country_siglas: {
        isUnique: 'Ya existe un país con estas siglas'

      },
      country_name: {
        isUnique: 'Ya existe un país con este nombre'

      },
      },
    }

  static columns = [
    {
      title: 'Siglas',
      dataIndex: 'country_siglas',
      key: 'country_siglas',
      width: '20%',
      align: 'center',
      sorter: (a, b) =>  (a.country_siglas > b.country_siglas)-(a.country_siglas < b.country_siglas)
    },
    {
      title: 'Nombre',
      dataIndex: 'country_name',
      key: 'country_name',
      width: '35%',
      align: 'center',
      sorter: (a, b) =>  (a.country_name > b.country_name)-(a.country_name < b.country_name)
    },
    {
      title: 'Código',
      dataIndex: 'country_code',
      key: 'country_code',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.country_code - b.country_code
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id(){
      return this.id_country;
    }

      class_name() {
        return 'Countries'
      }

   }

