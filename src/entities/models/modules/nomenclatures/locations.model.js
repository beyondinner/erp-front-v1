/**Generate by ASGENS
*@author Charlietyn 
*@date Wed Oct 07 18:06:37 GMT-04:00 2020  
*@time Wed Oct 07 18:06:37 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  const url = 'nomenclatures/locations';

    export default class Locations extends BaseModel {

       id_location
       location

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_location = attributes.id_location|| undefined
        this.location = attributes.location|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_location = attributes.id_location
        this.location = attributes.location
      }
    }

    static validations = {
      locations: {
        location: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_location,location}=object
               const _scenario=id_location?'update':'create'
               const params={id_location,location}
               const validation= await Locations.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
      },
    }

    static feedbacks = {
      locations: {
      id_location: {
        isUnique: 'This id_location has been taken' 

      },
      location: {
        isUnique: 'This location has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Location',
      dataIndex: 'location',
      key: 'location',
      width: '20%',
      sorter: (a, b) =>  (a.location > b.location)-(a.location < b.location)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id_location;
    }
    class_name() {
        return 'Locations'
      }
  

   }

