/**Generate by ASGENS
*@author Charlietyn
*@date Wed Nov 25 10:19:58 GMT-05:00 2020
*@time Wed Nov 25 10:19:58 GMT-05:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  const url = 'nomenclatures/order_status';

    export default class Order_status extends BaseModel {

       id_order_status
       order_status
       order_status_description

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_order_status = attributes.id_order_status|| undefined
        this.order_status = attributes.order_status|| ''
        this.order_status_description = attributes.order_status_description|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_order_status = attributes.id_order_status
        this.order_status = attributes.order_status
        this.order_status_description = attributes.order_status_description
      }
    }

    static validations = {
      order_status: {
        order_status: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_order_status,order_status}=object
               const _scenario=id_order_status?'update':'create'
               const params={id_order_status,order_status}
               const validation= await Order_status.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        order_status_description: {
          maxLength: maxLength(65535),
        },
      },
    }

    static feedbacks = {
      order_status: {
      id_order_status: {
        isUnique: 'This id_order_status has been taken'

      },
      order_status: {
        isUnique: 'This order_status has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Estado',
      dataIndex: 'order_status',
      align:'center',
      key: 'order_status',
      width: '30%',
      sorter: (a, b) =>  (a.order_status > b.order_status)-(a.order_status < b.order_status)
    },
    {
      title: 'Descripción',
      dataIndex: 'order_status_description',
      align:'center',
      key: 'order_status_description',
      width: '50%',
      sorter: (a, b) =>  (a.order_status_description > b.order_status_description)-(a.order_status_description < b.order_status_description)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '10%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_order_status;
    }
    class_name() {
        return 'Order_status'
      }


   }

