/**Generate by ASGENS
*@author Charlietyn 
*@date Wed Oct 07 18:06:37 GMT-04:00 2020  
*@time Wed Oct 07 18:06:37 GMT-04:00 2020  
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  const url = 'nomenclatures/register_type';

    export default class Register_type extends BaseModel {

       id_register_type
       resgister_type
       register_type_description

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_register_type = attributes.id_register_type|| undefined
        this.resgister_type = attributes.resgister_type|| ''
        this.register_type_description = attributes.register_type_description|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_register_type = attributes.id_register_type
        this.resgister_type = attributes.resgister_type
        this.register_type_description = attributes.register_type_description
      }
    }

    static validations = {
      register_type: {
        resgister_type: {
          required,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_register_type,resgister_type}=object
               const _scenario=id_register_type?'update':'create'
               const params={id_register_type,resgister_type}
               const validation= await Register_type.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        register_type_description: {
          maxLength: maxLength(65535),
        },
      },
    }

    static feedbacks = {
      register_type: {
      id_register_type: {
        isUnique: 'This id_register_type has been taken' 

      },
      resgister_type: {
        isUnique: 'This resgister_type has been taken' 

      },
      },
    }

  static columns = [
    {
      title: 'Resgister_type',
      dataIndex: 'resgister_type',
      key: 'resgister_type',
      width: '20%',
      sorter: (a, b) =>  (a.resgister_type > b.resgister_type)-(a.resgister_type < b.resgister_type)
    },
    {
      title: 'Register_type_description',
      dataIndex: 'register_type_description',
      key: 'register_type_description',
      width: '20%',
      sorter: (a, b) =>  (a.register_type_description > b.register_type_description)-(a.register_type_description < b.register_type_description)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };
  
    get url() {
      return url
    };
  
    get_id() {
      return this.id_register_type;
    }
    class_name() {
        return 'Register_type'
      }
  

   }

