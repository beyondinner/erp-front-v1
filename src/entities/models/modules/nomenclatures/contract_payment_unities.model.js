/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Sep 07 15:53:43 GMT-04:00 2020
 *@time Mon Sep 07 15:53:43 GMT-04:00 2020
 */
import {maxLength, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

import moment from 'moment'

const url = 'nomenclatures/contract_payment_unities'

export default class Contract_payment_unities extends BaseModel {

  id_contract_payment_unity
  contract_payment_unity_siglas
  contract_payment_unity_desc
  created_at
  updated_at

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_contract_payment_unity = attributes.id_contract_payment_unity || undefined
      this.contract_payment_unity_siglas = attributes.contract_payment_unity_siglas || ''
      this.contract_payment_unity_desc = attributes.contract_payment_unity_desc || ''
      this.created_at = attributes.created_at ? moment(attributes.created_at).format() : moment()
      this.updated_at = attributes.updated_at ? moment(attributes.updated_at).format() : moment()
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_contract_payment_unity = attributes.id_contract_payment_unity
      this.contract_payment_unity_siglas = attributes.contract_payment_unity_siglas
      this.contract_payment_unity_desc = attributes.contract_payment_unity_desc
      this.created_at = moment(attributes.created_at).format()
      this.updated_at = moment(attributes.updated_at).format()
    }
  }

  static validations = {
    contract_payment_unities: {
      contract_payment_unity_siglas: {
        required,
        maxLength: maxLength(50),
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_contract_payment_unity, contract_payment_unity_siglas} = object
          const _scenario = id_contract_payment_unity ? 'update' : 'create'
          const params = {id_contract_payment_unity, contract_payment_unity_siglas}
          const validation = await Contract_payment_unities.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        }
      },
      contract_payment_unity_desc: {
        required,
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_contract_payment_unity, contract_payment_unity_desc} = object
          const _scenario = id_contract_payment_unity ? 'update' : 'create'
          const params = {id_contract_payment_unity, contract_payment_unity_desc}
          const validation = await Contract_payment_unities.validate({...params, _scenario, _specific})
          return !validation.data ? false : validation.data.success
        }
      },
      created_at: {
        required,
      },
      updated_at: {
        required,
      },
    },
  }

  static feedbacks = {
    contract_payment_unities: {
      id_contract_payment_unity: {
        isUnique: 'Ya existe una unidad con este ID'

      },
      contract_payment_unity_siglas: {
        isUnique: 'Ya existe una unidad con este nombre'

      },
      contract_payment_unity_desc: {
        isUnique: 'Ya existe una unidad con esta descripción'

      },
    },
  }

  static columns = [
    {
      title: 'Nombre',
      dataIndex: 'contract_payment_unity_siglas',
      key: 'contract_payment_unity_siglas',
      width: '35%',
      align: 'center',
      sorter: (a, b) => (a.contract_payment_unity_siglas > b.contract_payment_unity_siglas) - (a.contract_payment_unity_siglas < b.contract_payment_unity_siglas)
    },
    {
      title: 'Descripción',
      dataIndex: 'contract_payment_unity_desc',
      key: 'contract_payment_unity_desc',
      width: '40%',
      align: 'center',
      sorter: (a, b) => (a.contract_payment_unity_desc > b.contract_payment_unity_desc) - (a.contract_payment_unity_desc < b.contract_payment_unity_desc)
    },
    {
      title: 'Creado',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Actualizado',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_contract_payment_unity
  }

  class_name () {
    return 'Contract_payment_unity'
  }
}

