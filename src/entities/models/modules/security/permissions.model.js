/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Sep 07 15:53:43 GMT-04:00 2020
 *@time Mon Sep 07 15:53:43 GMT-04:00 2020
 */
import {maxLength, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

import moment from 'moment'

const url = 'security/permissions'

export default class Permissions extends BaseModel {

  id_permission
  permission_name
  peromission_desc
  created_at
  updated_at

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_permission = attributes.id_permission || undefined
      this.permission_name = attributes.permission_name || ''
      this.peromission_desc = attributes.peromission_desc || ''
      this.created_at = attributes.created_at ? moment(attributes.created_at).format() : moment().format()
      this.updated_at = attributes.updated_at ? moment(attributes.updated_at).format() : moment().format()
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_permission = attributes.id_permission
      this.permission_name = attributes.permission_name
      this.peromission_desc = attributes.peromission_desc
      this.created_at = moment(attributes.created_at).format()
      this.updated_at = moment(attributes.updated_at).format()
    }
  }

  static validations = {
    permissions: {
      permission_name: {
        required,
        maxLength: maxLength(20),
      },
      peromission_desc: {
        required,
        maxLength: maxLength(40),
      },
      created_at: {
        required,
      },
      updated_at: {
        required,
      },
    },
  }

  static feedbacks = {
    permissions: {
      id_permission: {
        isUnique: 'Ya existe un permiso con este ID'

      },
    },
  }

  static columns = [
    {
      title: 'Nombre',
      dataIndex: 'permission_name',
      key: 'permission_name',
      width: '35%',
      align: 'center',
      sorter: (a, b) => (a.permission_name > b.permission_name) - (a.permission_name < b.permission_name)
    },
    {
      title: 'Descripción',
      dataIndex: 'peromission_desc',
      key: 'peromission_desc',
      width: '40%',
      align: 'center',
      sorter: (a, b) => (a.peromission_desc > b.peromission_desc) - (a.peromission_desc < b.peromission_desc)
    },
    {
      title: 'Created_at',
      dataIndex: 'created_at',
      key: 'created_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.created_at - b.created_at
    },
    {
      title: 'Updated_at',
      dataIndex: 'updated_at',
      key: 'updated_at',
      width: '20%',
      align: 'center',
      sorter: (a, b) => a.updated_at - b.updated_at
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_permission
  }

  class_name () {
    return 'Permission'
  }

}

