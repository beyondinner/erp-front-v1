/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Sep 07 15:53:43 GMT-04:00 2020
 *@time Mon Sep 07 15:53:43 GMT-04:00 2020
 */
import {integer, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

const url = 'security/actions'

export default class Actions extends BaseModel {

  id_actions
  app
  module
  controller
  action
  default_access
  enabled
  global

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_actions = attributes.id_actions || undefined
      this.app = attributes.app || ''
      this.module = attributes.module || ''
      this.controller = attributes.controller || ''
      this.action = attributes.action || ''
      this.default_access = attributes.default_access || ''
      this.enabled = attributes.enabled || ''
      this.global = attributes.global || ''
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_actions = attributes.id_actions
      this.app = attributes.app
      this.module = attributes.module
      this.controller = attributes.controller
      this.action = attributes.action
      this.default_access = attributes.default_access
      this.enabled = attributes.enabled
      this.global = attributes.global
    }
  }

  static validations = {
    actions: {
      app: {
        required,
      },
      module: {
        required,
      },
      controller: {
        required,
      },
      action: {
        required,
      },
      default_access: {
        required,
      },
      enabled: {
        required,
        integer,
      },
      global: {},
    },
  }

  static feedbacks = {
    actions: {
      id_actions: {
        isUnique: 'This id_actions has been taken'

      },
    },
  }

  static columns = [
    {
      title: 'App',
      dataIndex: 'app',
      key: 'app',
      width: '20%',
      sorter: (a, b) => (a.app > b.app) - (a.app < b.app)
    },
    {
      title: 'Module',
      dataIndex: 'module',
      key: 'module',
      width: '20%',
      sorter: (a, b) => (a.module > b.module) - (a.module < b.module)
    },
    {
      title: 'Controller',
      dataIndex: 'controller',
      key: 'controller',
      width: '20%',
      sorter: (a, b) => (a.controller > b.controller) - (a.controller < b.controller)
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      width: '20%',
      sorter: (a, b) => (a.action > b.action) - (a.action < b.action)
    },
    {
      title: 'Default_access',
      dataIndex: 'default_access',
      key: 'default_access',
      width: '20%',
      sorter: (a, b) => (a.default_access > b.default_access) - (a.default_access < b.default_access)
    },
    {
      title: 'Enabled',
      dataIndex: 'enabled',
      key: 'enabled',
      width: '20%',
      sorter: (a, b) => a.enabled - b.enabled
    },
    {
      title: 'Global',
      dataIndex: 'global',
      key: 'global',
      width: '20%',
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_actions
  }

  class_name () {
    return 'Actions'
  }

}

