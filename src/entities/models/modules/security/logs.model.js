/**Generate by ASGENS
*@author Charlietyn
*@date Fri Oct 16 13:30:29 GMT-04:00 2020
*@time Fri Oct 16 13:30:29 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength,
    decimal
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'security/logs';

    export default class Logs extends BaseModel {

       id_log
       log_action
       log_description
       user_id
       created_at
       updated_at
       table
       id_table
       name_user

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_log = attributes.id_log|| undefined
        this.log_action = attributes.log_action|| ''
        this.log_description = attributes.log_description|| ''
        this.user_id = attributes.user_id|| ''
        this.created_at = attributes.created_at?moment(attributes.created_at):null
        this.updated_at = attributes.updated_at?moment(attributes.updated_at):null
        this.table = attributes.table|| ''
        this.id_table = attributes.id_table|| ''
        this.name_user = attributes.name_user|| ''
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_log = attributes.id_log
        this.log_action = attributes.log_action
        this.log_description = attributes.log_description
        this.user_id = attributes.user_id
        this.created_at = moment(attributes.created_at)
        this.updated_at = moment(attributes.updated_at)
        this.table = attributes.table
        this.id_table = attributes.id_table
        this.name_user = attributes.name_user
      }
    }

    static validations = {
      logs: {
        log_action: {
          required,
        },
        log_description: {
          required,
        },
        user_id: {
          required,
          integer,
        },
        created_at: {
          required,
        },
        updated_at: {
          required,
        },
        table: {
        },
        id_table: {
          integer,
        },
        name_user: {
          required,
        },
      },
    }

    static feedbacks = {
      logs: {
      id_log: {
        isUnique: 'This id_log has been taken'

      },
      },
    }

  static columns = [
    {
      title: 'Acción',
      dataIndex: 'log_action',
      align:'center',
      key: 'log_action',
      width: '20%',
      sorter: (a, b) =>  (a.log_action > b.log_action)-(a.log_action < b.log_action)
    },
    {
      title: 'Log_description',
      dataIndex: 'log_description',
      align:'center',
      key: 'log_description',
      width: '20%',
      sorter: (a, b) =>  (a.log_description > b.log_description)-(a.log_description < b.log_description)
    },
    {
      title: 'User_id',
      dataIndex: 'user_id',
      align:'center',
      key: 'user_id',
      width: '20%',
      sorter: (a, b) => a.user_id - b.user_id
    },
    {
      title: 'Tabla',
      dataIndex: 'table',
      align:'center',
      key: 'table',
      width: '20%',
      sorter: (a, b) =>  (a.table > b.table)-(a.table < b.table)
    },
    {
      title: 'Record',
      dataIndex: 'id_table',
      align:'center',
      key: 'id_table',
      width: '20%',
      sorter: (a, b) => a.id_table - b.id_table
    },
    {
      title: 'Nombre del usuario',
      dataIndex: 'name_user',
      align:'center',
      key: 'name_user',
      width: '20%',
      sorter: (a, b) =>  (a.name_user > b.name_user)-(a.name_user < b.name_user)
    },
    {
      title: 'Fecha',
      customRender:(element)=> { return moment(element.created_at).format('DD/MM/YYYY HH:mm a')},
      align:'center',
      key: 'created_at',
      width: '20%',
      sorter: (a, b) =>  (a.created_at > b.created_at)-(a.created_at < b.created_at)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      fixed: 'right',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_log;
    }
    class_name() {
        return 'Logs'
      }


   }

