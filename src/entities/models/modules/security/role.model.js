/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Sep 07 15:53:43 GMT-04:00 2020
 *@time Mon Sep 07 15:53:43 GMT-04:00 2020
 */
import {integer, required} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

const url = 'security/role'

export default class Role extends BaseModel {

  id_role
  id_father_role
  role_name

  constructor (attributes = null) {
    super()
    if (attributes != null) {

      this.id_role = attributes.id_role || undefined
      this.id_father_role = attributes.id_father_role || ''
      this.role_name = attributes.role_name || ''
    }
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_role = attributes.id_role
      this.id_father_role = attributes.id_father_role
      this.role_name = attributes.role_name
    }
  }

  static validations = {
    role: {
      id_father_role: {
        integer,
      },
      role_name: {
        required,
      },
    },
  }

  static feedbacks = {
    role: {
      id_role: {
        isUnique: 'Ya existe un rol con este ID'

      },
    },
  }

  static columns = [
    {
      title: 'Rol padre',
      dataIndex: 'father_role.role_name',
      key: 'role.id_role',
      width: '35%',
      align: 'center',
      sorter: (a, b) => (a.father_role.role_name > b.father_role.role_name) - (a.father_role.role_name < b.father_role.role_name)
    },
    {
      title: 'Rol',
      dataIndex: 'role_name',
      key: 'role_name',
      width: '40%',
      align: 'center',
      sorter: (a, b) => (a.role_name > b.role_name) - (a.role_name < b.role_name)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_role
  }

  class_name () {
    return 'Role'
  }

}

