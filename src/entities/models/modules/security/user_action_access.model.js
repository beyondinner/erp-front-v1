/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'security/user_action_access';

    export default class User_action_access extends BaseModel {

       id_user_action_access
       id_user
       id_actions
       range_user_action_access
       enabled
       start_range
       end_range

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_user_action_access = attributes.id_user_action_access|| undefined
        this.id_user = attributes.id_user|| ''
        this.id_actions = attributes.id_actions|| ''
        this.range_user_action_access = attributes.range_user_action_access|| ''
        this.enabled = attributes.enabled|| ''
        this.start_range = attributes.start_range?moment(attributes.start_range).format():null
        this.end_range = attributes.end_range?moment(attributes.end_range).format():null
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_user_action_access = attributes.id_user_action_access
        this.id_user = attributes.id_user
        this.id_actions = attributes.id_actions
        this.range_user_action_access = attributes.range_user_action_access
        this.enabled = attributes.enabled
        this.start_range = moment(attributes.start_range).format()
        this.end_range = moment(attributes.end_range).format()
      }
    }

    static validations = {
      user_action_access: {
        id_user: {
          required,
          integer,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_user_action_access,id_user,id_actions}=object
               const _scenario=id_user_action_access?'update':'create'
               const params={id_user_action_access,id_user,id_actions}
               const validation= await User_action_access.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        id_actions: {
          required,
          integer,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_user_action_access,id_user,id_actions}=object
               const _scenario=id_user_action_access?'update':'create'
               const params={id_user_action_access,id_user,id_actions}
               const validation= await User_action_access.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        range_user_action_access: {
          required,
          integer,
        },
        enabled: {
          required,
          integer,
        },
        start_range: {
          required,
        },
        end_range: {
          required,
        },
      },
    }

    static feedbacks = {
      user_action_access: {
      id_user_action_access: {
        isUnique: 'This id_user_action_access has been taken'

      },
      id_user: {
        isUnique: 'The combination of id_user, id_actions has benn taken'

      },
      id_actions: {
        isUnique: 'The combination of id_user, id_actions has benn taken'

      },
      },
    }

  static columns = [
    {
      title: 'Users',
      dataIndex: 'users.nombre_usuario',
      key: 'users.id_user',
      width: '20%',
      sorter: (a, b) =>  (a.users.nombre_usuario > b.users.nombre_usuario)-(a.users.nombre_usuario < b.users.nombre_usuario)
    },
    {
      title: 'Actions',
      dataIndex: 'actions.app',
      key: 'actions.id_actions',
      width: '20%',
      sorter: (a, b) =>  (a.actions.app > b.actions.app)-(a.actions.app < b.actions.app)
    },
    {
      title: 'Range_user_action_access',
      dataIndex: 'range_user_action_access',
      key: 'range_user_action_access',
      width: '20%',
      sorter: (a, b) => a.range_user_action_access - b.range_user_action_access
    },
    {
      title: 'Enabled',
      dataIndex: 'enabled',
      key: 'enabled',
      width: '20%',
      sorter: (a, b) => a.enabled - b.enabled
    },
    {
      title: 'Start_range',
      dataIndex: 'start_range',
      key: 'start_range',
      width: '20%',
      sorter: (a, b) => a.start_range - b.start_range
    },
    {
      title: 'End_range',
      dataIndex: 'end_range',
      key: 'end_range',
      width: '20%',
      sorter: (a, b) => a.end_range - b.end_range
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_user_action_access;
    }

      class_name() {
        return 'User_action_access'
      }

   }

