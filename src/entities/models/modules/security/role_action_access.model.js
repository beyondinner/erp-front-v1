/**Generate by ASGENS
*@author Charlietyn
*@date Mon Sep 07 15:53:43 GMT-04:00 2020
*@time Mon Sep 07 15:53:43 GMT-04:00 2020
*/
import {
    required,
    integer,
    between,
    maxLength,
    minLength
  } from 'vuelidate/lib/validators';

  import BaseModel from '../../base.model';

  import moment from 'moment';

  const url = 'security/role_action_access';

    export default class Role_action_access extends BaseModel {

       id_role_action_access
       id_role
       id_actions
       range_role_action_access
       enabled
       start_range
       end_range

    constructor(attributes = null) {
      super();
      if (attributes != null) {

        this.id_role_action_access = attributes.id_role_action_access|| undefined
        this.id_role = attributes.id_role|| ''
        this.id_actions = attributes.id_actions|| ''
        this.range_role_action_access = attributes.range_role_action_access|| ''
        this.enabled = attributes.enabled|| ''
        this.start_range = attributes.start_range?moment(attributes.start_range).format():null
        this.end_range = attributes.end_range?moment(attributes.end_range).format():null
      }
    }

    set_attributes(attributes = null) {
      if (attributes != null) {

        this.id_role_action_access = attributes.id_role_action_access
        this.id_role = attributes.id_role
        this.id_actions = attributes.id_actions
        this.range_role_action_access = attributes.range_role_action_access
        this.enabled = attributes.enabled
        this.start_range = moment(attributes.start_range).format()
        this.end_range = moment(attributes.end_range).format()
      }
    }

    static validations = {
      role_action_access: {
        id_role: {
          required,
          integer,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_role_action_access,id_role,id_actions}=object
               const _scenario=id_role_action_access?'update':'create'
               const params={id_role_action_access,id_role,id_actions}
               const validation= await Role_action_access.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        id_actions: {
          required,
          integer,
          async isUnique(value,object) {
               if(!value)
                   return true
               const _specific=true
               const {id_role_action_access,id_role,id_actions}=object
               const _scenario=id_role_action_access?'update':'create'
               const params={id_role_action_access,id_role,id_actions}
               const validation= await Role_action_access.validate({...params,_scenario,_specific})
               return !validation.data?false:validation.data.success
        }
        },
        range_role_action_access: {
          required,
          integer,
        },
        enabled: {
          required,
          integer,
        },
        start_range: {
        },
        end_range: {
        },
      },
    }

    static feedbacks = {
      role_action_access: {
      id_role_action_access: {
        isUnique: 'This id_role_action_access has been taken'

      },
      id_role: {
        isUnique: 'The combination of id_role, id_actions has benn taken'

      },
      id_actions: {
        isUnique: 'The combination of id_role, id_actions has benn taken'

      },
      },
    }

  static columns = [
    {
      title: 'Role',
      dataIndex: 'role.role_name',
      key: 'role.id_role',
      width: '20%',
      sorter: (a, b) =>  (a.role.role_name > b.role.role_name)-(a.role.role_name < b.role.role_name)
    },
    {
      title: 'Actions',
      dataIndex: 'actions.app',
      key: 'actions.id_actions',
      width: '20%',
      sorter: (a, b) =>  (a.actions.app > b.actions.app)-(a.actions.app < b.actions.app)
    },
    {
      title: 'Range_role_action_access',
      dataIndex: 'range_role_action_access',
      key: 'range_role_action_access',
      width: '20%',
      sorter: (a, b) => a.range_role_action_access - b.range_role_action_access
    },
    {
      title: 'Enabled',
      dataIndex: 'enabled',
      key: 'enabled',
      width: '20%',
      sorter: (a, b) => a.enabled - b.enabled
    },
    {
      title: 'Start_range',
      dataIndex: 'start_range',
      key: 'start_range',
      width: '20%',
      sorter: (a, b) => a.start_range - b.start_range
    },
    {
      title: 'End_range',
      dataIndex: 'end_range',
      key: 'end_range',
      width: '20%',
      sorter: (a, b) => a.end_range - b.end_range
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '15%',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ];

    static get url() {
      return url
    };

    get url() {
      return url
    };

    get_id() {
      return this.id_role_action_access;
    }

      class_name() {
        return 'Role_action_access'
      }

   }

