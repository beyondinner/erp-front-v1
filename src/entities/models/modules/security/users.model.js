/**Generate by ASGENS
 *@author team_cujae
 *@date Thu Jun 11 12:27:00 GMT-04:00 2020
 *@time Thu Jun 11 12:27:00 GMT-04:00 2020
 */
import {email, integer, maxLength, minLength, required, sameAs} from 'vuelidate/lib/validators'

import BaseModel from '../../base.model'

const url = 'security/users'

export default class Users extends BaseModel {

  id_user
  nombre_usuario
  apellido_usuario
  username
  pass
  email
  auth_key
  created_at
  updated_at
  active
  phone
  image
  id_role
  confirm_password
  status

  constructor (attributes = null) {
    super()
    this.id_user = attributes != null ? attributes.id_user || undefined : undefined
    this.nombre_usuario = attributes != null ? attributes.nombre_usuario || '' : ''
    this.apellido_usuario = attributes != null ? attributes.apellido_usuario || '' : ''
    this.username = attributes != null ? attributes.username || '' : ''
    this.pass = attributes != null ? attributes.pass || undefined : undefined
    this.email = attributes != null ? attributes.email || '' : ''
    this.auth_key = attributes != null ? attributes.auth_key || '' : ''
    this.auth_key = attributes != null ? attributes.auth_key || '' : ''
    this.phone = attributes != null ? attributes.phone || undefined : undefined
    this.updated_at = attributes != null ? attributes.updated_at || undefined : undefined
    this.active = attributes != null ? attributes.active == '1' ? true : false : false
    this.image = attributes != null ? attributes.image || 'user.jpg' : 'user.jpg'
    this.id_role = attributes != null ? attributes.id_role || undefined : undefined
    this.status = attributes != null ? attributes.status || 'A' : 'A'
  }

  set_attributes (attributes = null) {
    if (attributes != null) {

      this.id_user = attributes.id_user
      this.nombre_usuario = attributes.nombre_usuario
      this.apellido_usuario = attributes.apellido_usuario
      this.username = attributes.username
      this.pass = attributes.pass
      this.email = attributes.email
      this.auth_key = attributes.auth_key
      this.phone = attributes.phone
      this.created_at = attributes.created_at
      this.updated_at = attributes.updated_at
      this.active = attributes.active == '1' ? true : false
      this.image = attributes.image
      this.id_role = attributes.id_role
      this.status = attributes.status
    }
  }

  get_full_name () {
    return this.nombre_usuario + ' ' + this.apellido_usuario
  }

  static validations = {
    users: {
      nombre_usuario: {
        required,
        maxLength: maxLength(45),
      },
      apellido_usuario: {
        required,
        maxLength: maxLength(45),
      },
      username: {
        required,
        maxLength: maxLength(45),
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_user, username} = object
          const params = {id_user, username}
          const _scenario = id_user ? 'update' : undefined
          const validation = await Users.validate({...params, _specific, _scenario})
          return !validation.data ? false : validation.data.success
        }
      },
      pass_actual: {
        required,
        async valid_pass (value, object) {
          if (!value) {
            return true
          }
          const {id_user} = object
          const params = {}
          params.id_user = id_user
          params.pass = value
          const validation = await Users.custom('post', 'validate_pass', params)
          return !validation.data ? false : validation.data.success
        }
      },
      pass: {
        required,
        minLength: minLength(8),
      },
      email: {
        email,
        required,
        maxLength: maxLength(45),
        async isUnique (value, object) {
          if (!value) {
            return true
          }
          const _specific = true
          const {id_user, email} = object
          const params = {id_user, email}
          const _scenario = id_user ? 'update' : undefined
          const validation = await Users.validate({...params, _specific, _scenario})
          return !validation.data ? false : validation.data.success
        }
      },
      auth_key: {
        maxLength: maxLength(255),
      },
      created_at: {},
      updated_at: {},
      active: {
        required,
      },
      image: {
        required,
        maxLength: maxLength(20),
      },
      id_role: {
        required,
        integer,
      },
      confirm_password: {
        sameAsPassword: sameAs('pass')
      },
    },
  }

  static getImageURL (image) {
    image = image || 'user.jpg'
    return process.env.BASE_URL + 'assets/users/' + image
  }

  static feedbacks = {
    users: {
      email: {
        isUnique: 'Ya existe un elemento con este email'
      },
      pass: {
        minLength: 'Debe tener como mínimo 8 caracteres'

      },
      id_user: {
        isUnique: 'Ya existe un elemento con este id_user'
      },
      username: {
        isUnique: 'Ya existe un elemento con este username'
      },
      pass_actual: {
        valid_pass: 'Error en contraseña actual'
      }
    },
  }
  static columns = [
    {
      title: '',
      key: 'image',
      hide_on_export:true,
      scopedSlots: {customRender: 'image'},
      width: '3%',
      align: 'center',
      sorter: (a, b) => (a.image > b.image) - (a.image < b.image)
    },
    {
      title: 'Nombre',
      dataIndex: 'nombre_usuario',
      key: 'nombre_usuario',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.nombre_usuario > b.nombre_usuario) - (a.nombre_usuario < b.nombre_usuario)
    },
    {
      title: 'Apellidos',
      dataIndex: 'apellido_usuario',
      key: 'apellido_usuario',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.apellido_usuario > b.apellido_usuario) - (a.apellido_usuario < b.apellido_usuario)
    },
    {
      title: 'Usuario',
      dataIndex: 'username',
      key: 'username',
      width: '10%',
      align: 'center',
      sorter: (a, b) => (a.username > b.username) - (a.username < b.username)
    },
    {
      title: 'Estado',
      scopedSlots: {
        customRender: 'status'
      },
      key: 'status',
      width: '10%',
      align: 'center',
      sorter: (a, b) => (a.status > b.status) - (a.status < b.status)
    },
    {
      title: 'Correo',
      dataIndex: 'email',
      key: 'email',
      width: '20%',
      align: 'center',
      sorter: (a, b) => (a.email > b.email) - (a.email < b.email)
    },
    {
      title: 'Activo',
      dataIndex: 'active',
      key: 'active',
      scopedSlots: {customRender: 'activo'},
      width: '10%',
      align: 'center',
    },
    {
      title: 'Rol',
      dataIndex: 'role.role_name',
      key: 'role.role_name',
      width: '15%',
      align: 'center',
      sorter: (a, b) => (a.role.role_name > b.role.role_name) - (a.role.role_name < b.role.role_name)
    },
    {
      title: 'Acciones',
      key: 'action_elements',
      width: '20%',
      align: 'center',
      scopedSlots: {
        customRender: 'action'
      }
    }
  ]

  static get url () {
    return url
  };

  get url () {
    return url
  };

  get_id () {
    return this.id_user
  }

  class_name () {
    return 'Users'
  }

}

