import Vue from 'vue'
import Router from 'vue-router'
import index from '../../components/views/site/index'
/* Módulo managment*/
import contacts_list from '../../components/views/managment/contacts/list/contacts_list'
import contacts_form from '../../components/views/managment/contacts/form/contacts_form'
import contacts_view from '../../components/views/managment/contacts/view/contacts_view'
import contracts_list from '../../components/views/managment/contracts/list/contracts_list'
import contracts_form from '../../components/views/managment/contracts/form/contracts_form'
import contracts_view from '../../components/views/managment/contracts/view/contracts_view'
import employees_list from '../../components/views/managment/employees/list/employees_list'
import employees_form from '../../components/views/managment/employees/form/employees_form'
import employees_view from '../../components/views/managment/employees/view/employees_view'
import payrolls_list from '../../components/views/managment/payrolls/list/payrolls_list'
import payrolls_form from '../../components/views/managment/payrolls/form/payrolls_form'
import payrolls_view from '../../components/views/managment/payrolls/view/payrolls_view'
import products_list from '../../components/views/managment/products/list/products_list'
import products_form from '../../components/views/managment/products/form/products_form'
import products_view from '../../components/views/managment/products/view/products_view'
import services_list from '../../components/views/managment/services/list/services_list'
import services_form from '../../components/views/managment/services/form/services_form'
import services_view from '../../components/views/managment/services/view/services_view'
import club_members_list from '../../components/views/managment/club_members/list/club_members_list'
import club_members_form from '../../components/views/managment/club_members/form/club_members_form'
import profile_member from '../../components/views/managment/club_members/profile/profile_member'
import club_member_history_list
  from '../../components/views/managment/club_member_history/list/club_member_history_list'
import club_member_history_form
  from '../../components/views/managment/club_member_history/form/club_member_history_form'
import history_list from '../../components/views/managment/history_data/history_list'
/* Módulo billings*/
import billings_list from '../../components/views/billings/billings/list/billings_list'
import billings_form from '../../components/views/billings/billings/form/billings_form'
import budget_items_list_list from '../../components/views/billings/budget_items_list/list/budget_items_list_list'
import budget_items_list_form from '../../components/views/billings/budget_items_list/form/budget_items_list_form'
import budget_items_list_taxes_list
  from '../../components/views/billings/budget_items_list_taxes/list/budget_items_list_taxes_list'
import budget_items_list_taxes_form
  from '../../components/views/billings/budget_items_list_taxes/form/budget_items_list_taxes_form'
import budgets_list from '../../components/views/billings/budgets/list/budgets_list'
import budgets_form from '../../components/views/billings/budgets/form/budgets_form'
/* Módulo nomenclatures*/
import billings_status_list from '../../components/views/nomenclatures/billings_status/list/billings_status_list'
import billings_status_form from '../../components/views/nomenclatures/billings_status/form/billings_status_form'
import budget_status_list from '../../components/views/nomenclatures/budget_status/list/budget_status_list'
import budget_status_form from '../../components/views/nomenclatures/budget_status/form/budget_status_form'
import contact_types_list from '../../components/views/nomenclatures/contact_types/list/contact_types_list'
import contact_types_form from '../../components/views/nomenclatures/contact_types/form/contact_types_form'
import employee_status_list from '../../components/views/nomenclatures/employee_status/list/employee_status_list'
import employee_status_form from '../../components/views/nomenclatures/employee_status/form/employee_status_form'
import legal_types_list from '../../components/views/nomenclatures/legal_types/list/legal_types_list'
import legal_types_form from '../../components/views/nomenclatures/legal_types/form/legal_types_form'
import payment_types_list from '../../components/views/nomenclatures/payment_types/list/payment_types_list'
import payment_types_form from '../../components/views/nomenclatures/payment_types/form/payment_types_form'
import payroll_status_list from '../../components/views/nomenclatures/payroll_status/list/payroll_status_list'
import payroll_status_form from '../../components/views/nomenclatures/payroll_status/form/payroll_status_form'
import product_status_list from '../../components/views/nomenclatures/product_status/list/product_status_list'
import product_status_form from '../../components/views/nomenclatures/product_status/form/product_status_form'
import service_status_list from '../../components/views/nomenclatures/service_status/list/service_status_list'
import service_status_form from '../../components/views/nomenclatures/service_status/form/service_status_form'
import contract_status_list from '../../components/views/nomenclatures/contract_status/list/contract_status_list'
import contract_status_form from '../../components/views/nomenclatures/contract_status/form/contract_status_form'
import contract_types_list from '../../components/views/nomenclatures/contract_types/list/contract_types_list'
import contract_types_form from '../../components/views/nomenclatures/contract_types/form/contract_types_form'
import countries_list from '../../components/views/nomenclatures/countries/list/countries_list'
import countries_form from '../../components/views/nomenclatures/countries/form/countries_form'
import currencies_list from '../../components/views/nomenclatures/currencies/list/currencies_list'
import currencies_form from '../../components/views/nomenclatures/currencies/form/currencies_form'
import states_list from '../../components/views/nomenclatures/states/list/states_list'
import states_form from '../../components/views/nomenclatures/states/form/states_form'
import languages_list from '../../components/views/nomenclatures/languages/list/languages_list'
import languages_form from '../../components/views/nomenclatures/languages/form/languages_form'
import contract_payment_unities_list
  from '../../components/views/nomenclatures/contract_payment_unities/list/contract_payment_unities_list'
import contract_payment_unities_form
  from '../../components/views/nomenclatures/contract_payment_unities/form/contract_payment_unities_form'
import contact_status_list from '../../components/views/nomenclatures/contact_status/list/contact_status_list'
import contact_status_form from '../../components/views/nomenclatures/contact_status/form/contact_status_form'
/* Módulo security*/
import role_list from '../../components/views/security/role/list/role_list'
import role_form from '../../components/views/security/role/form/role_form'
import role_action_access_list from '../../components/views/security/role_action_access/list/role_action_access_list'
import role_action_access_form from '../../components/views/security/role_action_access/form/role_action_access_form'
import user_action_access_list from '../../components/views/security/user_action_access/list/user_action_access_list'
import user_action_access_form from '../../components/views/security/user_action_access/form/user_action_access_form'
import users_list from '../../components/views/security/users/list/users_list'
import users_form from '../../components/views/security/users/form/users_form'
import actions_list from '../../components/views/security/actions/list/actions_list'
import actions_form from '../../components/views/security/actions/form/actions_form'
import permissions_list from '../../components/views/security/permissions/list/permissions_list'
import permissions_form from '../../components/views/security/permissions/form/permissions_form'
import logs_list from '../../components/views/security/logs/list/logs_list'
import logs_form from '../../components/views/security/logs/form/logs_form'
import {state} from '../../config/store/'
import club_members_list_adviser from '../../components/views/managment/club_members/list/club_members_list_adviser'
import products_buy from '../../components/views/managment/products/list/products_buy'
import services_buy from '../../components/views/managment/services/list/services_buy'
import event_buy_list from '../../components/views/managment/event_buy/list/event_buy_list'
import user_profile from '../../components/views/security/profile/user_profile'
import club_member_ficha from '../../components/views/managment/club_member_ficha/template/club_member_ficha'
import ficha_member_list from '../../components/views/nomenclatures/ficha_member/list/ficha_member_list'
import bitrix_contacts from '../../components/views/nomenclatures/bitrix/bitrix_contacts/bitrix_contacts'
import bitrix_deals from '../../components/views/nomenclatures/bitrix/bitrix_deals/bitrix_deals'

/* Módulo general*/

/* Módulo reports*/

Vue.use(Router)

const router = new Router({
  mode: 'history',
  hash: false,
  routes: [
    {
      path: '/',
      name: 'index',
      component: index,
      props: {page: 1}
    },
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/',
      name: 'home',
    },
    {
      path: '/managment/contacts_list',
      name: 'contacts_list',
      component: contacts_list,
      props: {page: 1}
    },
    {
      path: '/billings/orders_list',
      name: 'orders_list',
      component: require('../../components/views/billings/orders/list/orders_list').default,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/order_status_list',
      name: 'order_status_list',
      component: require('../../components/views/nomenclatures/order_status/list/order_status_list').default,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/order_status_form',
      name: 'order_status_form',
      component: require('../../components/views/nomenclatures/order_status/form/order_status_form').default,
      props: {page: 1}
    },
    {
      path: '/billings/orders_form',
      name: 'orders_form',
      component: require('../../components/views/billings/orders/form/orders_form').default,
      props: {page: 1}
    },
    {
      path: '/billings/orders_form/:id',
      name: 'update_order',
      component: require('../../components/views/billings/orders/form/orders_form').default,
      props: {page: 1}
    },
    {
      path: '/managment/contacts_form',
      name: 'contacts_form',
      component: contacts_form,
      props: {page: 1}
    },
    {
      path: '/managment/contacts_view',
      name: 'contacts_view',
      component: contacts_view,
      props: {page: 1}
    },
    {
      path: '/managment/contracts_list',
      name: 'contracts_list',
      component: contracts_list,
      props: {page: 1}
    },
    {
      path: '/managment/contracts_form',
      name: 'contracts_form',
      component: contracts_form,
      props: {page: 1}
    },
    {
      path: '/managment/contracts_view',
      name: 'contracts_view',
      component: contracts_view,
      props: {page: 1}
    },
    {
      path: '/managment/employees_list',
      name: 'employees_list',
      component: employees_list,
      props: {page: 1}
    },
    {
      path: '/managment/employees_form',
      name: 'employees_form',
      component: employees_form,
      props: {page: 1}
    },
    {
      path: '/managment/employees_view',
      name: 'employees_view',
      component: employees_view,
      props: {page: 1}
    },
    {
      path: '/managment/payrolls_list',
      name: 'payrolls_list',
      component: payrolls_list,
      props: {page: 1}
    },
    {
      path: '/managment/event_list',
      name: 'event_list',
      component: require('../../components/views/managment/event/list/event_list').default,
      props: {page: 1}
    },
    {
      path: '/managment/event_form',
      name: 'event_form',
      component: require('../../components/views/managment/event/form/event_form').default,
      props: {page: 1}
    },
    {
      path: '/managment/event_actions_list',
      name: 'event_actions_list',
      component: require('../../components/views/managment/event_actions/list/event_actions_list').default,
      props: {page: 1}
    },
    {
      path: '/managment/event_actions_form',
      name: 'event_actions_form',
      component: require('../../components/views/managment/event_actions/form/event_actions_form').default,
      props: {page: 1}
    },
    {
      path: '/managment/event_product_list',
      name: 'event_product_list',
      component: require('../../components/views/managment/event_product/list/event_product_list').default,
      props: {page: 1}
    },
    {
      path: '/managment/event_product_form',
      name: 'event_product_form',
      component: require('../../components/views/managment/event_product/form/event_product_form').default,
      props: {page: 1}
    },
    {
      path: '/managment/payrolls_form',
      name: 'payrolls_form',
      component: payrolls_form,
      props: {page: 1}
    },
    {
      path: '/managment/event_tab_form/:id',
      name: 'update_event',
      component: require('../../components/views/managment/event/form/event_tab_form').default,
      props: {page: 1}
    },
    {
      path: '/managment/add_event',
      name: 'add_event',
      component: require('../../components/views/managment/event/form/event_tab_form').default,
      props: {page: 1}
    },
    {
      path: '/managment/event_tab_form/:id',
      name: 'add_event_template',
      component: require('../../components/views/managment/event/form/event_tab_form').default,
      props: {page: 1}
    },
    {
      path: '/managment/payrolls_view',
      name: 'payrolls_view',
      component: payrolls_view,
      props: {page: 1}
    },
    {
      path: '/managment/products_list',
      name: 'products_list',
      component: products_list,
      props: {page: 1}
    },
    {
      path: '/managment/products_table',
      name: 'products_table',
      component: require('../../components/views/managment/products/list/products_table').default,
      props: {page: 1}
    },
    {
      path: '/managment/products_form',
      name: 'products_form',
      component: products_form,
      props: {page: 1}
    },
    {
      path: '/managment/products_view',
      name: 'products_view',
      component: products_view,
      props: {page: 1}
    },
    {
      path: '/managment/services_list',
      name: 'services_list',
      component: services_list,
      props: {page: 1}
    },
    {
      path: '/managment/services_form',
      name: 'services_form',
      component: services_form,
      props: {page: 1}
    },
    {
      path: '/managment/services_view',
      name: 'services_view',
      component: services_view,
      props: {page: 1}
    },
    {
      path: '/managment/club_members_list',
      name: 'club_members_list',
      component: club_members_list,
      props: {page: 1}
    },
    {
      path: '/managment/profile_member/:id',
      name: 'profile_member',
      component: profile_member,
      props: {page: 1}
    },
    {
      path: '/managment/club_members_form',
      name: 'club_members_form',
      component: club_members_form,
      props: {page: 1}
    },
    {
      path: '/managment/club_member_history_list',
      name: 'club_member_history_list',
      component: club_member_history_list,
      props: {page: 1}
    },
    {
      path: '/managment/club_member_history_form',
      name: 'club_member_history_form',
      component: club_member_history_form,
      props: {page: 1}
    },
    {
      path: '/billings/billings_list',
      name: 'billings_list',
      component: billings_list,
      props: {page: 1}
    },
    {
      path: '/billings/billings_form',
      name: 'billings_form',
      component: billings_form,
      props: {page: 1}
    },
    {
      path: '/billings/expenses_list',
      name: 'expenses_list',
      component: billings_list,
      meta: {gastos: true}
    },
    {
      path: '/billings/expenses_form',
      name: 'expenses_form',
      component: billings_form,
      props: {page: 1},
      meta: {gastos: true}

    },
    {
      path: '/billings/billings_budget_form/:id',
      name: 'billings_budget_form',
      component: require('../../components/views/billings/billings/form/billings_budget_form').default,
      props: {page: 1}
    },
    {
      path: '/billings/budget_items_list_list',
      name: 'budget_items_list_list',
      component: budget_items_list_list,
      props: {page: 1}
    },
    {
      path: '/billings/budget_items_list_form',
      name: 'budget_items_list_form',
      component: budget_items_list_form,
      props: {page: 1}
    },
    {
      path: '/billings/budget_items_list_taxes_list',
      name: 'budget_items_list_taxes_list',
      component: budget_items_list_taxes_list,
      props: {page: 1}
    },
    {
      path: '/billings/budget_items_list_taxes_form',
      name: 'budget_items_list_taxes_form',
      component: budget_items_list_taxes_form,
      props: {page: 1}
    },
    {
      path: '/billings/budgets_list',
      name: 'budgets_list',
      component: budgets_list,
      props: {page: 1}
    },
    {
      path: '/billings/budgets_form',
      name: 'budgets_form',
      component: budgets_form,
      props: {page: 1}
    },
    {
      path: '/billings/budgets_form/:id',
      name: 'update_budgets',
      component: budgets_form,
      props: {page: 1}
    },
    {
      path: '/billings/billings_form/:id',
      name: 'update_billings',
      component: billings_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/billings_status_list',
      name: 'billings_status_list',
      component: billings_status_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/billings_status_form',
      name: 'billings_status_form',
      component: billings_status_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/budget_status_list',
      name: 'budget_status_list',
      component: budget_status_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/budget_status_form',
      name: 'budget_status_form',
      component: budget_status_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contact_types_list',
      name: 'contact_types_list',
      component: contact_types_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contact_types_form',
      name: 'contact_types_form',
      component: contact_types_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/employee_status_list',
      name: 'employee_status_list',
      component: employee_status_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/employee_status_form',
      name: 'employee_status_form',
      component: employee_status_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/legal_types_list',
      name: 'legal_types_list',
      component: legal_types_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/legal_types_form',
      name: 'legal_types_form',
      component: legal_types_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/payment_types_list',
      name: 'payment_types_list',
      component: payment_types_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/payment_types_form',
      name: 'payment_types_form',
      component: payment_types_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/payroll_status_list',
      name: 'payroll_status_list',
      component: payroll_status_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/payroll_status_form',
      name: 'payroll_status_form',
      component: payroll_status_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/product_status_list',
      name: 'product_status_list',
      component: product_status_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/product_status_form',
      name: 'product_status_form',
      component: product_status_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/service_status_list',
      name: 'service_status_list',
      component: service_status_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/service_status_form',
      name: 'service_status_form',
      component: service_status_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contract_status_list',
      name: 'contract_status_list',
      component: contract_status_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contract_status_form',
      name: 'contract_status_form',
      component: contract_status_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contract_types_list',
      name: 'contract_types_list',
      component: contract_types_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contract_types_form',
      name: 'contract_types_form',
      component: contract_types_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/countries_list',
      name: 'countries_list',
      component: countries_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/countries_form',
      name: 'countries_form',
      component: countries_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/currencies_list',
      name: 'currencies_list',
      component: currencies_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/currencies_form',
      name: 'currencies_form',
      component: currencies_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/states_list',
      name: 'states_list',
      component: states_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/states_form',
      name: 'states_form',
      component: states_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/languages_list',
      name: 'languages_list',
      component: languages_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/languages_form',
      name: 'languages_form',
      component: languages_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contract_payment_unities_list',
      name: 'contract_payment_unities_list',
      component: contract_payment_unities_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contract_payment_unities_form',
      name: 'contract_payment_unities_form',
      component: contract_payment_unities_form,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contact_status_list',
      name: 'contact_status_list',
      component: contact_status_list,
      props: {page: 1}
    },
    {
      path: '/nomenclatures/contact_status_form',
      name: 'contact_status_form',
      component: contact_status_form,
      props: {page: 1}
    },
    {
      path: '/security/role_list',
      name: 'role_list',
      component: role_list,
      props: {page: 1}
    },
    {
      path: '/security/role_form',
      name: 'role_form',
      component: role_form,
      props: {page: 1}
    },
    {
      path: '/security/role_action_access_list',
      name: 'role_action_access_list',
      component: role_action_access_list,
      props: {page: 1}
    },
    {
      path: '/security/role_action_access_form',
      name: 'role_action_access_form',
      component: role_action_access_form,
      props: {page: 1}
    },
    {
      path: '/security/user_action_access_list',
      name: 'user_action_access_list',
      component: user_action_access_list,
      props: {page: 1}
    },
    {
      path: '/security/user_action_access_form',
      name: 'user_action_access_form',
      component: user_action_access_form,
      props: {page: 1}
    },
    {
      path: '/security/users_list',
      name: 'users_list',
      component: users_list,
      props: {page: 1}
    },
    {
      path: '/security/users_form',
      name: 'users_form',
      component: users_form,
      props: {page: 1}
    },
    {
      path: '/security/actions_list',
      name: 'actions_list',
      component: actions_list,
      props: {page: 1}
    },
    {
      path: '/security/actions_form',
      name: 'actions_form',
      component: actions_form,
      props: {page: 1}
    },
    {
      path: '/security/permissions_list',
      name: 'permissions_list',
      component: permissions_list,
      props: {page: 1}
    },
    {
      path: '/security/permissions_form',
      name: 'permissions_form',
      component: permissions_form,
      props: {page: 1}
    },
    {
      path: '/security/logs_list',
      name: 'logs_list',
      component: logs_list,
      props: {page: 1}
    },
    {
      path: '/security/logs_form',
      name: 'logs_form',
      component: logs_form,
      props: {page: 1}
    },
    {
      path: '/site/reset_pass/:id',
      name: 'reset_pass',
      component: require('../../components/views/site/reset_pass').default,
      props: {page: 1}
    },
    {
      path: '/site/init_pass',
      name: 'init_pass',
      component: require('../../components/views/site/reset_pass').default,
      props: {page: 1}
    },
    {
      path: '/site/register_member',
      name: 'register_member',
      component: require('../../components/views/site/register_member').default,
      props: {page: 1}
    },
    {
      path: '/billings/payment_recurrency_list/:id',
      name: 'payment_recurrency_list',
      component: require('../../components/views/billings/payment_recurrency/list/payment_recurrency_list').default,
      props: {page: 1}
    },
    {
      path: '/billings/payment_recurrency_form',
      name: 'payment_recurrency_form',
      component: require('../../components/views/billings/payment_recurrency/form/payment_recurrency_form').default,
      props: {page: 1}
    },
    {
      path: '/managment/contacts_adviser',
      name: 'contacts_adviser',
      component: club_members_list_adviser,
      props: {page: 1},
    },
    {
      path: '/managment/history',
      name: 'history_list',
      component: history_list,
      props: {page: 1},
    },
    {
      path: '/managment/products',
      name: 'products',
      component: products_buy,
      props: {page: 1},
    },
    {
      path: '/managment/services',
      name: 'services',
      component: services_buy,
      props: {page: 1},
    },
    {
      path: '/managment/events',
      name: 'event_buy_list',
      component: event_buy_list,
      props: {page: 1},
    },
    {
      path: '/account/profile',
      name: 'account_profile',
      component: user_profile,
      props: {page: 1},
    },
    {
      path: '/managment/club_member_ficha',
      name: 'club_member_ficha',
      component: club_member_ficha,
      props: {page: 1},
    },
    {
      path: '/nomenclatures/ficha_list',
      name: 'ficha_list',
      component: ficha_member_list,
      props: {page: 1},
    },
    {
      path: '/nomenclatures/bitrix_contacts',
      name: 'bitrix_contacts',
      component: bitrix_contacts,
      props: {page: 1},
    },
    {
      path: '/nomenclatures/bitrix_deals',
      name: 'bitrix_deals',
      component: bitrix_deals,
      props: {page: 1},
    },

  ]
})

router.beforeEach((to, from, next) => {
  if (state.site.user) {
    if (state.site.user.id_role === 1 || state.site.user.id_role === 2 || to.name === 'index') {
      next()
    } else if (state.site.user.id_role === 4) {
      if (to.name === 'account_profile' || to.name === 'contacts_adviser' || to.name === 'payrolls_list' || to.name === 'history_list' || to.name === 'profile_member') {
        next()
      } else {
        next({name: 'index'})
      }
    } else if (state.site.user.id_role === 3) {
      if (to.name === 'account_profile' || to.name === 'event_buy_list' || to.name === 'products' || to.name === 'services') {
        next()
      } else {
        next({name: 'index'})
      }
    } else {
      next()
    }
  }else {
    next()
  }
})

export default router

