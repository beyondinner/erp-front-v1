/**Generate by ASGENS
 *@author Charlietyn
 *@date Thu Sep 03 00:09:29 GMT-04:00 2020
 *@time Thu Sep 03 00:09:29 GMT-04:00 2020
 */
import axios from 'axios'
import Vue from 'vue'

/*Globally*/
axios.defaults.baseURL = process.env.BASE_URL

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

axios.pending = null
const reqInt = axios.interceptors.request.use(
  config => {
    if (Vue.prototype.$store.site.is_loggin) {
      config.headers.Authorization = 'Bearer ' + Vue.prototype.$store.site.jwt
    }
    return config
  }
)
// const respInt = axios.interceptors.response.use(
//   res => {
//     return res
//   },
//   (error) => {
//     console.log(error)
//     return error
//   }
// )
// axios.interceptors.request.eject(reqInt)
// axios.interceptors.response.eject(respInt)
export default axios
export const Axios = axios

